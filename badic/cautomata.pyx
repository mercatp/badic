# coding=utf8
"""
Det automaton of Finite state machines using C
DetAutomaton for determinist automata and CAutomaton for non necessarily determinist automaton


AUTHORS:

- Paul Mercat (2013)- I2M AMU Aix-Marseille Universite - initial version
- Dominique Benielli (2018) Labex Archimede - I2M -
  AMU Aix-Marseille Universite - Integration in -SageMath

REFERENCES:

.. [Hopcroft] "Around Hopcroft’s Algorithm"  Manuel of BACLET and
   Claire PAGETTI.

"""

# *****************************************************************************
#	   Copyright (C) 2014 Paul Mercat <paul.mercat@univ-amu.fr>
#
#  Distributed under the terms of the GNU General Public License (GPL)
#
#	This code is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#	General Public License for more details.
#
#  The full text of the GPL is available at:
#
#				  http://www.gnu.org/licenses/
# *****************************************************************************
from __future__ import division, print_function, absolute_import

from libc.stdlib cimport malloc, free

from cpython cimport bool as c_bool
from cysignals.signals cimport sig_on, sig_off, sig_check

from sage.graphs.digraph import DiGraph
from sage.graphs.graph import Graph

try:
	from sage.cpython.string import bytes_to_str, str_to_bytes
except ImportError:
	# for compatibility with sage version < 8.2
	def bytes_to_str(x):
		return str(x)
	def str_to_bytes(x):
		return str(x)

class Eqns:
	def __init__ (self, txt, lat):
		self.txt = txt
		self.lat = lat
	
	def __repr__ (self):
		return self.txt
	
	def _latex_(self):
		return self.lat

def proj_to_delta (v):
	s = sum(v)
	return [t/s for t in v[1:]]

def delta_to_proj (v):
	s = sum(v)
	from sage.modules.free_module_element import vector
	return vector([1-s]+list(v))

def is_positive (v):
	for t in v:
		if t < 0:
			return False
	return True

from sage.functions.other import sqrt
from sage.matrix.constructor import matrix
from sage.modules.free_module_element import zero_vector
from sage.modules.free_module_element import vector
from sage.matrix.special import identity_matrix

from badic.density import simplex_density

# list of regular simplices
RegSimplex = [matrix([[0],[1]]).transpose(), matrix([(0,0),(1,0),(.5, sqrt(3.)/2)]).transpose(), matrix([(0,0,0),(1,0,0),(.5, sqrt(3.)/2,0), (0.500000000000000, sqrt(3.)/6, 1/3*sqrt(3)*sqrt(2))]).transpose()]

# precomputation for function pt_to_delta
lv0 = []
lmat = []
for T in RegSimplex:
	n = T.ncols()
	mI = identity_matrix(n)
	B = [mI[i]-mI[0] for i in range(1,n)]
	m = matrix([T*b for b in B]).transpose()
	v0 = zero_vector(n)
	v0[0] = 1
	mat = matrix(B).transpose()*m.inverse()
	lmat.append(mat)
	lv0.append(v0 - mat*T*v0)

def sizeofAutomaton ():
	return sizeof(Automaton)

def proj_to_pt (v):
	"""
	Convert a pt of projective space to a pt in the regular simplex.
	"""
	if len(v) not in [2,3,4]:
		raise NotImplementedError("Sorry, this function is implemented only for vectors of size 2,3 or 4.")
	return RegSimplex[len(v)-2]*vector(v)/<float>sum(v)

def pt_to_delta (p):
	"""
	Convert a point in a regular simplex into a projective point.
	"""
	n = len(p)-1
	return lmat[n]*vector(p) + lv0[n]
	
def plot_tri (lv, color=None, alpha=1, fill=False, text=True, colors=None):
	"""
	Plot a triangle from projective coordinates of vertices.
	
	INPUT:
	
		- ``lv`` - list or matrix -- coordinates of vertices
		
		- ``alpha`` - float (default: ``1``) -- transparency
		
		- ``fill`` - boolean (default: ``False``) -- fill the polygon ?
		
		- ``text`` - boolean (default: ``True``) -- draw numbers on vertices ?
	
	OUTPUT:
		A Graphics object.

	EXAMPLES::
		sage: from badic import *
		sage: plot_tri(identity_matrix(3))
		Graphics object consisting of 4 graphics primitives

	"""
	from sage.plot.polygon import polygon
	from sage.structure.element import is_Matrix
	if is_Matrix(lv):
		lv = lv.columns()
	d = len(lv[0])
	if color is None:
		if d == 3:
			color = "black"
		else:
			color = "yellow"
	lC = RegSimplex[d-2].columns()
	pts = [proj_to_pt(p) for p in lv]
	g = polygon(pts, fill=fill, color=color, alpha=alpha)
	if text:
		if colors is None:
			colors = [color for _ in range(d)]
		m = sum(pts)/len(pts)
		for i in range(d):
			if d == 4:
				from sage.plot.plot3d.shapes2 import text3d
				g += text3d("%s" % i, 1.03*(pts[i] - m) + m, color=colors[i])
			else:
				from sage.plot.text import text
				g += text("%s" % i, 1.03*(pts[i] - m) + m, color=colors[i])
	return g

def loop_vectors(m, left=True, ring=None, verb=False):
	"""
	Compute the projective vectors associated to the iteration of matrix m.

	INPUT:

		- ``m`` - a square matrix, assumed to be with integer coefficients

		- ``left`` - boolean (default: ``True``) -- return the right or left limit vector

		- ``ring`` - a ring (default: ``None``) -- compute only vectors in this ring

	OUTPUT:
		A list of vectors.

	EXAMPLES::

		sage: from badic.cautomata import loop_vectors
		sage: m = matrix([[1,1],[0,1]])
		sage: loop_vectors(m)
		[(0, 1)]

	"""
	if left == False:
		return loop_vectors(m.transpose(), ring=ring)
	from sage.rings.qqbar import AlgebraicField
	J,P = m.jordan_form(transformation=True, base_ring=AlgebraicField())
	ri = 0
	mjs = 0
	vp = max(J.diagonal())
	if verb:
		print("max vp : %s" % vp)
	if ring is not None:
		try:
			ring(vp)
		except:
			return []
	r = []
	ls = J.subdivisions()[0]+[J.ncols()]
	# compute the max size of jordan blocs for the max eigenvalue
	ri = 0
	ms = 0
	for i in ls:
		if J[i-1][i-1] == vp:
			if ms < i-ri:
				ms = i-ri
		ri = i
	if verb:
		print("max size of Jordan blocs for the max eigenvalue : %s" % ms)
	# compute result vectors
	ri = 0
	for i in ls:
		if verb:
			print("%s" % (i-ri))
		if i-ri == ms and J[i-1][i-1] == vp:
			v = zero_vector(J.ncols())
			v[i-1] = 1
			v = v*P.inverse()
			if is_positive(v):
				r.append(v)
		ri = i
	return r

def mat_to_perm(m):
	r"""
	Convert a matrix of permutation to a permutation.

	INPUT:

		- ``m`` - matrix

	OUTPUT:
		A element of a SymmetricGroup.
	"""
	from sage.groups.perm_gps.permgroup_named import SymmetricGroup
	d = m.ncols()
	l = []
	for i in range(d):
		for j in range(d):
			if m[i][j] == 1:
				break
		else:
			raise ValueError("Cannot convert matrix to a permutation\n%s" % m)
		l.append(j)
	G = SymmetricGroup(range(d))
	return G(l)

def decompose_matrices(lm, verb=False):
	r"""
	Decompose simultaneously a list of matrices as product of transvections and permutations.
	Return a list of lists. The last list is the missing letter (None if there is none).
	Used by function matrices_to_winlose_with_permutations().
	"""
	from copy import copy
	if len(lm) == 0:
		return []
	if verb:
		print("decompose %s..." % lm)
	d = lm[0].ncols()
	for i in range(d):
		for j in range(i):
			# try to compare lines i and j
			l = [(i,j,[]), (j,i,[])]
			r = []
			ok = True
			for k,m in enumerate(lm):
				if is_positive(m[i]-m[j]):
					l[1][2].append(k)
					r.append([j])
				elif is_positive(m[j]-m[i]):
					l[0][2].append(k)
					r.append([i])
				else:
					ok = False
					break
			if not ok:
				continue
			if verb:
				print("%s and %s comparables" % (i,j))
				print("%s" % l)
			if len(l[0][2]) == 0 or len(l[1][2]) == 0:
				raise ValueError("The continued fraction algorithm is not plain.")
			for i2,j2,l2 in l:
				lk = []
				lm2 = []
				for k in l2:
					m2 = copy(lm[k])
					m2[j2] -= m2[i2]
					if sum(m2.coefficients()) > d:
						lm2.append(m2)
						lk.append(k)
					else:
						r[k].append(mat_to_perm(m2))
				if len(lm2) > 0:
					r2 = decompose_matrices(lm2, verb=verb)
					if len(r2) == 0:
						ok = False
						break
					for h,k in enumerate(lk):
						r[k] += r2[h]
			if ok:
				return r
	if verb:
		print("aucune comparaison")
		print(lm)
	return []

def edges_from_list (i, l, lf, C):
	r"""
	Used by function matrices_to_winlose_with_permutations().
	i : initial state
	l : list of edges labels
	lf : final states for each edge
	C : C[0] is the current number to use
	"""
	d = dict()
	r = []
	J = [l2[0] for l2 in l]
	for k,j in enumerate(J):
		if j not in d:
			d[j] = []
		d[j].append(k)
	l2 = []
	for j in d:
		if len(d[j]) == 1:
			c = C[0]
			C[0] += 1
			r.append((i, l[d[j][0]][0], (i,c)))
			r.append(((i,c), l[d[j][0]][1], lf[d[j][0]]))
		else:
			l2 = []
			lf2 = []
			for k in d[j]:
				l2.append(l[k][1:])
				lf2.append(lf[k])
			c = C[0]
			C[0] += 1
			r.append((i, j, (i,c)))
			r += edges_from_list((i,c), l2, lf2, C)
	return r

def test_memleak (n=10):
	r"""
	Do a memory leak, with size n.

	INPUT:

	-``n`` -- size of the leak

	OUTPUT:

	None

	EXAMPLES::

	sage: from badic.cautomata import test_memleak
	sage: test_memleak(30000)					   # not tested
	"""
	sig_on()
	TestMemLeak(n)
	sig_off()

cdef imagDict(dict d, list A, list A2=[]):
	"""
	Dictionary which is numbering projected alphabet
	"""
	d1 = {}
	i = 0
	for a in A:
		if a in d:
			if d[a] not in d1:
				d1[d[a]] = i
				A2.append(d[a])
				i += 1
	return d1

cdef imagDict2(dict d, list A, list A2=[]):
	"""
	Dictionary which is numbering a new alphabet
	"""
	# print("d=%s, A=%s"%(d,A))
	d1 = {}
	i = 0
	for a in A:
		if a in d:
			for v in d[a]:
				if v not in d1:
					d1[v] = i
					A2.append(v)
					i += 1
	return d1

cdef Dict getDict(dict d, list A, dict d1=None):
	A = list(A)
	cdef Dict r
	r = NewDict(len(A))
	cdef int i
	if d1 is None:
		d1 = imagDict(d, A)
	# print d1
	for i in range(r.n):
		if A[i] in d:
			r.e[i] = d1[d[A[i]]]
		else:
			r.e[i] = -1
	return r

cdef Dict list_to_Dict(l):
	cdef Dict d = NewDict(len(l))
	cdef int i
	for i,e in enumerate(l):
		d.e[i] = e
	return d

cdef InvertDict getDict2(dict d, list A, dict d1=None):
	A = list(A)
	cdef InvertDict r
	r = NewInvertDict(len(A))
	cdef int i
	if d1 is None:
		d1 = imagDict2(d, A)
	# print(d1)
	for i in range(r.n):
		if A[i] in d:
			r.d[i] = NewDict(len(d[A[i]]))
			for j in range(r.d[i].n):
				r.d[i].e[j] = d1[d[A[i]][j]]
		else:
			r.d[i].n = 0
	return r

cdef imagProductDict(dict d, list A1, list A2, list Av=[]):
	"""
	Dictionary numbering the projected alphabet
	"""
	dv = {}
	i = 0
	for a1 in A1:
		for a2 in A2:
			if (a1, a2) in d:
				if d[(a1, a2)] not in dv:
					dv[d[(a1, a2)]] = i
					Av.append(d[(a1, a2)])
					i += 1
	return dv

cdef Dict getProductDict(dict d, list A1, list A2, dict dv=None, int verb=True):
	cdef Dict r
	d1 = {}
	d2 = {}
	cdef int i, n1, n2
	n1 = len(A1)
	for i in range(n1):
		d1[A1[i]] = i
	if verb > 0:
		print(d1)
	n2 = len(A2)
	for i in range(n2):
		d2[A2[i]] = i
	if verb > 0:
		print(d2)
	if dv is None:
		dv = imagProductDict(d, A1, A2)
	r = NewDict(n1*n2)
	Keys = d.keys()
	if verb > 0:
		print("Keys=%s" % Keys)
	for (a1, a2) in Keys:
		if a1 in d1 and a2 in d2:
			r.e[d1[a1]+d2[a2]*n1] = dv[d[(a1, a2)]]
	return r

#def empty_automaton (A,S, keep_labels=True):
#	return empty_automaton_(A,S,keep_labels=keep_labels)

def vertices(g):
	try:
		return g.vertices(sort=False)
	except:
		return g.vertices()

cdef Automaton getAutomaton(a, initial=None, F=None, A=None):
	d = {}
	da = {}
	if F is None:
		if not hasattr(a, 'F'):
			try:
				F = vertices(a)
			except AttributeError:
					raise AttributeError("No vertices() method")
		else:
			F = a.F
	cdef Automaton r

	if A is None:
		A = list(set(a.edge_labels()))
	V = list(vertices(a))

	cdef int n = len(V)
	cdef int na = len(A)

	sig_on()
	r = NewAutomaton(n, na)
	initAutomaton(&r)
	sig_off()
	for i in range(na):
		da[A[i]] = i
	for i in range(n):
		r.e[i].final = 0
		d[V[i]] = i
	for v in F:
		#if not d.has_key(v):
		#	sig_on()
		#	FreeAutomaton(&r)
		#	r = NewAutomaton(0, 0)
		#	sig_off()
		#	raise ValueError("Incorrect set of final states.")
		if v in d:
			r.e[d[v]].final = 1

	if initial is None:
		if not hasattr(a, 'I'):
			I = []
		else:
			I = list(a.I)
		if len(I) > 1:
			raise ValueError("The automata must be determistic (I=%s)" % I)
		if len(I) >= 1:
			r.i = d[I[0]]
		else:
			r.i = -1
	else:
		if initial in d:
			r.i = d[initial]
		else:
			r.i = -1

	w = False
	for e, f, l in a.edges(sort=False):
		if r.e[d[e]].f[da[l]] != -1:
			if not w:
				print("Warning: the automaton was not deterministic! (edge %s -%s-> %s and edge %s -%s-> %s)\nThe result lost some informations."% (d[e], l, r.e[d[e]].f[da[l]], d[e], l, d[f]))
			w = True
		r.e[d[e]].f[da[l]] = d[f]
	#a.dA = da
	a.S = V
	#a.dS = d
	return r

cdef NAutomaton getNAutomaton(a, I=None, F=None, A=None, verb=False):
	d = {}
	da = {}
	dt = {}
	if F is None:
		if not hasattr(a, 'F'):
			F = vertices(a)
		else:
			F = a.F
	if I is None:
		if not hasattr(a, 'I'):
			I = vertices(a)
		else:
			I = a.I
	cdef NAutomaton r

	if A is None:
		A = list(a.Alphabet)

	V = list(vertices(a))
	cdef int n = len(V)
	cdef int na = len(A)

	if verb:
		print("vertices=%s" % V)

	if verb:
		print("Alloc(%s, %s) and init..." % (n, na))
	sig_on()
	r = NewNAutomaton(n, na)
	# initNAutomaton(&r)
	sig_off()
	if verb:
		print("init...")
	for i in range(n):
		d[V[i]] = i
		dt[V[i]] = []
	for i in range(na):
		da[A[i]] = i
	if verb:
		print("final states...")
	for v in F:
		if v not in d:
			sig_on()
			FreeNAutomaton(&r)
			sig_off()
			raise ValueError("Incorrect set of final states.")
		if verb:
			print("d[%s]=%s" % (v, d[v]))
		r.e[d[v]].final = 1
	if verb:
		print("initial states...")
	for v in I:
		if v not in d:
			sig_on()
			FreeNAutomaton(&r)
			sig_off()
			raise ValueError("Incorrect set of final states.")
		if verb:
			print("d[%s]=%s" % (v, d[v]))
		r.e[d[v]].initial = 1

	if verb:
		print(dt)
		print("browse transitions...")

	for e, f, l in a.edges(sort=False):
		if e in dt:
			dt[e].append((f, l))
		else:
			raise MemoryError("This transition %s -%s-> %s starts from a state not in the set of states."%(e, l, f))
	if verb:
		print(dt)
		print("Alloc...")
	for e in V:
		r.e[d[e]].n = len(dt[e])
		if verb:
			print("alloc r.e[%s].a (%s elements, %so total)" % (d[e], r.e[d[e]].n, sizeof(Transition)*r.e[d[e]].n))
		if r.e[d[e]].n > 0:
			r.e[d[e]].a = <Transition *>malloc(sizeof(Transition)*r.e[d[e]].n)
			if r.e[d[e]].a is NULL:
				raise MemoryError("Could not allocate the transition (size %s)"%(sizeof(Transition)*len(dt[e])))
			for i, (f, l) in enumerate(dt[e]):
				if verb:
					print("r.e[%s].a[%s] = %s,%s" % (d[e], i, l, f))
				r.e[d[e]].a[i].l = da[l]
				r.e[d[e]].a[i].e = d[f]
		else:
			r.e[d[e]].a = NULL
			r.e[d[e]].n = 0
	a.S = V
	return r

#cdef AutomatonGet(Automaton a, A):
#	"""
#	Transform an Automaton a with an alphabet A to a DiGraph
#	"""
#	from sage.graphs.digraph import DiGraph
#	r = DiGraph(multiedges=True, loops=True)
#	cdef int i, j
#	r.F = []
#	for i in range(a.n):
#		for j in range(a.na):
#			if a.e[i].f[j] != -1:
#				r.add_transition((i, a.e[i].f[j], A[j]))
#		if a.e[i].final:
#			r.F.append(i)
#	r.I = [a.i]
#	return r

cdef AutomatonToSageAutomaton(Automaton a, A):
	from sage.combinat.finite_state_machine import Automaton as SageAutomaton
	L = []
	if a.i == -1:
		I = []
	else:
		I = [a.i]
	F = []
	cdef int i, j
	for i in range(a.n):
		for j in range(a.na):
			if a.e[i].f[j] != -1:
				L.append((i, a.e[i].f[j], A[j]))
		if a.e[i].final:
			F.append(i)
	return SageAutomaton(L, initial_states=I, final_states=F)

cdef AutomatonToDiGraph(Automaton a, A, keep_edges_labels=True):
	from sage.graphs.digraph import DiGraph
	cdef int i, j
	cdef list L
	L = []
	for i in range(a.n):
		for j in range(a.na):
			if a.e[i].f[j] != -1:
				if keep_edges_labels:
					L.append((i, a.e[i].f[j], A[j]))
				else:
					L.append((i, a.e[i].f[j]))
	return DiGraph([list(range(a.n)), L], loops=True, multiedges=True)
	
cdef AutomatonToGraph(Automaton a, A, keep_edges_labels=False, multiedges=False):
	from sage.graphs.digraph import DiGraph
	cdef int i, j
	cdef list L
	L = []
	for i in range(a.n):
		for j in range(a.na):
			if a.e[i].f[j] != -1:
				if keep_edges_labels:
					L.append((i, a.e[i].f[j], A[j]))
				else:
					L.append((i, a.e[i].f[j]))
	return Graph([list(range(a.n)), L], loops=True, multiedges=multiedges)

def PIL_to_display(im):
	from sage.repl.image import Image as Image2
	im2 = Image2.__new__(Image2)
	im2._pil = im
	return im2

def png_to_display (file):
	from PIL import Image
	im = Image.open(file)
	return PIL_to_display(im)

cdef class CAutomaton:
	"""
	Class :class:`CAutomaton`, this class encapsulates a C structure
	for Automata and implement methods to manipulate them.

	INPUT:

	- ``a`` -- an automaton given as a list, a DiGraph, an Automaton, a DetAutomaton or a CAutomaton

	- ``I`` -- list (default: None)
		The set of initial states.

	- ``F`` -- list (default: None)
		The set of final states.

	- ``A``-- list (default: None)
		The alphabet.

	- ``keep_S`` -- Bool (default: True)
		Keep the labels of the states.

	- ``verb`` -- Bool (default: False)
		Display informations for debugging.

	OUTPUT:

	Return a instance of :class:`CAutomaton`.

	EXAMPLES::

		sage: from badic.cautomata import *
		sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
		sage: b = CAutomaton(a)
		sage: b
		CAutomaton with 4 states and an alphabet of 2 letters
		sage: c = Automaton({0:{1:'x',2:'z',3:'a'}, 2:{5:'o'}},initial_states=[0])
		sage: b = CAutomaton(c)
		sage: b
		CAutomaton with 5 states and an alphabet of 4 letters
		sage: d = DiGraph({0: [1,2,3], 1: [0,2]})
		sage: b = CAutomaton(d)
		sage: b
		CAutomaton with 4 states and an alphabet of 1 letter
		sage: g =  CAutomaton([(0,1,'a') ,(2,3,'b')])
		sage: g
		CAutomaton with 4 states and an alphabet of 2 letters

	"""
	def __cinit__(self):
		self.a = <NAutomaton *>malloc(sizeof(NAutomaton))
		if self.a is NULL:
			raise MemoryError("Failed to allocate memory for "
							  "C initialization of CAutomaton.")
		self.a.e = NULL
		self.a.n = 0
		self.a.na = 0
		self.A = []
		self.S = None

	def __init__(self, a, I=None, F=None, A=None, keep_S=True, verb=False):
		"""
		TESTS::

			sage: from badic.cautomata import *
			sage: CAutomaton([(0, 1, 'a'), (2, 3, 'b')], I=[1])
			CAutomaton with 4 states and an alphabet of 2 letters
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=1)
			sage: a = CAutomaton(a)
			sage: a
			CAutomaton with 4 states and an alphabet of 2 letters
			sage: CAutomaton(a)
			CAutomaton with 4 states and an alphabet of 2 letters

		"""
		cdef DetAutomaton da
		cdef CAutomaton na
		if a is None:
			if verb:
				print("a is None")
			return
		from sage.graphs.digraph import DiGraph
		from sage.combinat.finite_state_machine import Automaton as SageAutomaton
		if isinstance(a, SageAutomaton):
			if verb:
				print("Sage automaton...")
			L = []
			for t in a.transitions():
				if len(t.word_in) != 1 or len(t.word_out) != 0:
					print("Warning: informations lost during the conversion.")
				L.append((t.from_state, t.to_state, t.word_in[0]))
			if I is None:
				I = a.initial_states()
			if F is None:
				F = a.final_states()
			a = L
		if isinstance(a, list):
			if verb:
				print("List...")
			a = DiGraph(a, multiedges=True, loops=True)
		if isinstance(a, DiGraph):
			if verb:
				print("DiGraph...")
			SL = set(a.edge_labels())
			if A is None:
				A = list(SL)
			else:
				# test that A contains the labels
				if not SL.issubset(A):
					raise ValueError("A label of a transition is not in the alphabet %s"%A)
			self.A = A
			if verb:
				print("getNAutomaton(%s, I=%s, F=%s, A=%s)" % (a, I, F, self.A))
			self.a[0] = getNAutomaton(a, I=I, F=F, A=self.A, verb=verb)
			if keep_S:
				self.S = a.S
		elif isinstance(a, CAutomaton):
			if verb:
				print("CAutomaton")
			na = a
			sig_on()
			self.a[0] = CopyNAutomaton(na.a[0], na.a.n, na.a.na)
			sig_off()
			self.A = na.A
			self.S = na.S
		elif isinstance(a, DetAutomaton):
			if verb:
				print("DetAutomaton")
			da = a
			self.a[0] = CopyN(da.a[0], verb=verb)
			self.A = da.A
		else:
			raise ValueError("Cannot convert the input to CAutomaton.")

	def __dealloc__(self):
		sig_on()
		if self.a is not NULL:
			FreeNAutomaton(self.a)
			free(self.a)
		sig_off()

	def __repr__(self):
		r"""
		Return a representation of the automaton.

		OUTPUT:

		A string.

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: from badic.cautomata import *
			sage: CAutomaton(dag.AnyWord([0,1]))
			CAutomaton with 1 state and an alphabet of 2 letters

		"""
		str = "CAutomaton with %d state"%self.a.n
		if self.a.n > 1:
			str += 's'
		str += " and an alphabet of %d letter"%self.a.na
		if self.a.na > 1:
			str += 's'
		return str

	def _latex_(self):
		r"""
		Return a latex representation of the automaton.

		TESTS::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: b = CAutomaton(a)
			sage: latex(b)	 # not tested
			\documentclass{article}
			\usepackage[x11names, rgb]{xcolor}
			\usepackage[utf8]{inputenc}
			\usepackage{tikz}
			\usetikzlibrary{snakes,arrows,shapes}
			\usepackage{amsmath}
			%
			%
			<BLANKLINE>
			%
			<BLANKLINE>
			%
			<BLANKLINE>
			\begin{document}
			\pagestyle{empty}
			%
			%
			%
			<BLANKLINE>
			\enlargethispage{100cm}
			% Start of code
			% \begin{tikzpicture}[anchor=mid,>=latex',line join=bevel,]
			\begin{tikzpicture}[>=latex',line join=bevel,]
			  \pgfsetlinewidth{1bp}
			%%
			\pgfsetcolor{black}
			  % Edge: 0 -> 1
			  \draw [-stealth'] (44.247bp,22.0bp) .. controls (54.848bp,22.0bp) and (67.736bp,22.0bp)  .. (89.697bp,22.0bp);
			  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
			  \pgfsetstrokecolor{strokecol}
			  \draw (67.0bp,33.0bp) node {a};
			  % Edge: 2 -> 3
			  \draw [-stealth'] (44.247bp,80.0bp) .. controls (54.848bp,80.0bp) and (67.736bp,80.0bp)  .. (89.697bp,80.0bp);
			  \draw (67.0bp,91.0bp) node {b};
			  % Node: 1
			\begin{scope}
			  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
			  \pgfsetstrokecolor{strokecol}
			  \draw [solid] (112.0bp,22.0bp) ellipse (18.0bp and 18.0bp);
			  \draw [solid] (112.0bp,22.0bp) ellipse (22.0bp and 22.0bp);
			  \draw (112.0bp,22.0bp) node {1};
			\end{scope}
			  % Node: 0
			\begin{scope}
			  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
			  \pgfsetstrokecolor{strokecol}
			  \draw [solid] (22.0bp,22.0bp) ellipse (18.0bp and 18.0bp);
			  \draw [solid] (22.0bp,22.0bp) ellipse (22.0bp and 22.0bp);
			  \draw (22.0bp,22.0bp) node {0};
			\end{scope}
			  % Node: 3
			\begin{scope}
			  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
			  \pgfsetstrokecolor{strokecol}
			  \draw [solid] (112.0bp,80.0bp) ellipse (18.0bp and 18.0bp);
			  \draw [solid] (112.0bp,80.0bp) ellipse (22.0bp and 22.0bp);
			  \draw (112.0bp,80.0bp) node {3};
			\end{scope}
			  % Node: 2
			\begin{scope}
			  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
			  \pgfsetstrokecolor{strokecol}
			  \draw [solid] (22.0bp,80.0bp) ellipse (18.0bp and 18.0bp);
			  \draw [solid] (22.0bp,80.0bp) ellipse (22.0bp and 22.0bp);
			  \draw (22.0bp,80.0bp) node {2};
			\end{scope}
			%
			\end{tikzpicture}
			% End of code
			<BLANKLINE>
			%
			\end{document}
			%
			<BLANKLINE>
			<BLANKLINE>
			<BLANKLINE>
		"""
		sx = 800
		sy = 600
		from sage.misc.latex import LatexExpr
		cdef char *file
		from sage.misc.temporary_file import tmp_filename
		file_name = tmp_filename()+".dot"
		file = file_name
		try:
			from dot2tex import dot2tex
		except ImportError:
			raise RuntimeError("dot2tex must be installed in order to have the LaTeX representation of the CAutomaton.\n\
				You can install it by doing './sage -i dot2tex' in a shell in the sage directory, or by doing 'install_package(package='dot2tex')' in the notebook.")
		cdef char** ll
		ll = <char **> malloc(sizeof(char*) * self.a.na)
		if ll is NULL:
			raise MemoryError("Failed to allocate memory for ll in "
							  "_latex_")
		cdef int i
		strA = []
		for i in range(self.a.na):
			strA.append(str_to_bytes(str(self.A[i])))
			ll[i] = strA[i]
		sig_on()
		NplotDot(file, self.a[0], ll, "Automaton", sx, sy, False)
		free(ll)
		sig_off()
		dotfile = open(file_name)
		return LatexExpr(dot2tex(dotfile.read()))

	def copy(self):
		"""
		Do a copy of the :class:`CAutomaton`.

		OUTPUT:

		Return a copy of the :class:`CAutomaton`

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = CAutomaton([(0, 1, 'a'), (2, 3, 'b')], I=[0])
			sage: a.copy()
			CAutomaton with 4 states and an alphabet of 2 letters

		"""
		r = CAutomaton(None)
		sig_on()
		r.a[0] = CopyNAutomaton(self.a[0], self.a.n, self.a.na)
		sig_off()
		r.A = self.A[:]
		return r

	@property
	def n_states(self):
		"""
		return the numbers of states

		OUTPUT:

		return the numbers of states

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = CAutomaton(a)
			sage: b.n_states
			4
		"""
		return self.a.n

	def n_succs(self, int i):
		"""
		INPUT:

		- ``i`` -- int successor number

		OUTPUT:

		return the numbers of successor of state ``i``
		(i.e. the number of leaving transitions from state ``i``)

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = CAutomaton(a)
			sage: b.n_succs(0)
			1
		"""
		if i >= self.a.n or i < 0:
			raise ValueError("There is no state %s !" % i)
		return self.a.e[i].n

	def succ(self, int i, int j):
		"""
		Give the state at end of the ``j`` th edge from the state ``i``

		INPUT:

		- ``i`` int state number
		- ``j`` int edge number

		OUTPUT:

		return the state at end of the ``j``th edge of the state ``i``

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'),(1, 2, 'c'), (2, 3, 'b')], i=0)
			sage: b = CAutomaton(a)
			sage: b.succ(1, 0)
			2
		"""
		if i >= self.a.n or i < 0:
			raise ValueError("There is no state %s !" % i)
		if j >= self.a.e[i].n or j < 0:
			raise ValueError("The state %s has no edge number %s !" % (i, j))
		return self.a.e[i].a[j].e

	def label(self, int i, int j):
		"""
		Give the label of the ``j``th edge of the state ``i``

		INPUT:

		-``i`` -- int state number
		-``j`` -- int edge number

		OUTPUT:

		return the label index of the ``j``th edge of the state ``i``

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'),(1, 2, 'c'), (2, 3, 'b')], i=0)
			sage: b = CAutomaton(a)
			sage: b.label(1, 0) == b.alphabet.index('c')
			True
		"""
		if i >= self.a.n or i < 0:
			raise ValueError("There is no state %s !" % i)
		if j >= self.a.e[i].n or j < 0:
			raise ValueError("The state %s has no edge number %s !" % (i, j))
		return self.a.e[i].a[j].l

	@property
	def states(self):
		"""
		Indicate all states of the automaton

		OUTPUT:

		Return the list of states

		EXAMPLES::

			sage: from badic import *
			sage: a = CAutomaton(dag.Word(['a', 'b', None]))
			sage: a.states
			[0, 1, 2, 3]
		"""
		if self.S is None:
			return list(range(self.a.n))
		else:
			return self.S

	def is_final(self, int i):
		"""
		Return ``True`` if the state``i`` is  final, ``False`` otherwise.

		INPUT:

		-``i`` -- int state number

		OUTPUT:

		Return ``True`` if the state``i`` is  final, ``False`` otherwise.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'),(1, 2, 'c'), (2, 3, 'b')], i=0)
			sage: b = CAutomaton(a)
			sage: b.is_final(1)
			True
		"""
		if i >= self.a.n or i < 0:
			raise ValueError("There is no state %s !" % i)
		sig_on()
		ans = c_bool(self.a.e[i].final)
		sig_off()
		return ans

	def final(self, int i):
		"""
		Return the final value of state ``i``.

		INPUT:

		-``i`` -- int state number

		OUTPUT:
			A int.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'),(1, 2, 'c'), (2, 3, 'b')], i=0)
			sage: b = CAutomaton(a)
			sage: b.final(1)
			1
		"""
		if i >= self.a.n or i < 0:
			raise ValueError("There is no state %s !" % i)
		return self.a.e[i].final

	def is_initial(self, int i):
		"""
		Return `True`` if state ``i`` is initial, ``False`` otherwise.

		INPUT:

		-``i`` -- int state number

		OUTPUT:

		Return `True`` if state ``i`` is initial, ``False`` otherwise.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'),(1, 2, 'c'), (2, 3, 'b')], i=0)
			sage: b = CAutomaton(a)
			sage: b.is_initial(1)
			False

		TESTS::

			sage: from badic import *
			sage: a = CAutomaton(dag.AnyWord([0,1]))
			sage: a.is_initial(2)
			Traceback (most recent call last):
			...
			ValueError: There is no state 2 !
		"""
		if i >= self.a.n or i < 0:
			raise ValueError("There is no state %s !" % i)
		sig_on()
		answ = c_bool(self.a.e[i].initial)
		sig_off()
		return answ

	def initial(self, int i):
		"""
		Return the initial value of state ``i``.

		INPUT:

		-``i`` -- int state number

		OUTPUT:
			A int.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'),(1, 2, 'c'), (2, 3, 'b')], i=0)
			sage: b = CAutomaton(a)
			sage: b.initial(1)
			0

		TESTS::

			sage: from badic.cautomata import *
			sage: from badic.cautomata_generators import *
			sage: a = CAutomaton(dag.AnyWord([0,1]))
			sage: a.initial(2)
			Traceback (most recent call last):
			...
			ValueError: There is no state 2 !
		"""
		if i >= self.a.n or i < 0:
			raise ValueError("There is no state %s !" % i)
		return self.a.e[i].initial

	@property
	def initial_states(self):
		"""
		Return the set of initial states of the :class:`CAutomaton`

		OUTPUT:

		list

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: b = CAutomaton(a)
			sage: b.initial_states
			[]
			sage: a = DetAutomaton([(10,11,'a') ,(12,13,'b')], i=12)
			sage: b = CAutomaton(a)
			sage: b.initial_states
			[2]
			sage: a = DetAutomaton([(10,11,'a') ,(12,13,'b')], i=0).mirror()
			sage: a.initial_states
			[0, 1, 2, 3]
		"""
		cdef int i
		l = []
		for i in range(self.a.n):
			if self.a.e[i].initial:
				l.append(i)
		return l

	@property
	def final_states(self):
		"""
		Return the set of final states

		OUTPUT:

		list

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(10,11,'a') ,(12,13,'b')])
			sage: b = CAutomaton(a)
			sage: b.final_states
			[0, 1, 2, 3]
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')], )
			sage: b = CAutomaton(a)
			sage: b.final_states
			[0, 1, 2, 3]
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')], final_states=[0,3])
			sage: b = CAutomaton(a)
			sage: b.final_states
			[0, 3]
			sage: a = DetAutomaton([(10,10,'x'),(10,20,'y'),(20,20,'z'),\
				(20,10,'y'),(20,30,'x'),(30,30,'y'),(30,10,'z'),(30,20,'x'),\
				(10,30,'z')], i=10)
			sage: a.final_states
			[0, 1, 2]
		"""
		l = []
		for i in range(self.a.n):
			if self.a.e[i].final:
				l.append(i)
		return l

	@property
	def alphabet(self):
		"""
		To get the :class:`CAutomaton` attribut alphabet
		Return the alphabet of the :class:`CAutomaton`

		OUTPUT:

		list

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: b = CAutomaton(a)
			sage: sorted(b.alphabet)
			['a', 'b']
			sage: a = DetAutomaton([(10,10,'x'),(10,20,'y'),(20,20,'z'),\
				(20,10,'y'),(20,30,'x'),(30,30,'y'),(30,10,'z'),(30,20,'x'),\
				(10,30,'z')], i=10)
			sage: sorted(a.alphabet)
			['x', 'y', 'z']
		"""
		return self.A

	def set_final (self, int i, int final=True):
		"""
		Set the state as a final/non-final state.

		INPUT:

		- ``i`` -- int - the index of the state

		- ``final`` -- int (default: ``True``) - if True set this state as final set, otherwise set this state as non-final.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(10,11,'a') ,(12,13,'b')], i=10, final_states=[])
			sage: b = CAutomaton(a)
			sage: b.set_final(2)
			sage: b.final_states
			[2]

		TESTS::

			sage: b.set_final(6)
			Traceback (most recent call last):
			...
			ValueError: 6 is not an index of a state (must be between 0 and 3)

		"""
		if i < 0 or i >= self.a.n:
			raise ValueError("%s is not an index of a state (must be between "%i +
							 "0 and %s)" % (self.a.n - 1))
		self.a.e[i].final = final
	
	def set_final_states (self, F):
		"""
		Set the set of final states.

		INPUT:

		- ``F`` -- list - the list of final states

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(10,11,'a') ,(12,13,'b')], i=10, final_states=[])
			sage: b = CAutomaton(a)
			sage: b.set_final_states([2])
			sage: b.final_states
			[2]

		TESTS::

			sage: b.set_final_states([6])
			Traceback (most recent call last):
			...
			ValueError: 6 is not an index of a state (must be between 0 and 3)

		"""
		cdef int k
		for k in F:
			if k < 0 or k >= self.a.n:
				raise ValueError("%s is not an index of a state (must be between "%k +
							 "0 and %s)" % (self.a.n - 1))
		for k in range(self.a.n):
			self.a.e[k].final = 0
		for k in F:
			self.a.e[k].final = 1

	def set_initial (self, int i, bint initial=True):
		"""
		Set the state as an initial/non initial state.

		INPUT:

		- ``i`` -- int - the index of the state
		- ``initial`` -- (default: ``True``) - if True set this state as initial set,
		  otherwise set this state as non-initial.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: b = CAutomaton(a)
			sage: b.set_initial(2)
			sage: b.initial_states
			[2]
			sage: b.set_initial(6)
			Traceback (most recent call last):
			...
			ValueError: i=6 is not the index of a state (i.e. between 0 and 3).

		"""
		if i < 0 or i >= self.a.n:
			raise ValueError("i=%s is not the index of a state (i.e. between 0 and %s)."
							 % (i, self.a.n - 1))
		self.a.e[i].initial = initial
	
	def set_initial_states (self, I):
		"""
		Set the set of initial states.

		INPUT:

		- ``I`` -- list - the list of initial states

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(10,11,'a') ,(12,13,'b')], i=10, final_states=[])
			sage: b = CAutomaton(a)
			sage: b.set_initial_states([2])
			sage: b.initial_states
			[2]

		TESTS::

			sage: b.set_initial_states([6])
			Traceback (most recent call last):
			...
			ValueError: 6 is not an index of a state (must be between 0 and 3)

		"""
		cdef int k
		for k in I:
			if k < 0 or k >= self.a.n:
				raise ValueError("%s is not an index of a state (must be between "%k +
							 "0 and %s)" % (self.a.n - 1))
		for k in range(self.a.n):
			self.a.e[k].initial = 0
		for k in I:
			self.a.e[k].initial = 1

	def proj (self, dict d, A=None):
		"""
		Replace labels of transitions by their images by d.
		"""
		if A is None:
			A = set()
			for a in self.A:
				A.add(d[a])
			A = list(A)
		cdef int i,j
		for i in range(self.a.n):
			for j in range(self.a.e[i].n):
				self.a.e[i].a[j].l = A.index(d[self.A[self.a.e[i].a[j].l]])
		self.A = A
		self.a.na = len(A)

	def proji(self, int i):
		"""
		Assuming that letters of the automaton are iterables,
		project ON PLACE on the ith coordinate.
		Replace labels of transitions by their projection on
		the ith coordinate.

		INPUT:

		- ``i`` -- int - coordinate of projection

		EXAMPLES::

			sage: from badic import *
			sage: a = CAutomaton(dag.Word([('a', 'b'), ('b', 'a'), ('c', 'a')]))
			sage: a.proji(1)
			sage: a
			CAutomaton with 4 states and an alphabet of 2 letters

			sage: a = CAutomaton(DetAutomaton([(0, 1, 'abc')], i=0))
			sage: a.proji(0)
			sage: a
			CAutomaton with 2 states and an alphabet of 1 letter
			sage: a == CAutomaton(DetAutomaton([(1, 0, 'a')], i=1))
			False
		"""
		cdef dict d

		if i < 0:
			raise ValueError("index i=%d cannot be negative" % i)
		d = {}
		for l in self.A:
			if i < len(l):
				d[l] = l[i]
			else:
				raise ValueError("index i=%d must be smaller than the dimension of the label %s" % (i, l))
		self.proj(d)

	def add_transition(self, int i, l, int f):
		"""
		Add a transition from ``i`` to ``f`` labeled by ``l`` in the automaton.
		The states ``i`` and ``f`` must exist, and the label ``l`` must be in the alphabet.
		To add new states, use add_state(). To add letters in the alphabet use bigger_alphabet().

		INPUT:

		- ``i`` -- the first state
		- ``l`` -- the label of the edge
		- ``f`` -- the second state

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(10, 11, 'a'), (12, 13, 'b')], i=10)
			sage: b = CAutomaton(a)
			sage: b.add_transition(2,'a',1)
			sage: b.plot()  # not tested
			
		TESTS::
			
			sage: b.add_transition(2,'v',1)
			Traceback (most recent call last):
			...
			ValueError: The letter v doesn't exist.
			sage: b.add_transition(2,'v',6)
			Traceback (most recent call last):
			...
			ValueError: The state  6 doesn't exist.
			sage: b.add_transition(5,'v',6)
			Traceback (most recent call last):
			...
			ValueError: The state  5 doesn't exist.

		"""
		if i >= self.a.n or i < 0:
			raise ValueError("The state %s doesn't exist." % i)
		if f >= self.a.n or f < 0:
			raise ValueError("The state  %s doesn't exist." % f)
		try:
			k = self.A.index(l)
		except Exception:
			# La lettre %s n'existe pas.
			raise ValueError("The letter %s doesn't exist." % l)

		sig_on()
		AddTransitionN(self.a, i, f, k)
		sig_off()

	def add_state(self, bint final):
		"""
		Add a state in the automaton

		INPUT:

		- ``final`` -- Boolean indicate if the added state is final

		OUTPUT:

		return the numbers of states

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = CAutomaton(a)
			sage: b.add_state(True)  # not implemented
			TypeError								 Traceback (most recent call last)
			...
			TypeError: 'NotImplementedType' object is not callable
		"""
		raise NotImplemented()
	
	def mirror(self):
		"""
		Return the mirror of the CAutomaton (i.e. reverse transitions)
		"""
		raise NotImplementedError("Sorry, not yet implemented !")
	
	def bigger_alphabet(self, list nA):
		"""
		Increase the size of the alphabet

		INPUT:

		- ``nA`` -- list - new alphabet or new letters to add

		EXAMPLES::

			sage: from badic import *
			sage: a = CAutomaton(dag.AnyLetter([0, 1, 'a']))
			sage: set(a.alphabet) == {'a', 1, 0}
			True
			sage: a.bigger_alphabet(['b', 'c'])
			sage: set(a.alphabet) == {'a', 1, 0, 'c', 'b'}
			True
		"""
		self.A = self.A+list(set(nA).difference(self.A))

	def add_path(self, int e, int f, list li, verb=False):
		"""
		Add a path labeled by the list ``li`` from
		state ``e`` to state ``f`` of :class:`CAutomaton`

		INPUT:

		- ``e`` -- int the starting state
		- ``f`` -- int the ending state
		- ``li`` -- list of labels
		- ``verb`` -- Boolean (default: ``False``) if True, print debugging informations


		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a'), (2,3,'b')], i=2)
			sage: b = CAutomaton(a)
			sage: b.add_path(1, 2, ['a'])
			sage: b.plot()  # not tested

		"""
		cdef int *l = <int *>malloc(sizeof(int)*len(li))
		if l is NULL:
			raise MemoryError("Failed to allocate memory for l in "
							  "add_path")
		for i in range(len(li)):
			try:
				l[i] = self.A.index(li[i])
			except:
				raise ValueError("The label %s is not in the alphabet! Use bigger_alphabet() if you want to add it"%li[i])
		sig_on()
		AddPathN(self.a, e, f, l, len(li), verb)
		free(l)
		sig_off()

	def determinize(self, sink=False, verb=0):
		"""
		Compute a deterministic automaton that recognizes the same language as self.

		INPUT:

		- ``sink``  -- (default: ``False``) - give a complete automaton, with a sink state
		- ``verb`` -- Boolean (default: ``False``) if True, print debugging informations

		OUTPUT:

		Return a non determinist automaton  :class:`CAutomaton`

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = CAutomaton(a)
			sage: b.determinize()
			DetAutomaton with 2 states and an alphabet of 2 letters

			sage: a = CAutomaton([(0,0,0),(0,0,1),(0,1,1),(1,2,0),(1,2,1),(2,3,0),(2,3,1),(3,4,0),(3,4,1)], I=[0], F=[4])
			sage: a.determinize()
			DetAutomaton with 16 states and an alphabet of 2 letters

		"""
		cdef Automaton a
		r = DetAutomaton(None)
		sig_on()
		a = DeterminizeN(self.a[0], sink, verb)
		sig_off()
		r.a[0] = a
		r.A = self.A
		r.S = None
		return r

	def plot(self, file=None, int sx=10, int sy=8, verb=False):
		"""
		plot a representation of the :class:`CAutomaton`.

		INPUT:

		- ``sx`` -- int (default: 10) - width given to dot command
		- ``sy`` -- int (default: 8) - height given to dot command
		- ``verb`` -- Boolean (default: ``False``) if True, print debugging informations

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = CAutomaton(a)
			sage: b.plot()   # not tested

			sage: a = CAutomaton([(0,0,0),(0,0,1),(0,1,1),(1,2,0),(1,2,1),(2,3,0),(2,3,1),(3,4,0),(3,4,1)], I=[0], F=[4])
			sage: a.plot()   # not tested
			<PIL.PngImagePlugin.PngImageFile image mode=RGBA size=189x147 at 0x7F6711E442D0>
			
		.. PLOT::
		   :width: 50%

			a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			b = CAutomaton(a)
			sphinx_plot(b)

		"""
		cdef char** ll
		cdef int i
		cdef char *cfile
		cdef bint de
		if verb:
			print("Test if dot exists...")
		sig_on()
		de = DotExists ()
		sig_off()
		if de:
			if verb:
				print(" -> yes !")
				print("alloc ll (size=%s)..." % self.a.na)
			ll = <char **>malloc(sizeof(char*) * self.a.na)
			if ll is NULL:
				raise MemoryError("Failed to allocate memory for ll in "
								  "plot")
			strA = []
			for i in range(self.a.na):
				strA.append(str_to_bytes(str(self.A[i])))
				ll[i] = strA[i]
			if file is None:
				from sage.misc.temporary_file import tmp_filename
				file = tmp_filename()
			file = str_to_bytes(file)
			cfile = file
			if verb:
				print("file=%s" % file)
			sig_on()
			NplotDot(cfile, self.a[0], ll, "Automaton", sx, sy, True)
			free(ll)
			sig_off()
			if verb:
				print("Ouvre l'image produite...")
			return png_to_display(file+b'.png')
#			import ipywidgets as widgets
#			file = open(file+'.png', "rb")
#			image = file.read()
#			w = widgets.Image(
#				value=image,
#				format='png',
##				width=600,
##				height=400,
#			)
#			return w
		else:
			if verb:
				print(" -> no.")
			raise NotImplementedError("You cannot plot the CAutomaton without dot. Install the dot command of the GraphViz package.")


cdef class DetAutomaton:
	r"""
	Class :class:`DetAutomaton`, this class encapsulates a C structure
	for deterministic automata and implement methods to manipulate them.

	EXAMPLES::

		sage: from badic import *
		sage: DetAutomaton([(0,1,'a') ,(2,3,'b')])
		DetAutomaton with 4 states and an alphabet of 2 letters
		sage: d = DiGraph({0: [1,2,3], 1: [0,2]})
		sage: DetAutomaton(d)
		Warning: the automaton was not deterministic! (edge 0 -None-> 1 and edge 0 -None-> 2)
		The result lost some informations.
		DetAutomaton with 4 states and an alphabet of 1 letter
		sage: g = DiGraph({0:{1:'x',2:'z',3:'a'}, 2:{5:'o'}})
		sage: DetAutomaton(g)
		DetAutomaton with 5 states and an alphabet of 4 letters
		sage: a = DetAutomaton([(0, 1,'a') ,(2, 3,'b')], i = 2)
		sage: a
		DetAutomaton with 4 states and an alphabet of 2 letters
		sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')], final_states=[0,3])
		sage: a
		DetAutomaton with 4 states and an alphabet of 2 letters
		sage: b = DetAutomaton(a)
		sage: b
		DetAutomaton with 4 states and an alphabet of 2 letters
		sage: c = Automaton({0:{1:'x',2:'z',3:'a'}, 2:{5:'o'}},initial_states=[0])
		sage: b = DetAutomaton(c)
		sage: b
		DetAutomaton with 5 states and an alphabet of 4 letters
		sage: from badic.cautomata_generators import *
		sage: dag.AnyWord([0, 1, 'a'])
		DetAutomaton with 1 state and an alphabet of 3 letters
		sage: dag.Random(n=40, A=[None, -1, 1,2,3,'x','y','z'])  # random

	"""

#	cdef Automaton* a
#	cdef list A

	def __cinit__(self):
		"""

		"""
		# print("cinit")
		self.a = <Automaton *>malloc(sizeof(Automaton))
		if self.a is NULL:
			raise MemoryError("Failed to allocate memory for "
							  "C initialization of DetAutomaton.")
		# initialise
		self.a.e = NULL
		self.a.n = 0
		self.a.na = 0
		self.a.i = -1
		self.A = []
		self.A_inv = None
		self.S = None
		self.dA = None
		#self.dS = None

	def __init__(self, a, i=None, final_states=None,
				 A=None, S=None, avoidDiGraph=False, keep_S=True, verb=False):
		r"""
		INPUT:

		-``a`` - a list, a DiGraph, an Automaton, or a ``DetAutomaton``

		- ``i`` - (default: None) - initial state

		- ``final_states`` - (default: None) - list of final states

		- ``A`` - (default: None) - alphabet
		
		- ``S`` - (defualt: None) - set of states
		
		- ``avoidDiGraph`` - bool (default: True) - avoid using DiGraph, in order to avoid bugs

		- ``keep_S``- (default: `True`) - Keep labels of states or not

		OUTPUT:

		Return a instance of :class:`DetAutomaton`.

		TESTS::

			sage: from badic import *
			sage: DetAutomaton([(0,1,'a') ,(2,3,'b')])
			DetAutomaton with 4 states and an alphabet of 2 letters
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: DetAutomaton(a)
			DetAutomaton with 4 states and an alphabet of 2 letters
		"""
		cdef DetAutomaton da
		# print("init")
		self.A_inv = None
		if a is None:
			if A is not None:
				self.A = A
				self.a.na = len(A)
			if i is not None:
				print("Warning: Ignore i since there is no state.")
			if final_states is not None:
				print("Warning: Ignore final_states since there is no state.")
			if S is not None:
				print("Warning: Ignore S since there is no state.")
			return
		from sage.graphs.digraph import DiGraph
		from sage.combinat.finite_state_machine import Automaton as SageAutomaton
		if isinstance(a, CAutomaton):
			a = a.determinize().minimize()
		if isinstance(a, SageAutomaton):
			L = []
			for t in a.transitions():
				if len(t.word_in) != 1 or len(t.word_out) != 0:
					print("Warning: informations lost during the conversion.")
				L.append((t.from_state, t.to_state, t.word_in[0]))
			if i is None:
				I = a.initial_states()
				if len(I) == 1:
					i = I[0]
				else:
					print("Warning: the automaton has several initial states.")
			if final_states is None:
				final_states = a.final_states()
			a = L
		if isinstance(a, list):
			if verb:
				print("list...")
			if avoidDiGraph:
				if S is None:
					S = list(set([e for (e,j,f) in a]+[f for (e,j,f) in a]))
				if A is None:
					A = list(set([j for (e,j,f) in a]))
				self.A = A
				if keep_S:
					self.S = S
				sig_on()
				self.a[0] = NewAutomaton(len(S), len(A))
				initAutomaton(self.a)
				sig_off()
				dS = dict()
				for k,s in enumerate(S):
					dS[s] = k
				dA = dict()
				for k,j in enumerate(A):
					dA[j] = k
				pw = False
				for (e,k,f) in a:
					if not pw and self.a[0].e[dS[e]].f[dA[k]] != -1:
						pw = True
						print("Warning: informations lost during the conversion.")
					self.a[0].e[dS[e]].f[dA[k]] = dS[f]
				if i is not None:
					if i in dS:
						self.a[0].i = dS[i]
					else:
						self.a[0].i = -1
				if final_states is None:
					final_states = S
				for s in final_states:
					self.a[0].e[dS[s]].final = True
				return
			else:
				if S is None:
					a = DiGraph(a, multiedges=True, loops=True)
				else:
					a = DiGraph([S, a],
								format='vertices_and_edges',
								multiedges=True, loops=True)
		if isinstance(a, DiGraph):
			if verb:
				print("DiGraph...")
			SL = set(a.edge_labels())
			if A is None:
				A = list(SL)
			else:
				# test that A contains the labels
				if not SL.issubset(A):
					raise ValueError("A label of a transition is not in the alphabet %s"%A)
			self.A = A
			self.a[0] = getAutomaton(a, initial=i, F=final_states, A=self.A)
			#self.dA = a.dA
			if keep_S:
				self.S = vertices(a)
				#self.dS = a.dS
		elif isinstance(a, DetAutomaton):
			if verb:
				print("DetAutomaton...")
			da = a
			sig_on()
			self.a[0] = CopyAutomaton(da.a[0], da.a.n, da.a.na)
			sig_off()
			self.A = da.A
			self.S = da.S
		else:
			raise ValueError("Cannot convert the input to DetAutomaton.")

	def __dealloc__(self):
		"""
		Desalloc  Automaton  Overwrite built-in function

		TESTS::

		"""
		# print("free (%s States) "%self.a.n)
		sig_on()
		FreeAutomaton(self.a)
		# print("free self.a")
		free(self.a)
		sig_off()

	def __reduce__ (self):
		r"""
		Return a tuple of class_name to call, and parameters to recreate the DetAutomaton

		EXAMPLES::

			sage: from badic import *
			sage: save(dag.Word("test"), "test")
			sage: load("test")
			DetAutomaton with 5 states and an alphabet of 3 letters

		"""
		return (self.__class__, (self.edges(), self.a.i, self.final_states, self.A, self.S))

	def __contains__ (self, w):
		r"""
		Test if the word is in the language of the automaton.
		"""
		cdef int i,j
		if self.a.i == -1:
			return False
		if self.dA is None:
			self.dA = dict()
			for i,a in enumerate(self.A):
				self.dA[a] = i
		i = self.a.i
		for a in w:
			i = self.a.e[i].f[self.dA[a]]
			if i == -1:
				return False
		return self.a.e[i].final

	def new_empty_automaton (self, list A, list S, bint keep_labels=True):
		cdef DetAutomaton r
		cdef int n, na
		n = len(S)
		na = len(A)
		r = DetAutomaton(None)
		sig_on()
		r.a[0] = NewAutomaton(n, na)
		initAutomaton(r.a)
		sig_off()
		r.A = A
		if keep_labels:
			r.S = S
		return r

	def copy(self):
		"""
		Do a copy of the :class:`DetAutomaton`.

		OUTPUT:

		Return a copy of the :class:`DetAutomaton`

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.copy()
			DetAutomaton with 4 states and an alphabet of 2 letters
		
		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = dag.AnyLetter(['a', 'b'])
			sage: b = a.copy()
			sage: a == b
			True
			sage: a.alphabet.append(0)
			sage: a == b
			False
			sage: a = b.copy()
			sage: a.delete_state_op(0)
			sage: a == b
			False

		"""
		cdef DetAutomaton r 
		r = DetAutomaton(None)
		sig_on()
		r.a[0] = CopyAutomaton(self.a[0], self.a.n, self.a.na)
		sig_off()
		if self.S is not None:
			r.S = self.S[:]
		r.A = self.A[:]
		return r

	def __copy__(self):
		"""
		Return a copy of the :class:`DetAutomaton`.
		Overwrite builtin function.
		
		See copy() for more details.
		"""
		return self.copy()

	def __repr__(self):
		"""
		Return a representation of automaton,  Overwrite built-in function

		OUTPUT:

		Return a representation of automaton

		TESTS:

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: repr(a)
			'DetAutomaton with 4 states and an alphabet of 2 letters'

		"""
		str = "DetAutomaton with %d state"%self.a.n
		if self.a.n > 1:
			str += 's'
		str += " and an alphabet of %d letter"%self.a.na
		if self.a.na > 1:
			str += 's'
		return str

	def _latex_(self):
		r"""
		Return a latex representation of the automaton.

		OUTPUT:

		string - latex representation of the automaton.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: latex(a)	  # not tested
			\documentclass{article}
			\usepackage[x11names, rgb]{xcolor}
			\usepackage[utf8]{inputenc}
			\usepackage{tikz}
			\usetikzlibrary{snakes,arrows,shapes}
			\usepackage{amsmath}
			%
			%
			<BLANKLINE>
			%
			<BLANKLINE>
			%
			<BLANKLINE>
			\begin{document}
			\pagestyle{empty}
			%
			%
			%
			<BLANKLINE>
			\enlargethispage{100cm}
			% Start of code
			% \begin{tikzpicture}[anchor=mid,>=latex',line join=bevel,]
			\begin{tikzpicture}[>=latex',line join=bevel,]
			  \pgfsetlinewidth{1bp}
			%%
			\pgfsetcolor{black}
			  % Edge: 0 -> 1
			  \draw [-stealth'] (44.247bp,22.0bp) .. controls (54.848bp,22.0bp) and (67.736bp,22.0bp)  .. (89.697bp,22.0bp);
			  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
			  \pgfsetstrokecolor{strokecol}
			  \draw (67.0bp,33.0bp) node {a};
			  % Edge: 2 -> 3
			  \draw [-stealth'] (44.247bp,80.0bp) .. controls (54.848bp,80.0bp) and (67.736bp,80.0bp)  .. (89.697bp,80.0bp);
			  \draw (67.0bp,91.0bp) node {b};
			  % Node: 1
			\begin{scope}
			  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
			  \pgfsetstrokecolor{strokecol}
			  \draw [solid] (112.0bp,22.0bp) ellipse (18.0bp and 18.0bp);
			  \draw [solid] (112.0bp,22.0bp) ellipse (22.0bp and 22.0bp);
			  \draw (112.0bp,22.0bp) node {1};
			 \end{scope}
			  % Node: 0
			\begin{scope}
			  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
			  \pgfsetstrokecolor{strokecol}
			  \draw [solid] (22.0bp,22.0bp) ellipse (18.0bp and 18.0bp);
			  \draw [solid] (22.0bp,22.0bp) ellipse (22.0bp and 22.0bp);
			  \draw (22.0bp,22.0bp) node {0};
			\end{scope}
			  % Node: 3
			\begin{scope}
			  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
			  \pgfsetstrokecolor{strokecol}
			  \draw [solid] (112.0bp,80.0bp) ellipse (18.0bp and 18.0bp);
			  \draw [solid] (112.0bp,80.0bp) ellipse (22.0bp and 22.0bp);
			  \draw (112.0bp,80.0bp) node {3};
			\end{scope}
			  % Node: 2
			\begin{scope}
			  \definecolor{strokecol}{rgb}{0.0,0.0,0.0};
			  \pgfsetstrokecolor{strokecol}
			  \draw [solid] (22.0bp,80.0bp) ellipse (18.0bp and 18.0bp);
			  \draw [solid] (22.0bp,80.0bp) ellipse (22.0bp and 22.0bp);
			  \draw (22.0bp,80.0bp) node {2};
			\end{scope}
			%
			\end{tikzpicture}
			% End of code
			<BLANKLINE>
			%
			\end{document}
			%
			<BLANKLINE>
			<BLANKLINE>
			<BLANKLINE>

		"""
		sx = 800
		sy = 600
		vlabels = None
		html = False
		verb = False
		from sage.misc.latex import LatexExpr
		cdef char *file
		from sage.misc.temporary_file import tmp_filename
		file_name = tmp_filename()+".dot"
		file = file_name
		try:
			from dot2tex import dot2tex
		except ImportError:
			print("dot2tex must be installed in order to have the LaTeX representation of the DetAutomaton.")
			print("You can install it by doing './sage -i dot2tex' in a shell in the sage directory, or by doing 'install_package(package='dot2tex')' in the notebook.")
			return None
		cdef char** ll # labels of edges
		cdef char** vl # labels of vertices
		cdef int i
		ll = <char **>malloc(sizeof(char*) * self.a.na)
		if ll is NULL:
			raise MemoryError("Failed to allocate memory for ll in "
							  "_latex_")
		if vlabels is None:
			vl = NULL
		else:
			if verb:
				print("alloc %s..."%self.a.n)
			vl = <char **>malloc(sizeof(char*) * self.a.n)
			if vl is NULL:
				raise MemoryError("Failed to allocate memory for vl in "
								  "_latex_")
			strV = []
			if verb:
				print("len %s %s" % (self.a.n, len(vlabels)))
			for i in range(self.a.n):
				if html:
					strV.append("<" + vlabels[i] + ">")
				else:
					strV.append("\"" + vlabels[i] + "\"")
				if verb:
					print(strV[i])
				vl[i] = strV[i]
				if verb:
					print("i=%s : %s" % (i, vl[i]))
		strA = []
		for i in range(self.a.na):
			strA.append(str_to_bytes(str(self.A[i])))
			ll[i] = strA[i]
		if verb:
			for i in range(self.a.n):
				print("i=%s : %s" % (i, vl[i]))
		if verb:
			print("plot...")
		sig_on()
		plotDot(file, self.a[0], ll, "Automaton", sx, sy, vl, html, verb, False)	 
		if verb:
			print("free...plot")
		free(ll)
		if vlabels is not None:
			free(vl)
		sig_off()
		dotfile = open(file_name)
		return LatexExpr(dot2tex(dotfile.read()))

	def __hash__(self):
		r"""
		Hash the automaton

		OUTPUT:

		Return a ``int``, hash code of the :class:`DetAutomaton`

		TESTS::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')], A=['a', 'b'])
			sage: hash(a)	 # random
			761033288
			
			sage: from badic.cautomata_generators import *
			sage: a = dag.AnyLetter([0,1], A2=[0,1])
			sage: hash(a)	 # random
			1519588028
		"""
		cdef int h
		h = hash(tuple(self.A))
		sig_on()
		h += hashAutomaton(self.a[0])
		sig_off()
		return h

	def string(self):
		r"""
		Return a ``string`` that can be evaluated to recover the DetAutomaton.
		"""
		S = self.states
		r = "DetAutomaton([%s, [" % S
		c = 0
		for i in range(self.a.n):
			for j in range(self.a.na):
				k = self.a.e[i].f[j]
				if k != -1:
					if c != 0:
						r += ", "
					r += "(%r, %r, %r)" % (S[i], S[k], self.A[j])
					c += 1
		r += "]], A=%s, i=%r, final_states=%s)" % (self.A, S[self.a.i], self.final_states)
		return r

	def is_equal_to(self, DetAutomaton other, verb=False):
		r"""
		Test if the two DetAutomata are equal.
		To be equal, the automata must have the same alphabet
		and exactly the same transitions
		(a permutation of the indices is not allowed).

		INPUT:

		- ``other`` -- other :class:`DetAutomaton` to compare

		OUTPUT:

		Return the result of test (``True`` or ``False``)

		TESTS::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: b = DetAutomaton([(0, 1, 'a'),(2,3,'b')], i=0)
			sage: a.is_equal_to(b)
			False
			sage: a.set_initial_state(0)
			sage: a.is_equal_to(b)
			True
		"""
		if self.A != other.A:
			if verb:
				print("The alphabets are differents.")
			return False
		sig_on()
		r = equalsAutomaton(self.a[0], other.a[0], verb)
		sig_off()
		return c_bool(r)

	def __richcmp__(self, DetAutomaton other, int op):
		r"""
		Compare function, Overwrite built-in function

		INPUT:

		- ``other`` -- other :class:`DetAutomaton` to compare

		OUTPUT:

		Return the result of test (``True`` or ``False``)

		TESTS::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: b = DetAutomaton([(0, 1, 'a'),(2,3,'b')], i=0)
			sage: a == b
			False
			sage: a.set_initial_state(0)
			sage: a == b
			True
			sage: a != b
			False
			sage: a < b
			Traceback (most recent call last):
			...
			NotImplementedError: Comparaison <, >, <= or >= not implemented for DetAutomata.

		"""
		from sage.structure.richcmp import (op_EQ, op_NE)
		# (rich_to_bool,
		# op_EQ, op_NE, op_LT, op_LE, op_GT, op_GE)
		cdef int r
		if op != op_EQ and op != op_NE:
			raise NotImplementedError("Comparaison <, >, <= or >= not implemented for DetAutomata.")
		r = self.is_equal_to(other)
		if op == op_EQ:
			return c_bool(r)
		else:
			return not c_bool(r)

	# give a Sage Automon from the DetAutomaton
	def get_Automaton(self):
		r"""
		Give a Sage Automon from the DetAutomaton

		TESTS::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: a.get_Automaton()
			Automaton with 4 states
		"""
		return AutomatonToSageAutomaton(self.a[0], self.A)
	
	def outgoingedges (self, int i):
		"""
		Return the list of outgoing edges from state of index i.
		"""
		cdef int j,k
		r = []
		A = self.A
		for k in range(self.a.na):
			j = self.a.e[i].f[k]
			if j != -1:
				r.append((i,j,A[k]))
		return r
	
	def edges (self):
		"""
		Return the list of edges.
		"""
		cdef int i
		r = []
		for i in range(self.a.n):
			r += self.outgoingedges(i)
		return r
	
	# give a Graph from the DetAutomaton
	def get_DiGraph(self, keep_transitions_labels=True):
		r"""
		Give a DiGraph from the DetAutomaton

		INPUT:

		- ``keep_transition_labels`` -- Bool (default: ``True``)
			If false, return the graph without labels on edges.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.AnyLetter(['a', 'b'])
			sage: g = a.get_DiGraph()
			sage: g
			Looped multi-digraph on 2 vertices
			sage: g.edges(sort=True)
			[(0, 1, 'a'), (0, 1, 'b')]

		TESTS::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: a.get_DiGraph()
			Looped multi-digraph on 4 vertices

		"""
		return AutomatonToDiGraph(self.a[0], self.A, keep_edges_labels=keep_transitions_labels)
	
	# give a Graph from the DetAutomaton
	def get_Graph(self, keep_transitions_labels=True, multiedges=True):
		r"""
		Give a DiGraph from the DetAutomaton

		INPUT:

		- ``keep_transition_labels`` -- Bool (default: ``True``)
			If false, return the graph without labels on edges.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.AnyLetter(['a', 'b'])
			sage: g = a.get_Graph()
			sage: g
			Looped multi-graph on 2 vertices
			sage: g.edges(sort=True)
			[(0, 1, 'a'), (0, 1, 'b')]

		TESTS::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1) ,(2,3)])
			sage: a.get_Graph()
			Looped multi-graph on 4 vertices

		"""
		return AutomatonToGraph(self.a[0], self.A, keep_edges_labels=keep_transitions_labels, multiedges=multiedges)

	def plot(self, int sx=10, int sy=8, vlabels=None,
			 html=False, file=None, bint draw=True, verb=False):
		"""
		Plot the :class:`DetAutomaton`. Draw using the dot command, if installed on the platform.

		It is strongly recommanded to install the dot command of the Graphviz package in your system in order to get a nice picture.
		Otherwise it will draw using the function plot of :class:`Automaton` of Sage.

		INPUT:

		- ``sx`` - int (default: 10) - width of the picture
		- ``sy`` - int (default: 8) - height of the picture
		- ``vlabels`` - (default: None) - labels of the vertices
		- ``html`` - (default: ``False``) - tell if dot should draw vertices in html mode
		- ``file`` - (default: ``None``) - the address of the .dot file of the drawing (only if dot is installed)
		- ``draw`` - (default: ``True``) - if False, only generate the .dot file (only if dot is installed)
		- ``verb`` - (default: ``False``) - active or not the verbose mode

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: a.plot()  # not tested

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a', 0, None])
			sage: a.plot()  # not tested

		TESTS::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: a.plot()  # not tested

		.. PLOT::
		   :width: 50%

			a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sphinx_plot(a)
		"""
		cdef char *cfile
		cdef char** ll = NULL # labels of edges
		cdef char** vl = NULL # labels of vertices
		cdef int i
		sig_on()
		de = DotExists()
		sig_off()
		if de:
			if file is None:
				from sage.misc.temporary_file import tmp_filename
				file = tmp_filename()+".dot"
			if verb:
				print("alloc %s..."%self.a.na)
			sig_on()
			ll = <char **>malloc(sizeof(char*) * self.a.na)
			if ll is NULL:
				raise MemoryError("Failed to allocate memory for ll in "
								  "plot")
			sig_off()
			if vlabels is None:
				if self.S is not None:
					if verb:
						print("alloc %s..." % self.a.n)
					sig_on()
					vl = <char **>malloc(sizeof(char*) * self.a.n)
					if vl is NULL:
						raise MemoryError("Failed to allocate memory for vl in "
										  "plot")
					sig_off()
					strV = []
					if html:
						from sage.misc.html import html as htm
					for i in range(self.a.n):
						if html:
							strV.append(b"<" + str_to_bytes(str(htm(self.S[i]))) + b">")
						else:
							strV.append(b"\"" + str_to_bytes(str(self.S[i])) + b"\"")
						if verb:
							print(strV[i])
						vl[i] = strV[i]
						if verb:
							print("i=%s : %s" % (i, vl[i]))
				else:
					if verb:
						print("vl = NULL")
					vl = NULL
			else:
				if verb:
					print("alloc %s..." % self.a.n)
				vl = <char **>malloc(sizeof(char*) * self.a.n)
				if vl is NULL:
					raise MemoryError("Failed to allocate memory for vl in "
									  "plot")
				strV = []
				if verb:
					print("len %s %s" % (self.a.n, len(vlabels)))
				for i in range(self.a.n):
					if html:
						strV.append("<" + vlabels[i] + ">")
					else:
						strV.append("\"" + vlabels[i] + "\"")
					if verb:
						print(strV[i])
					vl[i] = <char *>strV[i]
					if verb:
						print("i=%s : %s" % (i, vl[i]))
			strA = []
			for i in range(self.a.na):
				strA.append(str_to_bytes(str(self.A[i])))
				ll[i] = strA[i]
			if verb and vl != NULL:
				for i in range(self.a.n):
					print("i=%s : %s" % (i, vl[i]))
			if verb:
				print("plot...")
			file = str_to_bytes(file)
			sig_on()
			plotDot(file, self.a[0], ll, b"Automaton", sx, sy, vl, html, verb, draw)
			sig_off()
			if verb:
				print("free...plot")
			if ll is not NULL:
				sig_on()
				free(ll)
				sig_off()
			if vl is not NULL: # vlabels is not None and self.S is not None:
				sig_on()
				free(vl)
				sig_off()
			if draw:
				return png_to_display(file+b'.png')
#other possibility to get the image
#			import ipywidgets as widgets
#			file = open(file+'.png', "rb")
#			image = file.read()
#			w = widgets.Image(
#				value=image,
#				format='png',
##				width=600,
##				height=400,
#			)
#			return w
		else:
			return AutomatonToSageAutomaton(self.a[0], self.A).plot()

	@property
	def alphabet(self):
		"""
		To get the :class:`DetAutomaton` attribut alphabet

		OUTPUT:

		Return the alphabet ``A`` of the :class:`DetAutomaton`

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: set(a.alphabet) == {'a', 'b'}
			True

			sage: from badic import *
			sage: a = dag.Word(['a', 1, None])
			sage: set(a.alphabet) == {'a', 1, None}
			True
		"""
		return self.A

	def set_states(self, S):
		"""
		Relabel states by S.

		INPUT:

		- ``S`` -- list of labels of states

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word("Hello")
			sage: a.set_states("abcdef")

		"""
		if len(S) >= self.a.n:
			self.S = list(S)[:self.a.n]
		else:
			raise ValueError("The size %s of S must be the same as the number of states %s" % (len(S), self.a.n))

	def set_alphabet(self, list A):
		"""
		Replace the alphabet by another one.

		INPUT:

		- ``A`` -- list of letters of alphabet

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word(['a', 'b', 0], A=[0, 'a', 'b'])
			sage: a.alphabet
			[0, 'a', 'b']
			sage: a.set_alphabet([0, 1, 'c'])
			sage: b = dag.Word([1, 'c', 0], A=a.alphabet)
			sage: a == b
			True

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: a.set_alphabet(['a', 'c', 'b'])
			sage: set(a.alphabet) == {'a', 'c'}
			True
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: a.set_alphabet(['a','e'])
			sage: set(a.alphabet) == {'a', 'e'}
			True

		TESTS::

			sage: from badic import *
			sage: a = dag.Word(['a', 'b', 'c'])
			sage: a.set_alphabet([0, 1])
			Traceback (most recent call last):
			...
			ValueError: The size 2 of the new alphabet has to be the same (i.e. 3)

		"""
		if len(A) < len(self.A):
			raise ValueError("The size %s of the new alphabet has to be the same (i.e. %s)"%(len(A), len(self.A)))
		self.A = A[:self.a.na]

	@property
	def initial_state(self):
		"""
		Get the initial state of the :class:`DetAutomaton`

		OUTPUT:

		Return an int, index of the initial state ``i`` of :class:`DetAutomaton`,
		or -1 if there is no initial state.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: a.initial_state
			-1
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')], i=2)
			sage: a.initial_state
			2
		"""
		return self.a.i

	def set_initial_state(self, int i):
		"""
		Set the initial state.

		INPUT:

		- ``i`` -- int -- the initial state of the automaton

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: a.set_initial_state(2)
			sage: a.initial_state
			2
			sage: a.set_initial_state(6)
			Traceback (most recent call last):
			...
			ValueError: The initial state must be a state of the automaton or -1: 6 is not in [-1, 3]
		"""
		if i < self.a.n and i >= -1:
			self.a.i = i
		else:
			raise ValueError("The initial state must be a state of the automaton or -1: " +
							 "%d is not in [-1, %d]" % (i, self.a.n - 1))

	@property
	def final_states(self):
		"""
		Indicate all final states

		OUTPUT:

		Return the list of final states

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: a.final_states
			[0, 1, 2, 3]
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')], final_states=[0,3])
			sage: a.final_states
			[0, 3]
			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a', 'b', None])
			sage: a.final_states
			[3]
		"""
		cdef int i
		l = []
		for i in range(self.a.n):
			if self.a.e[i].final:
				l.append(i)
		return l

	@property
	def states(self):
		"""
		Indicate all states of the automaton

		OUTPUT:

		Return the list of states

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word(['a', 'b', None])
			sage: a.states
			[0, 1, 2, 3]
		"""
		if self.S is None:
			return list(range(self.a.n))
		else:
			return self.S

	def set_final_states(self, list lf):
		"""
		Set the set of final states.

		 INPUT:

		- ``lf`` -- list of states to set as final

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: a.set_final_states([0,3])
			sage: a.final_states
			[0, 3]
			sage: a.set_final_states([0,4])
			Traceback (most recent call last):
			...
			ValueError: 4 is not a state!

		"""
		cdef int f
		for f in range(self.a.n):
			self.a.e[f].final = 0
		for f in lf:
			if f < 0 or f >= self.a.n:
				raise ValueError("%d is not a state!" % f)
			self.a.e[f].final = 1

	def is_final(self, int e):
		"""
		Indicate if the state is final

		INPUT:

		- ``e`` -- int input state to examine as final

		OUTPUT:

		``True`` if the state ``e`` is final (i.e. ``False`` in the other case)

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word(['a', 'b'])
			sage: a.is_final(1)
			False

			sage: from badic import *
			sage: a = DetAutomaton([(10,11,'a') ,(12,13,'b')])
			sage: a.is_final(1)
			True
			sage: a.is_final(10)
			Traceback (most recent call last):
			...
			ValueError: 10 is not a state!
		"""
		if e >= 0 and e < self.a.n:
			sig_on()
			ans = c_bool(self.a.e[e].final)
			sig_off()
			return ans
		else:
			raise ValueError("%s is not a state!"%e)

	def final(self, int e):
		"""
		Indicate the final value of state ``e``

		INPUT:

		- ``e`` -- int input state to examine as final

		OUTPUT:
			A int.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word(['a', 'b'])
			sage: a.final(1)
			0

			sage: from badic import *
			sage: a = DetAutomaton([(10,11,'a') ,(12,13,'b')])
			sage: a.final(1)
			1
			sage: a.final(10)
			Traceback (most recent call last):
			...
			ValueError: 10 is not a state!
		"""
		if e >= 0 and e < self.a.n:
			return self.a.e[e].final
		else:
			raise ValueError("%s is not a state!"%e)

	def set_final(self, int e, final=True):
		"""
		Set the state as final.

		 INPUT:

		- ``e`` -- int state to set as final
		- ``final`` -- (default: ``True``) set the state final or not final

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word(['a', 'b'])
			sage: a.set_final(1)
			sage: a.final_states
			[1, 2]

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a') ,(2,3,'b')])
			sage: a.set_final(2, False)
			sage: a.final_states
			[0, 1, 3]
			sage: a.set_final(4)
			Traceback (most recent call last):
			...
			ValueError: 4 is not the index of a state (i.e. it is not between 0 and 3)!
		"""
		if e >= 0 and e < self.a.n:
			self.a.e[e].final = final
		else:
			raise ValueError("%d is not the index of a state (i.e. it is not between 0 and %s)!" % (e, self.a.n-1))

	def succ(self, int i, int j):
		"""
		Return the state reached by following the transition with label of index j from state i.
		Return -1 if it doesn't exists.

		INPUT:

		- ``i`` - int - index of the state
		- ``j`` - int - index of the label

		OUTPUT:

		return a int, successor of the state i following edge j

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a'), (2,3,'b')])
			sage: a.succ(0, a.alphabet.index('b'))
			-1
			sage: a.succ(2, a.alphabet.index('b'))
			3
		"""
		if i < 0 or i >= self.a.n or j < 0 or j >= self.a.na:
			return -1
		return self.a.e[i].f[j]

	def succc(self, int i, l):
		"""
		Return the state reached by following the transition with label l from state i.
		Return -1 if it doesn't exists.

		INPUT:

		- ``i`` - int - index of the state
		- ``l`` - label

		OUTPUT:

		return a int, successor of the state i following edge j

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a'), (2,3,'b')])
			sage: a.succc(0, 'b')
			-1
			sage: a.succc(2, 'b')
			3
			sage: a.succc(2, 'c')
			-1
		"""
		cdef int j
		try:
			j = self.A.index(l)
		except:
			return -1
		if i < 0 or i >= self.a.n:
			return -1
		return self.a.e[i].f[j]

	def succs(self, int i):
		"""
		Return indices of letters of leaving transitions from state ``i``.

		INPUT:

		- ``i`` -- int - the input state

		OUTPUT:

		return a list of int

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a'), (2,3,'b')], A=['a','b'])
			sage: a.succs(2)
			[1]
			sage: a.succs(4)
			[]

		"""
		cdef int j
		if i < 0 or i >= self.a.n:
			return []
		return [j for j in range(self.a.na) if self.a.e[i].f[j] != -1]

	def path(self, list l, i=None):
		"""
		Follows the path labeled by a list ``l`` and return the reached state

		INPUT:

		- ``l`` -- list of indices of labels
		- ``i`` -- (default: ``None``) the initial state

		OUTPUT:

		int 
		return the state reached after following the path

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a'), (2,3,'b')], A=['a', 'b'], i=2)
			sage: a.path([1])
			3
			sage: a.path([0, 2])
			-1
		"""
		if i is None:
			i = self.a.i
		for j in l:
			i = self.succ(i, j)
		return i

	def set_succ(self, int i, int j, int k):
		"""
		Set the successor of the state i, following the transition labeled by j.
		In other words, add a transition from state i to state k labeled by A[j].
		If k is -1, this permits to remove the transition from state i labeled by A[j].

		INPUT:

		- ``i`` -- int - the input state
		- ``j`` -- int - the index of a letter
		- ``k`` -- int - the state we reach

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,'a'), (2, 3,'b')], A=['a', 'b'], i=2)
			sage: a.set_succ(0, 1, 2)
			sage: a.succs(0)
			[0, 1]
			sage: a.set_succ(0, 4, 2)
			Traceback (most recent call last):
			...
			ValueError: set_succ(0, 4, 2) : index out of bounds !

		"""
		if i < 0 or i >= self.a.n or j < 0 or j >= self.a.na or k < -1 or k >= self.a.n:
			raise ValueError("set_succ(%s, %s, %s) : index out of bounds !" % (i, j, k))
		self.a.e[i].f[j] = k

	def zero_complete_op(self, z=None, algo=1, verb=False):
		"""
		Compute an automaton recognizing the language L(l*)^(-1), where L is
		the language of self and l is the letter of index z.
		This language L(l*)^(-1) is the set of words u such that there exists
		an integer n such that ul^n is in the language of self.

		INPUT:

		- ``z`` - int (default: ``None``) - index of the letter l
			If ``None``, take the index of 0.

		- ``algo`` - int (default: ``1``) - warning: algo !=1 is incorrect !!!!

		- ``verb`` -- (default: ``False``) if True, print debugging informations

		OUTPUT:

		Return a :class:`DetAutomaton`.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word([0,1,0])
			sage: a.zero_complete_op()
			sage: a.final_states	# random
			[2, 3]

		TESTS::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (0, 3, 'b')], i=0)
			sage: a.zero_complete_op()
			Traceback (most recent call last):
			...
			ValueError: Letter 0 must be in the alphabet !

		"""
		cdef int i,j,k
		cdef CAutomaton a
		if z is None:
			try:
				z = self.alphabet.index(0)
			except:
				raise ValueError("Letter 0 must be in the alphabet !")
		if algo == 1:
			a = self.mirror()
			to_see = self.final_states
			seen = set(to_see)
			while len(to_see) > 0:
				i = to_see.pop()
				for j in range(a.a.e[i].n):
					l = a.a.e[i].a[j].l
					if l == z:
						k = a.a.e[i].a[j].e
						if k not in seen:
							seen.add(k)
							to_see.append(k)
							self.set_final(k)
		else:
			sig_on()
			ZeroComplete(self.a, z, verb)
			sig_off()

	def zero_complete (self, z=None):
		"""
		Compute an automaton recognizing the language L(l*), where L is
		the language of self and l is the letter of index z.
		"""
		a = self.mirror().determinize()
		a.zero_complete_op(z=z)
		return a.mirror().determinize()

	def concat_zero_star(self, z=None, sink_state=False, simplify=True, verb=False):
		"""
		Compute an automaton recognizing the language L(l*), where L is
		the language of self and l is the letter of index z.

		INPUT:

		- ``z`` -- (default: ``None``) - index of the letter l
			If ``None`` take the index of 0.

		- ``sink_state`` --  (default: ``False``) - give a result with or without sink state

		- ``simplify`` -- (default: ``True``) - prune and minimize the result
			(this removes the sink state)

		- ``verb`` -- (default: ``False``) if True, print debugging informations

		OUTPUT:

		Return a :class:`DetAutomaton`.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word([0,1,0])
			sage: a.concat_zero_star()
			DetAutomaton with 4 states and an alphabet of 2 letters

			sage: a = DetAutomaton([(0, 1, 'a'), (0, 3, 'b')], i=0)
			sage: a.concat_zero_star(z=0)
			DetAutomaton with 2 states and an alphabet of 2 letters
			sage: a = DetAutomaton([(0, 1, 'a'), (0, 3, 'b')], i=0)
			sage: a.concat_zero_star(z=0, sink_state=True, simplify=False)
			DetAutomaton with 5 states and an alphabet of 2 letters
			sage: b = DetAutomaton([(0, 1, 'a'), (0, 3, 'b')])
			sage: b.concat_zero_star(True)
			DetAutomaton with 1 state and an alphabet of 2 letters

		TESTS::

			sage: from badic import *
			sage: a = dag.Word(['a', 'b'])
			sage: a.concat_zero_star()
			Traceback (most recent call last):
			...
			ValueError: 0 is not in list
		"""
		cdef Automaton a
		if z is None:
			z = self.A.index(0)
		r = DetAutomaton(None)
		sig_on()
		a = ZeroComplete2(self.a, z, sink_state, verb)
		sig_off()
		r.a[0] = a
		r.A = self.A
		r.S = None
		if simplify:
			return r.prune().minimize()
		else:
			return r

	def zero_star_concat(self, z=None, simplify=True):
		"""
		Compute an automaton recognizing the language (l*)L, where L is
		the language of self and l is the letter of index z.

		INPUT:

		- ``z`` - (default: ``None``) - index of the letter l
			If ``None``, take the index of letter 0.

		- ``simplify`` - (default: ``True``) - if True, prune and minimize the result

		OUTPUT:

		Return a :class:`DetAutomaton` whose language is the set of words of
		the language of a with some zeroes on the beggining
		(we call zero the letter of index z).

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word([0,1,0])
			sage: a.zero_star_concat()
			DetAutomaton with 4 states and an alphabet of 2 letters

			sage: a = DetAutomaton([(0, 1, 'a'), (0, 3, 'b')], i=0)
			sage: a.zero_star_concat(0)
			DetAutomaton with 2 states and an alphabet of 2 letters
			sage: a.zero_star_concat(1)
			DetAutomaton with 2 states and an alphabet of 2 letters
			sage: b = DetAutomaton([(0, 1, 'a'), (0, 3, 'b')])
			sage: b.zero_star_concat(1)
			DetAutomaton with 1 state and an alphabet of 2 letters

		TESTS::

			sage: from badic import *
			sage: a = dag.Word(['a', 'b'])
			sage: a.zero_star_concat()
			Traceback (most recent call last):
			...
			ValueError: 0 is not in list

		"""
		cdef Automaton a
		if z is None:
			z = self.A.index(0)
		r = DetAutomaton(None)
		sig_on()
		a = ZeroInv(self.a, z)  # list(self.A).index(self.A[z]))
		sig_off()
		r.a[0] = a
		r.A = self.A
		r.S = None
		if simplify:
			return r.prune().minimize()
		else:
			return r

	def interior_op(self, DetAutomaton ar, int verb=False):
		"""
		Determine the interior of self with respect to ar.
		Assume ar to be strongly connected.

		INPUT:

			- ``ar`` - DetAutomaton - language to found in self.
					ar is assumed to be strongly connected.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,1,1),(1,0,1)], i=0, avoidDiGraph=True)
			sage: a.interior_op(dag.AnyWord([0,1])); a
			DetAutomaton with 2 states and an alphabet of 2 letters

		"""
		if ar is None:
			return
		try:
			i,c = self.find_state_with_language(ar, get_scc=True)
			self.set_final_states(c)
		except Exception as e:
			if verb > 0:
				print(e)
			self.set_initial_state(-1)

	def boundary_op (self, ar, verb=False):
		"""
		Determine the boundary of self with respect to ar.
		Assume ar to be strongly connected.

		INPUT:

			- ``ar`` - DetAutomaton - language to found in self.
					ar is assumed to be strongly connected.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,1,1),(1,0,1)], i=0, avoidDiGraph=True)
			sage: a.boundary_op(dag.AnyWord([0,1])); a
			DetAutomaton with 2 states and an alphabet of 2 letters

		"""
		cdef int i
		if ar is None:
			return
		try:
			i,c = self.find_state_with_language(ar, get_scc=True)
			self.set_final_states([i for i in range(self.a.n) if i not in c])
		except Exception as e:
			if verb > 0:
				print(e)
			self.set_initial_state(-1)

	def diff(self, DetAutomaton a, bint det=True, bint simplify=True):
		"""
		Compute an automaton whose language is the set of differences of the two languages.

		INPUT:

		- ``a`` - DetAutomaton - the automaton of the language that we substract
		- ``det`` - Bool (default: ``True``) - determinize the result
		- ``simplify`` - Bool (default: ``True``) - prune and minimize the result

		OUTPUT:

		Return a :class:`DetAutomaton` if ``det`` is True, and return a :class:`Cautomaton` otherwise.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.AnyWord([0, 1])
			sage: a.diff(a)
			DetAutomaton with 1 state and an alphabet of 3 letters

		TESTS::

			sage: from badic import *
			sage: a = dag.AnyWord(['a', 'b'])
			sage: a.diff(a)
			Traceback (most recent call last):
			...
			TypeError: unsupported operand type(s) for -: 'str' and 'str'
		"""
		cdef DetAutomaton r
		cdef CAutomaton nr
		cdef dict d
		r = self.product(a)
		d = {}
		for i in self.A:
			for j in a.A:
				d[(i, j)] = i-j
		return r.proj(d, det=det, simplify=simplify)

	def prune_inf(self, verb=False):
		"""
		Prune "at infinity": remove all accessible states from which there no infinite way.

		INPUT:

		- ``verb`` -- Boolean (default: ``False``) if True, print
		  debugging informations

		OUTPUT:

		Return the pruned :class:`DetAutomaton`.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (0, 3, 'b')], i=0)
			sage: a.prune_inf()
			DetAutomaton with 0 state and an alphabet of 2 letters
			sage: b = DetAutomaton([(0, 1, 'a'), (0, 3, 'b')])
			sage: b.prune_inf()
			DetAutomaton with 0 state and an alphabet of 2 letters

			sage: a = DetAutomaton([(10,10,'x'),(10,20,'y'),(20,20,'z'),\
				(20,10,'y'),(20,30,'x'),(30,30,'y'),(30,10,'z'),(30,20,'x'),\
				(10,30,'z')], i=10)
			sage: a.prune_inf()
			DetAutomaton with 3 states and an alphabet of 3 letters

			sage: a = DetAutomaton([(10,10,'x'),(10,20,'y'),(20,20,'z'),\
				(20,10,'y'),(20,30,'x'),(30,30,'y'),(30,10,'z'),(30,20,'x'),\
				(10,30,'z')], i=10, final_states=[])
			sage: a.prune_inf()
			DetAutomaton with 3 states and an alphabet of 3 letters


		TESTS::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (0, 3, 'b')], i=0)
			sage: a.prune_inf(verb=True)
			induction...
			States counter = 0
			count...
			cpt = 0
			final states...
			DetAutomaton with 0 state and an alphabet of 2 letters

		"""
		cdef Automaton a
		r = DetAutomaton(None)
		sig_on()
		a = prune_inf(self.a[0], verb)
		sig_off()
		r.a[0] = a
		r.A = self.A
		r.S = None
		return r

	def prune_i(self, verb=False):
		"""
		Prune the automaton:
		remove all non-reachable states

		INPUT:

		- ``verb`` -- Boolean (default: ``False``) if True, print debugging informations

		OUTPUT:

		Return the pruned :class:`DetAutomaton`.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (0, 3, 'b'), (1, 3, 'b')], i=0)
			sage: a.prune_i()
			DetAutomaton with 3 states and an alphabet of 2 letters
			sage: a = DetAutomaton([(0, 1, 'a'), (0, 3, 'b'), (1, 3, 'b')])
			sage: a.prune_i()
			DetAutomaton with 0 state and an alphabet of 2 letters
			sage: b = DetAutomaton([(0, 1, 'a'), (0, 3, 'b'), (1, 3, 'b')])
			sage: b.prune_i()
			DetAutomaton with 0 state and an alphabet of 2 letters
		"""
		if self.a.i == -1:
			return DetAutomaton([], A=self.alphabet)
		cdef Automaton a
		r = DetAutomaton(None)
		sig_on()
		a = pruneI(self.a[0], verb)
		sig_off()
		r.a[0] = a
		r.A = self.A
		r.S = None
		return r

	def prune_f (self, keep_labels=True):
		"""
		Remove non-co-reachable states.

		INPUT:

		- ``keep_labels`` - bool (default: ``True``) - keep states labels

		OUTPUT:
			A DetAutomaton.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,0,1),(0,1,2)], i=0, final_states=[1], avoidDiGraph=True)
			sage: a.prune_f()
			DetAutomaton with 2 states and an alphabet of 2 letters

		"""
		a = self.mirror()
		to_see = a.initial_states
		seen = set(to_see)
		while len(to_see) > 0:
			i = to_see.pop()
			for j in range(a.n_succs(i)):
				k = a.succ(i,j)
				l = a.label(i,j)
				if k not in seen:
					seen.add(k)
					to_see.append(k)
		return self.sub_automaton(seen, keep_states_labels=keep_labels)

	def prune(self, bint verb=False):
		"""
		Prune the automaton:
		remove all non-reachable and non-co-reachable states

		INPUT:

		- ``verb`` -- Boolean (default: ``False``) if True, print debugging informations

		OUTPUT:

		Return the pruned :class:`DetAutomaton`.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.prune()
			DetAutomaton with 2 states and an alphabet of 2 letters
			sage: b = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')])
			sage: b.prune()
			DetAutomaton with 0 state and an alphabet of 2 letters

		"""
		cdef Automaton a
		r = DetAutomaton(None)
		sig_on()
		a = prune(self.a[0], verb)
		sig_off()
		r.a[0] = a
		r.A = self.A
		r.S = None
		return r

	def product(self, DetAutomaton b, dict d=None, bint simplify=True, int verb=False):
		"""
		Give the product of the :class:`DetAutomaton` and ``a`` an other
		``DetAutomaton``. Assume the dictionnary ``d`` to be injective.

		INPUT:

		- ``a`` -- :class:`DetAutomaton` to multiply

		- ``d`` -- dict (default: ``None``) - dictionary that associates
			something to couples of letters of each alphabet.
			The alphabet of the result is given by values of this dictionnary.
			If a couple of letters is not a key of this dictionnary,
			then corresponding edges of the product are avoided.

		- ``simplify`` -- Bool (default: ``True``) - prune and minimize the result

		- ``verb`` -- Boolean (default: ``False``) - if True,
			print debugging informations

		OUTPUT:

		Return the product as a :class:`DetAutomaton`.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word(['a','b'])
			sage: b = dag.Word([0,1])
			sage: c = a.product(b)
			sage: c
			DetAutomaton with 3 states and an alphabet of 4 letters
			sage: set(c.alphabet) == {('a', 0), ('a', 1), ('b', 0), ('b', 1)}
			True

			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = DetAutomaton([(3, 2, 'c'), (1, 2, 'd')], i=2)
			sage: a.product(b)
			DetAutomaton with 1 state and an alphabet of 4 letters
		
		TESTS::

			sage: from badic import *
			sage: a = dag.Word(['a','b'])
			sage: b = dag.Word([0,1])
			sage: c = a.product(b)
			sage: d = dag.Word([('a',0),('b',1)], A = c.alphabet)
			sage: c.has_same_language_as(d)
			True
			
			sage: a = dag.Word(['a', 'b'])
			sage: d = {('a', 'a'):0, ('b', 'b'):0}
			sage: a.product(a, d)
			Traceback (most recent call last):
			...
			ValueError: The dictionnary is not injective!

		"""
		if d is None:
			d = {}
			for la in self.A:
				for lb in b.A:
					d[(la, lb)] = (la, lb)
			if verb:
				print(d)
		else:
			V = d.values()
			if len(V) != len(set(V)):
				raise ValueError("The dictionnary is not injective!")
		cdef Automaton a
		cdef Dict dC
		r = DetAutomaton(None)
		Av = []
		sig_on()
		dv = imagProductDict(d, self.A, b.A, Av=Av)
		sig_off()
		if verb > 0:
			print("Av=%s" % Av)
			print("dv=%s" % dv)
		sig_on()
		dC = getProductDict(d, self.A, b.A, dv=dv, verb=verb-1)
		sig_off()
		if verb > 0:
			print("dC=")
			printDict(dC)
		sig_on()
		a = Product(self.a[0], b.a[0], dC, verb > 1)
		FreeDict(&dC)
		sig_off()
		r.a[0] = a
		r.A = Av
		if simplify:
			return r.prune().minimize()
		else:
			return r

	def intersection(self, DetAutomaton other, int algo=1, bint simplify=True, int verb=False):
		"""
		Give a automaton recognizing the intersection of the languages
		of ``self`` and ``other``.

		INPUT:

		- ``other`` -- :class:`DetAutomaton` to intersect

		- ``simplify`` - Boolean (default: ``True``) - if True,
			prune and minimize the result

		- ``algo`` -- int (default: ``1``) Algorithm used to compute the intersection

		- ``verb`` -- Boolean (default: ``False``) if True,
			print debugging informations

		OUTPUT:

		Return the intersected :class:`DetAutomaton`.

		EXAMPLES::

			#. Intersection of words that contains 'aa' and words that contains 'bb'

				sage: from badic import *
				sage: a = dag.AnyWord(['a','b']).concat(dag.Word(['a', 'a'])).concat(dag.AnyWord(['a','b']))
				sage: b = dag.AnyWord(['a','b']).concat(dag.Word(['b', 'b']).concat(dag.AnyWord(['a','b'])))
				sage: a.intersection(b)
				DetAutomaton with 8 states and an alphabet of 2 letters

			#. Intersection of two languages whose alphabets are different

				sage: from badic import *
				sage: a = dag.AnyWord(['a','b','c'])
				sage: b = dag.AnyWord(['a','b','e'])
				sage: a.intersection(b)
				DetAutomaton with 1 state and an alphabet of 2 letters

			sage: from badic import *
			sage: a = dag.AnyWord(['a', 'b'])
			sage: b = dag.Word(['a','b','a'])
			sage: a.intersection(b).has_same_language_as(b)
			True

		TESTS::

			sage: from badic import *
			sage: a = dag.AnyWord(['a','b','c'])
			sage: b = dag.AnyWord(['a','b','e'])
			sage: a.intersection(b).has_same_language_as(dag.AnyWord(['a', 'b']))
			True

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = DetAutomaton([(3, 2, 'c'), (1, 2, 'd')], i=2)
			sage: a.intersection(b)
			DetAutomaton with 1 state and an alphabet of 0 letter
			sage: a.intersection(b, simplify=False)
			DetAutomaton with 0 state and an alphabet of 0 letter

		"""
		cdef DetAutomaton p
		cdef dict d
		cdef int i1,i2, j1, j2, k1, k2
		if verb > 1:
			print("intersection of")
			print(self)
			print("with")
			print(other)
			print("with algo %s" % algo)
		if algo == 1:
			A = list(set(self.A).intersection(other.A))
			A.sort()
			if self.a.i == -1 or other.a.i == -1:
				return DetAutomaton([], A=A)
			lj = []
			for a in A:
				lj.append((self.A.index(a), other.A.index(a)))
			if verb > 0:
				print(lj)
			to_see = [(self.a.i, other.a.i)]
			seen = set(to_see)
			F = []
			R = []
			while len(to_see) > 0:
				if verb > 0:
					print(to_see)
				(i1,i2) = to_see.pop()
				if self.a.e[i1].final and other.a.e[i2].final:
					F.append((i1,i2))
				for j1,j2 in lj:
					k1 = self.a.e[i1].f[j1]
					k2 = other.a.e[i2].f[j2]
					if k1 != -1 and k2 != -1:
						R.append(((i1,i2),(k1,k2),self.alphabet[j1]))
						if (k1,k2) not in seen:
							seen.add((k1,k2))
							to_see.append((k1,k2))
			p = DetAutomaton(R, A=A, i=(self.a.i, other.a.i), final_states=F)
		else:
			d = {}
			for l in self.A:
				if l in other.A:
					d[(l, l)] = l
			if verb > 0:
				print("d=%s" % d)
			p = self.product(other, d, simplify=simplify, verb=verb-1)
		if simplify:
			return p.prune().minimize()
		else:
			return p

	def is_complete(self):
		"""
		Determine if the automaton is complete
		(i.e. every state has leaving transitions
		labeled by every letter of the alphabet)

		OUTPUT:

		Return ``True`` if the automaton is complete, ``False`` otherwise.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.AnyWord(['a','b'])
			sage: a.is_complete()
			True
			sage: a = dag.Word(['a','b'])
			sage: a.is_complete()
			False
			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.is_complete()
			False
			sage: a = DetAutomaton([(0, 0, 'a')])
			sage: a.is_complete()
			True
		"""
		cdef bint res
		sig_on()
		res = IsCompleteAutomaton(self.a[0])
		answ = c_bool(res)
		sig_off()
		return answ

	def complete_op(self, label_sink='s'):
		"""
		Complete the automaton ON PLACE, by adding a sink state if necessary.

		INPUT:

		- ``label_sink`` -- (default: ``s``) - label of the sink state
		  if added and if self has labels of states.

		OUTPUT:

		Return ``True`` if the automaton was not complete
		(a sink state has been added), return ``False`` otherwise.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.complete_op()
			True
			sage: a
			DetAutomaton with 5 states and an alphabet of 2 letters

			sage: from badic import *
			sage: dag.AnyWord(['a','b']).complete_op()
			False

		TESTS::

			sage: from badic import *
			sage: a = dag.Word(['a','b','a'])
			sage: a.complete_op()
			True
			sage: a.has_same_language_as(dag.Word(['a','b','a']))
			True
			sage: a == dag.Word(['a','b','a'])
			False

		"""
		sig_on()
		res = CompleteAutomaton(self.a)
		res = c_bool(res)
		sig_off()
		if res:
			if self.S is not None:
				# add a label for the sink state
				self.S.append(label_sink)
		return res

	def prefix(self, w, i=None, int algo=1):
		"""
		Give an automaton recognizing the language w(w^(-1)L) where L is the language of self.
		It is the set of words recognized by self and starting with word w.

		INPUT:

		- ``w`` -- a word

		- ``i`` -- int (default: ``None``) - the initial state used

		- ``algo`` -- int (default: ``1``)
		  The algorithm used for the computation.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.AnyWord(['a', 'b'])
			sage: a.prefix(['a', 'a'])
			DetAutomaton with 3 states and an alphabet of 2 letters

			sage: a = dag.AnyWord(['a', 'b'])
			sage: a.prefix(['a', 'a'], algo=2)
			DetAutomaton with 3 states and an alphabet of 2 letters

		TESTS::

			sage: from badic import *
			sage: a = dag.Word(['a', 'b', 'a'], A=['a', 'b'])
			sage: a.prefix(['c'])
			Traceback (most recent call last):
			...
			ValueError: 'c' is not in the alphabet ['a', 'b']

			sage: a = dag.AnyWord(['a', 'b'])
			sage: a.prefix(['a', 'a']).has_same_language_as(a.prefix(['a', 'a'], algo=2))
			True

		"""
		cdef DetAutomaton a
		cdef int* l
		cdef int j
		if algo == 2:
			l = <int*>malloc(sizeof(int)*self.a.n)
			for j in range(len(w)):
				try:
					l[j] = self.A.index(w[j])
				except:
					raise ValueError("letter %s of the word %s is not in the alphabet of self"%(w[j], w))
			if i is None:
				i = self.a.i
			a = DetAutomaton(None)
			sig_on()
			a.a[0] = PieceAutomaton(self.a[0], l, len(w), i)
			sig_off()
			free(l)
			a.A = self.A
			return a
		else:
			a = self.copy()
			a.shift_list_op(w)
			from .cautomata_generators import dag
			return dag.Word(w).concat(a)

	def set_all_final (self):
		r"""
		Set every state as final.
		"""
		cdef int i
		for i in range(self.a.n):
			self.a.e[i].final = True

	def prefix_closure(self):
		"""
		Give an automaton recognizing the smallest language stable
		by prefix and containing the language of self
		i.e. every states after pruning begins final

		OUTPUT:

		Return a :class:`DetAutomaton`

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.prefix_closure()
			DetAutomaton with 2 states and an alphabet of 2 letters

			sage: from badic import *
			sage: b = dag.Word(['a','b','a'])
			sage: c = b.prefix_closure()
			sage: c
			DetAutomaton with 4 states and an alphabet of 2 letters
			sage: dag.Word(['a', 'b']).included(c)
			True
		"""
		cdef int i
		r = DetAutomaton(None)
		sig_on()
		r.a[0] = prune(self.a[0], False)
		sig_off()
		r.A = self.A
		r.set_all_final()
		return r

	def kleene_star (self, determinize=True):
		"""
		Return the Kleene star of self.
		
		EXAMPLES::
			
			sage: from badic import *
			sage: a = dag.Word('aba')
			sage: a.kleene_star()
			DetAutomaton with 3 states and an alphabet of 2 letters
		"""
		cdef i,j,k
		cdef CAutomaton a
		a = CAutomaton(self)
		for i in range(self.a.n):
			for j in range(self.a.na):
				k = self.a.e[i].f[j]
				if k != -1:
					if self.a.e[k].final:
						a.add_transition(i, self.A[j], self.a.i)
		a.set_final(self.a.i)
		if determinize:
			return a.determinize().minimize()
		else:
			return a

	# Use epsilon-transitions rather than product ?
	def union(self, DetAutomaton a, simplify=True, verb=False):
		"""
		Return an automaton recognizing the union of the two languages.
		Warning: there is a side effect, the automata are completed.

		INPUT:

		- ``a`` -- :class:`DetAutomaton`

		- ``simplify`` --  (default: ``True``)
		  prune and minimize the result ?

		- ``verb`` -- Boolean (default: ``False``) if True,
		  print debugging informations

		OUTPUT:

		Return a :class:`DetAutomaton`

		EXAMPLES::

			#. Union of words that contains 'aa' and words that contains 'bb'

				sage: from badic import *
				sage: a = dag.AnyWord(['a','b']).concat(dag.Word(['a', 'a'])).concat(dag.AnyWord(['a','b']))
				sage: b = dag.AnyWord(['a','b']).concat(dag.Word(['b', 'b']).concat(dag.AnyWord(['a','b'])))
				sage: a.union(b)
				DetAutomaton with 4 states and an alphabet of 2 letters

			#. Union of two languages whose alphabets are differents

				sage: from badic import *
				sage: a = dag.Word(['a','b','c'])
				sage: b = dag.Word(['a','b','a'])
				sage: a.union(b)
				DetAutomaton with 4 states and an alphabet of 3 letters

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = DetAutomaton([(3, 2, 'a'), (1, 2, 'd')], i=2)
			sage: a.union(b)
			DetAutomaton with 2 states and an alphabet of 3 letters
			sage: b = DetAutomaton([(3, 2, 'a'), (1, 2, 'd')])
			sage: a.union(b)
			DetAutomaton with 2 states and an alphabet of 3 letters

		TESTS::

			sage: from badic import *
			sage: a = dag.AnyWord(['a','b']).concat(dag.Word(['a', 'a'])).concat(dag.AnyWord(['a','b']))
			sage: b = dag.AnyWord(['a','b']).concat(dag.Word(['b', 'b']).concat(dag.AnyWord(['a','b'])))
			sage: c = DetAutomaton([(0, 0, 'a'), (0, 0, 'b'), (1, 2, 'a'), (1, 3, 'b'), (2, 0, 'a'), (2, 3, 'b'), (3, 2, 'a'), (3, 0, 'b')], i=1, final_states=[0])
			sage: a.union(b).has_same_language_as(c)
			True

			sage: a = dag.Word(['a','b','c'])
			sage: b = dag.Word(['a','b','a'])
			sage: a.included(a.union(b))
			True
			sage: b.included(a.union(b))
			True

		"""
		cdef DetAutomaton a1 = self
		cdef DetAutomaton a2 = a

		# increase the alphabets if necessary
		if set(a1.A) != set(a2.A):
			A = list(set(a1.A+a2.A))
			a1 = a1.bigger_alphabet(A)
			a2 = a2.bigger_alphabet(A)
		else:
			a1 = a1.copy()  # in order to avoid side effect
			a2 = a2.copy()

		# complete the automata
		sig_on()
		CompleteAutomaton(a1.a)
		CompleteAutomaton(a2.a)
		sig_off()

		# make the product
		d = {}
		for l in a1.A:
			d[(l, l)] = l

		cdef Automaton ap
		cdef Dict dC
		r = DetAutomaton(None)
		Av = []
		sig_on()
		dv = imagProductDict(d, a1.A, a2.A, Av=Av)
		sig_off()
		if verb:
			print("Av=%s" % Av)
			print("dv=%s" % dv)
		sig_on()
		dC = getProductDict(d, a1.A, a2.A, dv=dv, verb=verb)
		sig_off()
		sig_on()
		if verb:
			print("dC=")
			printDict(dC)
		ap = Product(a1.a[0], a2.a[0], dC, verb)
		FreeDict(&dC)
		sig_off()
		# set final states
		cdef int i, j
		cdef n1 = a1.a.n
		for i in range(n1):
			for j in range(a2.a.n):
				ap.e[i + n1 * j].final = a1.a.e[i].final or a2.a.e[j].final

		r.a[0] = ap
		r.A = Av
		if simplify:
			return r.prune().minimize()
		else:
			return r

	def split(self, DetAutomaton a, bint simplify=True, bint verb=False):
		"""
		Split the automaton with respect to a :class:`DetAutomaton` ``a``.
		Return two DetAutomaton recognizing the intersection of the language
		of self with the one of a and with the complementary of the language
		of a.
		We assume that the two automata have the same alphabet, otherwise the
		complementary is taken in the set of words over the intersection of
		the two alphabets.

		.. WARNING::

			There is a side-effect: the automaton ``a`` is completed.

		INPUT:

		- ``a`` -- :class:`DetAutomaton` - we split ``self`` with respect
		  to this automaton.

		- ``simplify`` -- Bool (default: True) - if True, prune and
		  minimize the result.

		- ``verb`` -- Bool (default: False) - if True, display
		  informations for debugging.

		OUTPUT:

		Return tuple of two splited automaton, recognizing respectively
		the language intersection of L and La, and the language
		intersection of L and complementary of La,
		where L is the language of self and La is the language of a.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.AnyWord(['a', 'b'])
			sage: b = a.concat(dag.Word(['a', 'a'])).concat(a)
			sage: a.split(b)
			[DetAutomaton with 3 states and an alphabet of 2 letters,
			 DetAutomaton with 2 states and an alphabet of 2 letters]
			sage: a = dag.AnyWord(['a', 'b'])
			sage: b = a.concat(dag.Word(['a', 'a'])).concat(a)
			sage: c = a.concat(dag.Word(['b', 'b'])).concat(a)
			sage: c.split(b)
			[DetAutomaton with 8 states and an alphabet of 2 letters,
			 DetAutomaton with 5 states and an alphabet of 2 letters]
			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = DetAutomaton([(3, 2, 'a'), (1, 2, 'd')], i=2)
			sage: a.split(b)
			[DetAutomaton with 1 state and an alphabet of 1 letter,
			 DetAutomaton with 2 states and an alphabet of 1 letter]
			sage: b = DetAutomaton([(3, 2, 'a'), (1, 2, 'd')])
			sage: a.split(b)
			[DetAutomaton with 0 state and an alphabet of 1 letter,
			 DetAutomaton with 2 states and an alphabet of 1 letter]
			sage: print(a)
			DetAutomaton with 4 states and an alphabet of 2 letters

		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = dag.AnyWord(['a', 'b'])
			sage: b = dag.AnyWord(['c', 'a'])
			sage: a.split(b)
			[DetAutomaton with 1 state and an alphabet of 1 letter,
			 DetAutomaton with 1 state and an alphabet of 1 letter]
			sage: print(a)
			DetAutomaton with 1 state and an alphabet of 2 letters

		"""
		return [self.intersection(a, simplify=simplify, verb=verb-1), self.intersection(a.complementary(), simplify=simplify, verb=verb-1)]
#		cdef Automaton ap, ap2
#		cdef Dict dC
#		cdef DetAutomaton r, r2
#		cdef list Av
#
#		# complete the automaton a
#		sig_on()
#		CompleteAutomaton(a.a)
#		sig_off()
#		if algo == 1:
#			# make the product
#			d = {}
#			for l in self.A:
#				if l in a.A:
#					d[(l, l)] = l
#
#			r = DetAutomaton(None)
#			r2 = DetAutomaton(None)
#			Av = []
#			sig_on()
#			dv = imagProductDict(d, self.A, a.A, Av=Av)
#			sig_off()
#			if verb:
#				print("Av=%s" % Av)
#				print("dv=%s" % dv)
#			sig_on()
#			dC = getProductDict(d, self.A, a.A, dv=dv, verb=verb)
#			sig_off()
#			if verb:
#				print("dC=")
#				printDict(dC)
#			sig_on()
#			ap = Product(self.a[0], a.a[0], dC, verb)
#			FreeDict(&dC)
#			sig_off()
#			# set final states for the intersection
#			cdef int i, j
#			cdef n1 = self.a.n
#			for i in range(n1):
#				for j in range(a.a.n):
#					ap.e[i+n1*j].final = self.a.e[i].final and a.a.e[j].final
#		else:
#			
#		# complementary of a in self
#		sig_on()
#		ap2 = CopyAutomaton(ap, ap.n, ap.na)
#		sig_off()
#		# set final states
#		for i in range(n1):
#			for j in range(a.a.n):
#				ap2.e[i+n1*j].final = self.a.e[i].final and not a.a.e[j].final
#
#		r.a[0] = ap
#		r.A = Av
#		r2.a[0] = ap2
#		r2.A = Av
#		if simplify:
#			return [r.prune().minimize(), r2.prune().minimize()]
#		else:
#			return [r, r2]

	def shift_op(self, l, int np=1, verb=False):
		"""
		Shift the automaton ON PLACE to recognize the language shifted ``np``
		times by the letter l.
		The new language is the language of words u such that (l^np)u 
		was recognized by self.

		INPUT:

		- ``l`` -- letter to shift
		- ``np`` -- int (default: ``1``) - number of time we shift
		- ``verb`` -- Boolean (default: ``False``) - if True, print
		  debugging informations

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.initial_state
			0
			sage: a.shift_op('a')
			sage: a.initial_state
			1
			sage: a.shift_op('a', 2)
			sage: a.initial_state
			-1

		TESTS::

			sage: from badic import *
			sage: a = dag.AnyWord(['a', 'b'], A2=['a','b'])
			sage: a.shift_op('c')
			Traceback (most recent call last):
			...
			ValueError: 'c' is not a letter of the alphabet ['a', 'b']

		"""
		cdef int i
		try:
			l = self.A.index(l)
		except:
			raise ValueError("%r is not a letter of the alphabet %s" % (l, self.A))
		for i in range(np):
			if self.a.i != -1:
				self.a.i = self.a.e[self.a.i].f[l]

	def shift_list_op(self, list l):
		"""
		Shift the automaton ON PLACE to recognize the language shifted by l (list of letters).
		The new language is the language of words u such that lu 
		was recognized by self.

		INPUT:

		- ``l`` -- list - list of letter to shift

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.initial_state
			0
			sage: a.shift_list_op(['a'])
			sage: a.initial_state
			1

		TESTS::

			sage: from badic import *
			sage: a = dag.Word(['a','b','a','b'])
			sage: a.shift_list_op(['a','b'])
			sage: a.simplify().has_same_language_as(dag.Word(['a','b']))
			True

			sage: a = dag.AnyWord(['a', 'b'], A2=['a', 'b'])
			sage: a.shift_list_op(['a','c'])
			Traceback (most recent call last):
			...
			ValueError: 'c' is not in the alphabet ['a', 'b']

		"""
		for i in l:
			try:
				i = self.A.index(i)
			except:
				raise ValueError("%r is not in the alphabet %s" % (i, self.A))
			if self.a.i != -1:
				self.a.i = self.a.e[self.a.i].f[i]

	def unshift(self, l, int np=1, final=False):
		"""
		Unshift the automaton to recognize the language shifted
		``np`` times by letter ``l``.
		The new language is the languages of words (l^np)u,
		where u is recognized by self and l^np is
		the letter l repeated np times.

		INPUT:

		- ``l`` -- letter to shift

		- ``np``  --  int (default: ``1``)
		  Number of times we shift

		- ``final`` -- Boolean (default: ``False``)
		  if True, the empty word is added to the language

		OUTPUT:

		Return a :class:`DetAutomaton`

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.initial_state
			0
			sage: a.unshift('a')
			DetAutomaton with 5 states and an alphabet of 2 letters
			sage: a.unshift('c')
			DetAutomaton with 5 states and an alphabet of 3 letters
			sage: a.unshift('a', 4)
			DetAutomaton with 8 states and an alphabet of 2 letters
		"""
		cdef DetAutomaton r
		cdef int i
		cdef int ne

		if np <= 0:
			raise ValueError("np must be positive!")

		r = DetAutomaton(None)
		try:
			l = self.A.index(l)
			r.A = self.A[:]
			sig_on()
			r.a[0] = CopyAutomaton(self.a[0], self.a.n+np, self.a.na)
			sig_off()
		except:
			r.A = self.A+[l]
			l = len(self.A) # new letter
			sig_on()
			r.a[0] = CopyAutomaton(self.a[0], self.a.n+np, self.a.na+1)
			sig_off()
		ne = self.a.n # first new state
		for j in range(np):
			for i in range(r.a.na):
				r.a.e[ne+j].f[i] = -1
			if j > 0:
				r.a.e[ne+j].f[l] = ne+j-1
			else:
				r.a.e[ne+j].f[l] = self.a.i
			r.a.e[ne+j].final = final
			sig_check()
		r.a.i = ne+np-1
		return r

	# this function could be written in a more efficient way
	def unshiftl(self, list l):
		"""
		Return a new automaton whose language is the set of words wu,
		where u is recognized by self, and w is the word
		corresponding to the list of indices of letters l.

		INPUT:

		- ``l`` -- list of indices of letters

		OUTPUT:

		Return a :class:`DetAutomaton`

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.unshiftl(['a', 'b'])
			DetAutomaton with 6 states and an alphabet of 2 letters
			sage: a.unshiftl(['c', 'd'])
			DetAutomaton with 6 states and an alphabet of 4 letters

			sage: from badic import *
			sage: a = dag.AnyWord([0,1])
			sage: a.unshiftl([1,0,1])
			DetAutomaton with 4 states and an alphabet of 2 letters

		"""
		a = self
		l.reverse()
		for i in l:
			a = a.unshift(i)
		l.reverse()
		return a

	def copyn(self, verb=False):
		"""
		Convert  a determinist automaton :class:`DetAutomaton` to
		a non determinist automaton :class:`CAutomaton`

		INPUT:

		- ``verb`` -- Boolean (default: ``False``) if True,
		  print debugging informations

		OUTPUT:

		Return a :class:`CAutomaton`

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.copyn()
			CAutomaton with 4 states and an alphabet of 2 letters

			sage: from badic import *
			sage: CAutomaton(dag.AnyWord(['a','b']))
			CAutomaton with 1 state and an alphabet of 2 letters

		TESTS::

			sage: from badic import *
			sage: a = dag.Word(['a','b'])
			sage: CAutomaton(a)
			CAutomaton with 3 states and an alphabet of 2 letters

			sage: a = dag.Word(['a','b'])
			sage: a.copyn().determinize() == a
			True

		"""
		cdef NAutomaton a
		cdef CAutomaton r

		r = CAutomaton(None)
		sig_on()
		a = CopyN(self.a[0], verb)
		sig_off()
		r.a[0] = a
		r.A = self.A[:]
		return r

	def concat(self, DetAutomaton b, det=True, simplify=True, verb=False):
		"""
		Return an automaton recognizing the concatenation of the
		languages of self and ``b``.

		INPUT:

		- ``b`` -- :class:`DetAutomaton`  to concatenate to self

		- ``det``  -- Boolean (default: ``True``) - if True, determinize

		- ``simplify`` -- Boolean (default: ``True``) - if True and if det=True,
		  prune and minimize

		- ``verb`` -- Boolean (default: ``False``) if True, print
		  debugging informations

		OUTPUT:

		Return a :class:`CAutomaton` (if ``det`` is ``False``)
		or  :class:`DetAutomaton` (if ``det`` is ``True``)

		EXAMPLES::

			#. Words starting by 'aa'
				sage: from badic import *
				sage: dag.Word(['a','a']).concat(dag.AnyWord(['a','b']))
				DetAutomaton with 3 states and an alphabet of 2 letters

			#. words containing 'aa'

				sage: a = dag.AnyWord(['a','b'])
				sage: a.concat(dag.Word(['a','a'])).concat(a)
				DetAutomaton with 3 states and an alphabet of 2 letters
			
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = DetAutomaton([(3, 2, 'a'), (1, 2, 'd')], i=2)
			sage: a.concat(b)
			DetAutomaton with 2 states and an alphabet of 3 letters
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.concat(b, det=False)
			CAutomaton with 7 states and an alphabet of 3 letters
			sage: b = DetAutomaton([(3, 2, 'a'), (1, 2, 'd')])
			sage: a.concat(b)
			DetAutomaton with 1 state and an alphabet of 3 letters

		TESTS::

			sage: from badic import *
			sage: a = dag.Word(['a', 'b'])
			sage: b = dag.Word(['a', 'c'])
			sage: a.concat(b).has_same_language_as(dag.Word(['a','b','a','c']))
			True

			sage: a = dag.AnyWord([0])
			sage: b = DetAutomaton([(0,1,0), (0,0,1), (1,0,1), (1,1,1)], i=0, final_states=[1], avoidDiGraph=True)
			sage: a.concat(b)
			DetAutomaton with 2 states and an alphabet of 2 letters

		"""
		cdef DetAutomaton a
		cdef NAutomaton na
		cdef CAutomaton r
		cdef DetAutomaton r2

		if self.A != b.A:
			A = list(set(self.A).union(set(b.A)))
			if verb:
				print("Alphabet Changing (%s, %s -> %s)..." % (self.A, b.A, A))
			a = self.bigger_alphabet(A)
			b = b.bigger_alphabet(A)
		else:
			a = self
			A = self.A
		if verb:
			print("a=%s (A=%s)\nb=%s (A=%s)" % (a, a.A, b, b.A))
		r = CAutomaton(None)
		sig_on()
		na = Concat(a.a[0], b.a[0], verb)
		sig_off()
		r.a[0] = na
		r.A = A

		if det:
			if verb:
				print("Determinize...")
			r2 = r.determinize()
			if simplify:
				if verb:
					print("Prune and minimize...")
				return r2.prune().minimize()
			else:
				return r2
		else:
			return r

	def proj(self, dict d, bint det=True, bint simplify=True, bint verb=False):
		"""
		Project with respect to the dictionary ``d``.
		Give an automaton where labels are replaced according to ``d``.

		INPUT:

		- ``d`` -- dictionary used for the projection

		- ``det``  --  Bool (default: ``true``) - determinize the result or not

		- ``simplify`` -- Bool (default: ``True``) - if True and if
		  det=True, prune and minimize

		- ``verb`` -- Boolean (default: ``False``) - activate or
		  desactivate the verbose mode

		OUTPUT:

		Return a :class:`CAutomaton` (when ``det``=``False``)
		or a :class:`DetAutomaton` (when ``det``=``True``)

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a','b'])
			sage: b = a.product(a)
			sage: d = {('a','a'):'a', ('a','b'):'a', ('b','a'):'b', ('b','b'):'b'}
			sage: b.proj(d)
			DetAutomaton with 3 states and an alphabet of 2 letters

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: d = {'a' : 0, 'b': 0, 'c':1, 'd':1}
			sage: a.proj(d)
			DetAutomaton with 2 states and an alphabet of 1 letter

			sage: a = dag.Word([(0,1), (1,0), (1,1)])
			sage: d = {}
			sage: for i in range(2):
			....:	 for j in range(2):
			....:		 d[(i,j)] = i-j
			sage: a.proj(d).has_same_language_as(dag.Word([-1, 1, 0]))
			True

		TESTS::

			sage: from badic import *
			sage: a = dag.Word(['a','b'])
			sage: b = a.product(a)
			sage: d = {('a','a'):'a', ('a','b'):'a', ('b','a'):'b', ('b','b'):'b'}
			sage: a.has_same_language_as(b.proj(d))
			True

		"""
		cdef NAutomaton a
		cdef Dict dC
		cdef CAutomaton r
		cdef DetAutomaton r2

		r = CAutomaton(None)
		A2 = []
		sig_on()
		d1 = imagDict(d, self.A, A2=A2)
		sig_off()
		if verb:
			print("d1=%s, A2=%s" % (d1, A2))
		sig_on()
		dC = getDict(d, self.A, d1=d1)
		a = Proj(self.a[0], dC, verb)
		FreeDict(&dC)
		sig_off()
		r.a[0] = a
		r.A = A2
		if det:
			r2 = r.determinize()
			if simplify:
				return r2.prune().minimize()
			else:
				return r2
		else:
			return r

	def proji(self, int i, bint det=True, bint simplify=True, bint verb=False):
		"""
		Assuming that the alphabet of the automaton are iterable, project on
		the ith coordinate.
		Give a new automaton where labels are replaced by the projection on
		the ith coordinate.

		INPUT:

		- ``i`` -- int - coordinate of projection

		- ``det``  --  Bool (default: ``true``) - determinize or not the result

		- ``simplify`` -- Bool (default: ``True``) - if True and if det=True, prune
		  and minimize

		- ``verb`` -- Boolean (default: ``False``) - to activate or
		  desactivate the verbose mode

		OUTPUT:

		Return a :class:`CAutomaton` (``det``=``False``)
		or :class:`DetAutomaton` (``det``=``True``)


		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word([('a', 'b'), ('b', 'a'), ('c', 'a')])
			sage: a.proji(1)
			DetAutomaton with 4 states and an alphabet of 2 letters

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'abc')], i=0)
			sage: b = a.proji(0)
			sage: b
			DetAutomaton with 2 states and an alphabet of 1 letter
			sage: b == DetAutomaton([(1, 0, 'a')], i=1)
			True

		TESTS::

			sage: from badic import *
			sage: a = dag.Word([('a', 'b'), ('b', 'a'), ('c', 'a')])
			sage: a.proji(1).has_same_language_as(dag.Word(['b', 'a', 'a']))
			True

			sage: a = dag.Word([0,1])
			sage: a.proji(0)
			Traceback (most recent call last):
			...
			TypeError: object of type 'sage.rings.integer.Integer' has no len()

		"""
		cdef dict d

		if i < 0:
			raise ValueError("index i=%d cannot be negative" % i)
		d = {}
		for l in self.A:
			if i < len(l):
				d[l] = l[i]
			else:
				raise ValueError("index i=%d must be smaller than the dimension of the label %s" % (i, l))
		return self.proj(d, det=det, simplify=simplify, verb=verb)

	def duplicate(self, d, verb=False):
		"""
		Replace every transition of self labeled by a letter l
		by a list of transitions labeled by elements of d[l].
		The result is assumed deterministic !!!

		INPUT:

		- ``d``  -- dictionary giving a list a new letters for each letter of the alphabet of self

		- ``verb`` -- Boolean (default: ``False``) fix to ``True``
		  for activation the verbose mode

		OUTPUT:

		Return a new :class:`DetAutomaton` with new letters

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: d = { 'a' : 'ac', 'b': 'cc', 'ca':'c', 'd':'b'}
			sage: b = a.duplicate(d)
			sage: b.alphabet	# random
			['a', 'c']
			
			sage: from badic import *
			sage: a = dag.Word(['a', 'b', 'c'])
			sage: d = { 'a' : [0,1], 'b': [0], 'c':[]}
			sage: b = a.duplicate(d)
			sage: b.alphabet	# random
			[0, 1]
			sage: b.has_empty_language()
			True
		"""
		cdef Automaton a
		cdef InvertDict dC
		cdef DetAutomaton r

		r = DetAutomaton(None)
		A2 = []
		sig_on()
		d1 = imagDict2(d, self.A, A2=A2)
		sig_off()
		if verb:
			print("d1=%s, A2=%s" % (d1, A2))
		sig_on()
		dC = getDict2(d, self.A, d1=d1)
		sig_off()
		if verb:
			sig_on()
			printInvertDict(dC)
			sig_off()
		sig_on()
		a = Duplicate(self.a[0], dC, len(A2), verb)
		sig_off()
		if verb:
			print("end...")
		sig_on()
		FreeInvertDict(dC)
		sig_off()
		r.a[0] = a
		r.A = A2
		return r

	def relabel(self, dict d):
		"""
		Change letters of the :class:`DetAutomaton` ON PLACE,
		with respect to the dictionnary ``d``.
		The dictionary is assumed to be one-to-one.

		INPUT:

		 - ``d``  -- dictionary that gives new letters from the old ones

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: d = { 'a' : 'a', 'b': 'c', 'c':'b', 'd':'b'}
			sage: a.relabel(d)
			sage: a.alphabet	# random
			['a', 'c']
			
			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a', 'b'], A=['a', 'b'])
			sage: d = {'a':0, 'b':1}
			sage: a.relabel(d)
			sage: a.alphabet
			[0, 1]
			sage: a.has_same_language_as(dag.Word([0, 1]))
			True

		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = dag.AnyWord([0,1])
			sage: a.relabel({})
			Traceback (most recent call last):
			...
			KeyError: 0

		"""
		self.A = [d[c] for c in self.A]

	def permut(self, list A, bint verb=False):
		"""
		Permutes (and eventually remove) letters of the alphabet,
		and return permuted new :class:`DetAutomaton` with
		the same language restricted to the new alphabet.

		INPUT:

		- ``A``  -- list of letters in the new order
		  (with potentially less letters)

		- ``verb`` -- Boolean (default: ``False``) set to ``True`` to
		  activate the verbose mode

		OUTPUT:

		Return new :class:`DetAutomaton`

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word([0,1,2])
			sage: a.alphabet	# random
			[0, 1, 2]
			sage: b = a.permut([2,1,0])
			sage: b.alphabet	# random
			[2, 1, 0]
			sage: a.has_same_language_as(b)
			True

			sage: a = dag.AnyWord(['a', 'b', 'c'])
			sage: a.alphabet	# random
			['a', 'c', 'b']
			sage: b = a.permut(['a', 'b'])
			sage: b.alphabet	# random
			['a', 'b']
			sage: b.has_same_language_as(dag.AnyWord(['a', 'b']))
			True

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: l = [ 'b', 'c', 'a']
			sage: b = a.permut(l)
			sage: b.alphabet	# random
			['b', 'a']

		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a', 'b'])
			sage: b = a.permut([0,1])
			sage: b.alphabet
			[]

		"""
		cdef Automaton a
		cdef int *l
		cdef int i, na
		cdef DetAutomaton r

		if verb:
			print("A=%s" % A)
		r = DetAutomaton(None)
		l = <int*>malloc(sizeof(int) * len(A))
		if l is NULL:
			raise MemoryError("Failed to allocate memory for l in "
							  "permut")
		for i in range(self.a.na):
			l[i] = -1
		d = {}
		for i, c in enumerate(self.A):
			d[c] = i
		r.A = []
		for i, c in enumerate(A):
			if c in d:
				l[i] = d[c]  # l gives the old index from the new one
				r.A.append(c)
		if verb:
			str = "l=["
			for i in range(len(A)):
				str += " %s" % l[i]
			str += " ]"
			print(str)
		sig_on()
		a = Permut(self.a[0], l, len(A), verb)
		free(l)
		sig_off()
		r.a[0] = a
		r.a.na = len(A)
		return r

	def permut_op(self, list A, bint verb=False):
		"""
		Permutes (and eventually remove) letters of the alphabet ON PLACE,
		without changing the language restricted to the new alphabet.

		INPUT:

		- ``A``  -- list of letters in the new order (number can be less to
		  the alphabet)
		- ``verb`` -- Boolean (default: ``False``) fix to ``True`` for
		  activation the verbose mode

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word([0,1,2])
			sage: a.alphabet	# random
			[0, 1, 2]
			sage: a.permut_op([2,1,0])
			sage: a.alphabet	# random
			[2, 1, 0]
			sage: a.has_same_language_as(dag.Word([0,1,2]))
			True

			sage: a = dag.AnyWord(['a', 'b', 'c'])
			sage: a.alphabet	# random
			['a', 'c', 'b']
			sage: a.permut_op(['a', 'b'])
			sage: set(a.alphabet) == {'a', 'b'}
			True
			sage: a.has_same_language_as(dag.AnyWord(['a', 'b']))
			True

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: l = [ 'b', 'c', 'a']
			sage: a.permut_op(l)
			sage: a.alphabet
			['b', 'a']

		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a', 'b'])
			sage: a.permut_op([0,1])
			sage: a.alphabet
			[]

		"""
		cdef int *l
		cdef int i
		cdef int nA = len(A)
		cdef list A2

		if verb:
			print("A=%s" % A)
		sig_on()
		l = <int*>malloc(sizeof(int) * nA)
		sig_off()
		if l is NULL:
			raise MemoryError("Failed to allocate memory for l in "
							  "permut_op")
		for i in range(self.a.na):
			l[i] = -1
		d = {}
		for i, c in enumerate(self.A):
			d[c] = i
		A2 = []
		for i, c in enumerate(A):
			if c in d:
				l[i] = d[c]  # l gives the old index from the new one
				A2.append(c)
				sig_check()
		if verb:
			str = "l=["
			for i in range(len(A)):
				str += " %s" % l[i]
			str += " ]"
			print(str)
		sig_on()
		PermutOP(self.a[0], l, len(A), verb)
		free(l)
		sig_off()
		self.A = A2
		self.a.na = len(A2)

	def mirror_det(self):
		"""
		Return a :class:`DetAutomaton`, whose language is the mirror of the
		language of self.
		We assume the result to be deterministic!
		If it is not the case, you should rather use mirror()
		to get a correct result.

		OUTPUT:

		Return a :class:`DetAutomaton`

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a', 'b', 'c', 'b'])
			sage: b = a.mirror_det()
			sage: b
			DetAutomaton with 5 states and an alphabet of 3 letters
			sage: b.has_same_language_as(dag.Word(['b', 'c', 'b', 'a']))
			True

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.mirror_det()
			DetAutomaton with 4 states and an alphabet of 2 letters

		TESTS::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([('a','a',0), ('a','b',1), ('b','a',0)], i='a')
			sage: b = a.mirror_det()
			sage: b
			DetAutomaton with 2 states and an alphabet of 2 letters
			sage: b.has_same_language_as(a.mirror().determinize())
			False
			sage: b.states
			['a', 'b']
		"""
		cdef DetAutomaton r

		r = DetAutomaton(None)
		sig_on()
		r.a[0] = MirrorDet(self.a[0])
		sig_off()
		r.A = self.A
		r.S = self.S
		return r

	def mirror(self):
		"""
		Return a :class:`CAutomaton`, whose language is the mirror of the
		language of self.

		OUTPUT:

		Return a :class:`CAutomaton`

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a', 'b', 'c', 'b'])
			sage: b = a.mirror()
			sage: b
			CAutomaton with 5 states and an alphabet of 3 letters
			sage: b.determinize().has_same_language_as(dag.Word(['b', 'c', 'b', 'a']))
			True

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.mirror()
			CAutomaton with 4 states and an alphabet of 2 letters

		TESTS::
		
			sage: from badic.cautomata import *
			sage: a = DetAutomaton([('a','a',0), ('a','b',1), ('b','a',0)], i='a')
			sage: a.mirror()
			CAutomaton with 2 states and an alphabet of 2 letters

		"""
		cdef CAutomaton r

		r = CAutomaton(None)
		sig_on()
		r.a[0] = Mirror(self.a[0])
		sig_off()
		r.A = self.A
		r.S = self.S
		return r

	def strongly_connected_components(self, bint no_trivials=False, bint only_terminal=False, bint get_graph=False):
		r"""
		Determine a partition into strongly connected components.
		A strongly connected component is a minimal subset of the set
		of states such that
		there is no path going outside of the subset, from a state of
		the subset to a state of the subset.
		This computation is done by computing an efficient topological
		ordering of the graph.

		INPUT:

		- ``no_trivials`` -- Bool (default: ``False``) If True, do not take into
		  account components without any transition from itself to itself
		  (such component contains only one element).

		- ``only_terminal`` -- Bool (default: ``False``) If True, return only terminal components.

		- ``get_graph`` -- Bool (default: ``False``) If True, return also the graph of strongly connected components (without loops).

		OUTPUT:

		Return the list of strongly connected components, given as a list of
		list of states.

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a', 'b']).concat(dag.AnyWord(['a', 'b']))
			sage: a
			DetAutomaton with 3 states and an alphabet of 2 letters
			sage: a.strongly_connected_components()	 # random
			[[0], [2], [1]]
			
			sage: a = dag.Random(20, [0, 1, 2])
			sage: a.strongly_connected_components()	 # random
			[[4],
			 [12, 15],
			 [2, 6, 13, 18],
			 [3, 5, 7, 8, 10, 11, 16, 19],
			 [0, 14],
			 [17],
			 [1],
			 [9]]

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (1,0,'b'), (2, 3, 'b')], i=0)
			sage: a.strongly_connected_components()	 # random
			[[0, 1], [3], [2]]

			sage: a = DetAutomaton([('a', 'a', 0), ('a', 'b', 1), ('b', 'a', 0)], i='a')
			sage: a.strongly_connected_components()	 # random
			[[0, 1]]
		
		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = DetAutomaton([(0, 1, 'a'), (1,0,'b'), (2, 3, 'b')], i=0)
			sage: {frozenset(c) for c in a.strongly_connected_components()} == {frozenset({3}), frozenset({0, 1}), frozenset({2})}
			True

		"""
		cdef int* l
		cdef int ncc
		cdef list l2
		cdef int i,j,k

		sig_on()
		l = <int*>malloc(sizeof(int) * self.a.n)
		sig_off()
		if l is NULL:
			raise MemoryError("Failed to allocate memory for l in "
							  "strongly_connected_component.")
		sig_on()
		ncc = StronglyConnectedComponents3(self.a[0], l)
		sig_off()
		#print(l)
		# inverse la liste
		l2 = [[] for _ in range(ncc)] #{}
		for i in range(self.a.n):
			l2[l[i]].append(i)
		if only_terminal or get_graph:
			L = set()
			term = [True for _ in range(ncc)]
			for i in range(self.a.n):
				if not term[l[i]]:
					continue
				for j in range(self.a.na):
					k = self.a.e[i].f[j]
					if k == -1:
						continue
					if l[i] != l[k]:
						if get_graph:
							L.add((l[i], self.A[j], l[k]))
						else:
							term[l[i]] = False
		if no_trivials:
			l3 = [] 
			for i in range(ncc): #l2.keys():
				if len(l2[i]) == 1:
					trivial = True
					for j in range(len(self.A)):
						if self.a.e[l2[i][0]].f[j] == l2[i][0]:
							trivial = False
							break
					if not trivial:
						# on retire cette composante qui est triviale
						l3.append(i)
			l2 = l3
		sig_on()
		free(l)
		sig_off()
		if only_terminal:
			return [L for i,L in enumerate(l2) if term[i]]
		elif get_graph:
			return l2,L
			#L = list(L)
			#if self.a.i == -1:
			#	return l2, DetAutomaton(L, avoidDiGraph=True)
			#return l2, DetAutomaton(L, avoidDiGraph=True, i=l[self.a.i])
		return l2

	def is_strongly_connected (self, verb=False):
		r"""
		Determine whether the automaton is strongly connected.

		OUTPUT:
			A bool

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Word("elephant")
			sage: a.is_strongly_connected()
			False
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0)])
			sage: a.is_strongly_connected()
			True

		"""
		cdef int* l
		cdef int ncc

		sig_on()
		l = <int*>malloc(sizeof(int) * self.a.n)
		sig_off()
		if l is NULL:
			raise MemoryError("Failed to allocate memory for l in "
							  "strongly_connected_component.")
		sig_on()
		ncc = StronglyConnectedComponents3(self.a[0], l)
		free(l)
		sig_off()
		if verb > 0:
			print("ncc = %s" % ncc)
		return ncc == 1

	def sub_automaton(self, l, bint keep_states_labels=True, bint check_several=False, bint verb=False):
		"""
		Compute the sub automaton whose states are given by the set ``l``.

		INPUT:

		- ``l``  -- list (or iterable) of states to keep

		- ``verb`` -- Boolean (default: ``False``) set to ``True`` to activate
		  the verbose mode

		- ``keep_states_labels``  -- Boolean (default: ``True``) keep the labels of states

		OUTPUT:

		Return a :class:`DetAutomaton`

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a', 'b', 'c'])
			sage: a.sub_automaton([0,1])
			DetAutomaton with 2 states and an alphabet of 3 letters

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = a.sub_automaton([0,1])
			sage: b
			DetAutomaton with 2 states and an alphabet of 2 letters
			sage: b.has_same_language_as(a)
			True

		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a','b','a'])
			sage: a.sub_automaton([3,4])
			Traceback (most recent call last):
			...
			ValueError: Element 4 of the list given is not a state (i.e. between 0 and 4).

		"""
		cdef DetAutomaton r
		cdef int i, n
		if check_several:
			# check that the list l is correct
			if len(l) != len(set(l)):
				raise ValueError("A state of the list appears several times")
		n = self.a.n
		for i in l:
			if i < 0 or i >= n:
				raise ValueError("Element %s of the list given is not a state (i.e. between 0 and %s)."% (i, n))
		r = DetAutomaton(None)
		sig_on()
		r.a[0] = SubAutomaton(self.a[0], list_to_Dict(l), verb)
		sig_off()
		r.A = self.A
		if keep_states_labels and self.S is not None:
			r.S = []
			for i in l:
				r.S.append(self.S[i])
		return r

	def simplify(self):
		"""
		Prune and minimize self.
		This gives a canonical automaton for the language of self.

		OUTPUT:

		:class:`DetAutomaton`

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a','b','a','b'])
			sage: a.shift_list_op(['a','b'])
			sage: a
			DetAutomaton with 5 states and an alphabet of 2 letters
			sage: a.simplify()
			DetAutomaton with 3 states and an alphabet of 2 letters

		"""
		return self.prune().minimize()

	def minimize(self, verb=False):
		"""
		Compute the minimal automaton
		by Hopcroft's algorithm
		see [Hopcroft]

		INPUT:

		 - ``verb`` -- Boolean (default: ``False``) set to ``True`` to
		   activate the verbose mode


		OUTPUT:

		Return a minimized :class:`DetAutomaton`

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([('a','b',0),('a','c',1),('b','a',0),
			....:				   ('b','d',1),('c','e',0),('c','f',1),
			....:				   ('d','e',0),('d','f',1),('e','e',0),
			....:				   ('e','f',1),('f','f',0),('f','f',1)],
			....:				   i='a', final_states=['c','d','e'])
			sage: a.minimize()
			DetAutomaton with 3 states and an alphabet of 2 letters

			sage: a = DetAutomaton([(0,1,0), (0,5,1), (1,2,1), (1,6,0),
			....:				   (2,0,0), (2,2,1), (3,2,0), (3,6,1),
			....:				   (4,7,0), (4,5,1), (5,2,0), (5,6,1),
			....:				   (6,6,0), (6,4,1), (7,6,0), (7,2,1)],
			....:				   i=0, final_states=[2,6])
			sage: a.minimize()
			DetAutomaton with 5 states and an alphabet of 2 letters

			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.minimize()
			DetAutomaton with 3 states and an alphabet of 2 letters

			sage: a = DetAutomaton([(10,10,'x'),(10,20,'y'),(20,20,'z'),\
					(20,10,'y'),(20,30,'x'),(30,30,'y'),(30,10,'z'),(30,20,'x'),\
					(10,30,'z')], i=10)
			sage: a.minimize()
			DetAutomaton with 1 state and an alphabet of 3 letters

		TESTS::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([('a','b',0),('a','c',1),('b','a',0),
			....:				   ('b','d',1),('c','e',0),('c','f',1),
			....:				   ('d','e',0),('d','f',1),('e','e',0),
			....:				   ('e','f',1),('f','f',0),('f','f',1)],
			....:				   i='a', final_states=['c','d','e'])
			sage: b = DetAutomaton([(0, 0, 0), (0, 2, 1), (1, 1, 0),
			....:				   (1, 0, 1), (2, 2, 0), (2, 2, 1)],
			....:				  A=[0, 1], i=1, final_states=[0])
			sage: a.minimize() == b
			True
			
			sage: a = DetAutomaton([(0,1,0), (0,5,1), (1,2,1), (1,6,0),
			....:				   (2,0,0), (2,2,1), (3,2,0), (3,6,1),
			....:				   (4,7,0), (4,5,1), (5,2,0), (5,6,1),
			....:				   (6,6,0), (6,4,1), (7,6,0), (7,2,1)],
			....:				   i=0, final_states=[2,6])
			sage: b = DetAutomaton([(0, 0, 0), (0, 2, 1), (1, 3, 0),
			....:				   (1, 0, 1), (2, 4, 0), (2, 1, 1),
			....:				   (3, 2, 0), (3, 3, 1), (4, 0, 0),
			....:				   (4, 3, 1)], i=2, final_states=[0, 3])
			sage: a.minimize() == b
			True

			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], A=['a','b'], i=0)
			sage: a.minimize(verb=True)
			transition i[0][0] = [ ]
			transition i[0][1] = [ ]
			transition i[1][0] = [ 0 ]
			transition i[1][1] = [ ]
			transition i[2][0] = [ ]
			transition i[2][1] = [ ]
			transition i[3][0] = [ ]
			transition i[3][1] = [ 2 ]
			transition i[4][0] = [ 1 2 3 4 ]
			transition i[4][1] = [ 0 1 3 4 ]
			partition = [ 0 1 2 3 4 ]
			partitioni = [ 0 1 2 3 4 ]
			Initial partition :
			class 0 : 0 1 2 3
			class 1 : 4
			split 1 0...
			new visited class : 0 (1 parent of 4)
			re-visited class : 0 (2 parent of 4)
			re-visited class : 0 (3 parent of 4)
			new visited class : 1 (4 parent of 4)
			class 0 : 1 2 3 0
			class 1 : 4
			2 class encountered
			class 0 : l = 0 3 4 = h
			class 1 : l = 4 5 5 = h
			split 1 1...
			new visited class : 2 (0 parent of 4)
			new visited class : 0 (1 parent of 4)
			re-visited class : 0 (3 parent of 4)
			new visited class : 1 (4 parent of 4)
			class 0 : 1 3 2
			class 1 : 4
			class 2 : 0
			3 class encountered
			class 2 : l = 3 4 4 = h
			class 0 : l = 0 2 3 = h
			class 1 : l = 4 5 5 = h
			split 3 0...
			class 0 : 1 3
			class 1 : 4
			class 2 : 0
			class 3 : 2
			0 class encountered
			split 3 1...
			class 0 : 1 3
			class 1 : 4
			class 2 : 0
			class 3 : 2
			0 class encountered
			split 2 0...
			class 0 : 1 3
			class 1 : 4
			class 2 : 0
			class 3 : 2
			0 class encountered
			split 2 1...
			class 0 : 1 3
			class 1 : 4
			class 2 : 0
			class 3 : 2
			0 class encountered
			Final partition :
			class 0 : 1 3
			class 1 : 4
			class 2 : 0
			class 3 : 2
			a.i = 0 class 2
			removes the sink state  1...
			DetAutomaton with 3 states and an alphabet of 2 letters
		"""
		cdef DetAutomaton r

		r = DetAutomaton(None)
		sig_on()
		r.a[0] = Minimise(self.a[0], verb)
		sig_off()
		r.A = self.A
		return r

	def adjacency_matrix(self, sparse=None):
		"""
		Compute the adjacency matrix of the :class:`DetAutomaton`

		INPUT:

		- ``sparse`` -- indicate if the return matrix is sparse or not

		OUTPUT:

		Return the corresponding adjacency matrix

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.adjacency_matrix()
			[0 1 0 0]
			[0 0 0 0]
			[0 0 0 1]
			[0 0 0 0]

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word(['a','b','a','c'])
			sage: a.adjacency_matrix()
			[0 1 0 0 0]
			[0 0 1 0 0]
			[0 0 0 1 0]
			[0 0 0 0 1]
			[0 0 0 0 0]

			sage: a = dag.AnyLetter(['a','b','c'])
			sage: a.adjacency_matrix()
			[0 3]
			[0 0]

			sage: a = dag.Random(500, ['a', 'b', 'c'])
			sage: a.adjacency_matrix()
			500 x 500 sparse matrix over Integer Ring (use the '.str()' method to see the entries)

		"""
		cdef int i, j, f
		cdef dict d

		if sparse is None:
			if self.a.n <= 128:
				sparse = False
			else:
				sparse = True

		d = {}
		for i in range(self.a.n):
			for j in range(self.a.na):
				f = self.a.e[i].f[j]
				if f != -1:
					if (i, f) in d:
						d[(i, f)] += 1
					else:
						d[(i, f)] = 1
		from sage.matrix.constructor import matrix
		from sage.rings.integer_ring import IntegerRing
		return matrix(IntegerRing(), self.a.n, self.a.n, d, sparse=sparse)

	def delete_state(self, int i):
		"""
		Gives a copy of the :class:`DetAutomaton` but
		without the state ``i``.

		INPUT:

		- ``i``  - int - the state to remove

		OUTPUT:

		:class:`DetAutomaton`

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.delete_state(2)
			DetAutomaton with 3 states and an alphabet of 2 letters
			sage: a.delete_state(2).delete_state(1)
			DetAutomaton with 2 states and an alphabet of 2 letters

		TESTS::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([('a', 'a', 0), ('a', 'b', 1), ('b', 'a', 0), ('b', 'c', 1), ('c', 'a', 0)], i='a')
			sage: a = a.delete_state(a.states.index('b'))
			sage: b = DetAutomaton([('a', 'a', 0), ('c', 'a', 0)], A=[0, 1], i='a')
			sage: a == b
			True

			sage: from badic.cautomata_generators import *
			sage: a = dag.AnyWord(['a','b'])
			sage: a.delete_state(1)
			Traceback (most recent call last):
			...
			ValueError: 1 is not a state (should be between 0 and 0)

		"""
		cdef DetAutomaton r
		if i < 0 or i >= self.a.n:
			raise ValueError("%s is not a state (should be between 0 and %s)"%(i, self.a.n-1))
		r = DetAutomaton(None)
		sig_on()
		r.a[0] = DeleteVertex(self.a[0], i)
		sig_off()
		r.A = self.A
		if self.S is not None:
			r.S = [s for j,s in enumerate(self.S) if j != i]
		return r

	def delete_state_op(self, int i):
		"""
		Delete vertex ``i`` on place.

		INPUT:

		- ``i``  -- int number of vertex to remove

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.delete_state_op(2)
			sage: a
			DetAutomaton with 3 states and an alphabet of 2 letters
			sage: a.delete_state_op(1)
			sage: a
			DetAutomaton with 2 states and an alphabet of 2 letters

		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = dag.AnyWord(['a','b'])
			sage: a.delete_state_op(1)
			Traceback (most recent call last):
			...
			ValueError: 1 is not a state (should be between 0 and 0)

		"""
		if i < 0 or i >= self.a.n:
			raise ValueError("%s is not a state (should be between 0 and %s)"%(i, self.a.n-1))
		sig_on()
		DeleteVertexOP(self.a, i)
		sig_off()
		if self.S is not None:
			self.S = [s for j,s in enumerate(self.S) if j != i]

	def spectral_radius(self, bint approx=True, bint couple=False, bint only_non_trivial=False, bint verb=False):
		"""
		Return the spectral radius of the underlying graph.

		INPUT:

		- ``approx`` - Boolean (default: ``True``) If True gives an approximation,
		  otherwise gives the exact value as an algebraic number. 

		- ``only_non_trivial`` - Boolean (default: ``False``) - if True,
		  don't take into account strongly connected components of
		  cardinality one.

		- ``couple``- Boolean (default: ``False``) - return a interval
		containing the spectral radius (only when ``approx`` is ``True``)

		- ``verb`` - Boolean (default: ``False``) - set to ``True`` to activate
		  the verbose mode

		OUTPUT:

		A positive real algebraic number if approx is False,
		and an float number otherwise.

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.spectral_radius()
			0.0000000000...
			
			sage: a = DetAutomaton([(10,10,'a'),(10,20,'b'),(20,10,'a')], i=10)
			sage: a.spectral_radius()
			1.61803398...
			
			sage: a = DetAutomaton([(10,10,'a'),(10,20,'b'),(20,10,'a')], i=10)
			sage: a.spectral_radius(couple=True)
			(1.61803398..., 1.61803398...)
			
			sage: a = DetAutomaton([(10,10,'a'),(10,20,'b'),(20,10,'a')], i=10)
			sage: a.spectral_radius(approx=False).minpoly()
			x^2 - x - 1

		"""
		a = self.minimize()
		if verb:
			print("minimal Automata : %s" % a)
		l = a.strongly_connected_components()
		if verb:
			print("%s component strongly connex." % len(l))
		r = 0  # valeur propre maximale trouvée
		spm = (0,0) #encadrement de l'approximation maximale trouvée
		for c in l:
			if not only_non_trivial or len(c) > 1:
				if verb:
					print("component with %s states..." % len(c))
				b = a.sub_automaton(c)
				if approx:
					g = b.get_DiGraph()
					if verb:
						print("g=%s" % g)
					if g.is_aperiodic():
						if verb:
							print("aperiodic")
						sp = g.spectral_radius()
						if verb:
							print("sp=%s" % sp)
					else:
						if verb:
							print("non aperiodic")
						r = max(b.adjacency_matrix().charpoly().real_roots())
						sp = (r,r)
					spm = max([sp, spm], key=lambda x:x[0])
					if verb:
						print("spm=%s" % spm)
				else:
					m = b.adjacency_matrix()
					cp = m.charpoly()
					fs = cp.factor()
					if verb:
						print(fs)
					for f in fs:
						if verb:
							print(f)
						from sage.functions.other import real_part
						from sage.rings.qqbar import AlgebraicRealField
						r = max([ro[0] for ro in f[0].roots(ring=AlgebraicRealField())] + [r])
		if approx:
			if couple:
				return spm
			else:
				return spm[1]
		else:
			return r

	def has_empty_language(self):
		r"""
		Test if the  :class:`DetAutomaton` has an empty language.

		OUTPUT:

		Return ``True`` the :class:`DetAutomaton` has a empty language
		``False`` if not

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.has_empty_language()
			False

		"""
		sig_on()
		res = emptyLanguage(self.a[0])
		answ = c_bool(res)
		sig_off()
		return answ

	def has_same_language_as(self, DetAutomaton a, bint minimized=False,
						bint pruned=False, bint verb=False):
		"""
		Test if the languages of :class:`DetAutomaton` ``self`` and ``a`` are
		equal or not.
		If the alphabets are differents it returns False, even if the automata describe the same languages.

		INPUT:

		- ``a``  -- the :class:`DetAutomaton` to compare

		- ``minimized``  -- (default: ``False``) if minimization is
		  required or not

		- ``pruned``  -- (default: ``False``) if emondation is required or not

		- ``verb`` -- Boolean (default: ``False``) set to ``True`` to activate
		  the verbose mode

		OUTPUT:

		Return ``True`` if the both :class:`DetAutomaton` have
		the same language and same alphabet ``False`` if not

		EXAMPLES::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = DetAutomaton([(3, 2, 'a'), (1, 2, 'd')], i=3)
			sage: c = DetAutomaton([(3, 2, 'd'), (1, 2, 'c')], i=2)
			sage: a.has_same_language_as(b)
			False
			sage: a.has_same_language_as(c)
			False
			sage: c = DetAutomaton([(3, 2, 'd'), (1, 2, 'c')])
			sage: a.has_same_language_as(c)
			False

			sage: from badic.cautomata_generators import *
			sage: a = DetAutomaton([(0,0,0), (0,1,1), (1,1,0), (1,1,1)], i=0)
			sage: dag.AnyWord([0,1]).has_same_language_as(a)
			True
		"""
		cdef Dict d
		cdef int i, j
		if set(self.A) != set(a.A):
			return False
		sig_on()
		d = NewDict(self.a.na)
		sig_off()
		for i in range(self.a.na):
			for j in range(a.a.na):
				if self.A[i] == a.A[j]:
					d.e[i] = j
					if verb:
						print("%d -> %d" % (i, j))
					break
				sig_check()
		if verb:
			sig_on()
			printDict(d)
			sig_off()
		sig_on()
		res = equalsLanguages(self.a, a.a, d, minimized, pruned, verb)
		answ = c_bool(res)
		FreeDict(&d)
		sig_off()
		sig_on()
		FreeDict(&d)
		sig_off()
		return answ

	def intersect(self, DetAutomaton a, bint verb=False):
		"""
		Determine if the languages of the :class:`DetAutomaton` ``self``
		and ``a`` have a non-empty intersection.

		INPUT:

		-  ``a``  -- the :class:`Detautomaton` to intersect

		- ``verb`` -- Boolean (default: ``False``) True to activate
		  the verbose mode

		OUTPUT:

		Return ``True`` if the intersection of the languages is non-empty,
		return ``False`` otherwise.

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("ababb")
			sage: b = dag.Word("aba")
			sage: a.intersect(b)
			False
			sage: a.prefix_closure().intersect(b)
			True

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: b = DetAutomaton([(3, 2, 'a'), (1, 2, 'd')], i=2)
			sage: a.intersect(b)
			True
			sage: b = DetAutomaton([(3, 2, 'a'), (1, 2, 'd')])
			sage: a.intersect(b)
			False
		"""
		sig_on()
		res = Intersect(self.a[0], a.a[0], verb)
		answ = c_bool(res)
		sig_off()
		return answ

	def find_word(self, bint verb=False):
		"""
		Find a word in the language of the automaton

		INPUT:

		- ``verb`` -- (default: ``False``)  the verbose parameter

		OUTPUT:

		return a word of the language as a list
		of letters if it exists, otherwise return None

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("gabian")
			sage: a.find_word()
			['g', 'a', 'b', 'i', 'a', 'n']

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.find_word()
			[]
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')])
			sage: a.find_word()
			
			sage: a = DetAutomaton([(0, 0, 'x'), (0, 1, 'y')], i=0, final_states=[1])
			sage: a.find_word()
			['y']
		"""
		cdef Dict w
		cdef list r

		sig_on()
		res = findWord(self.a[0], &w, verb)
		sig_off()
		if not res:
			return None
		r = []
		for i in range(w.n):
			r.append(self.A[w.e[i]])
		sig_on()
		FreeDict(&w)
		sig_off()
		return r

	def shortest_word(self, i=None, f=None, bint verb=False):
		"""
		Compute a shortest words of the language of self.

		INPUT:

		- ``i`` -- (default: None)  the initial state

		- ``f`` -- (default: None)  the final state

		- ``verb`` -- (default: False)  the verbose parameter

		OUTPUT:

		return a word, as list of letters, or None if the language is empty.

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("gabian")
			sage: a.shortest_word()
			['g', 'a', 'b', 'i', 'a', 'n']

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')])
			sage: a.shortest_word(i=2, f=3)
			['b']

			sage: a = DetAutomaton([(0, 0, 'x'), (0, 1, 'y')], i=0, final_states=[1])
			sage: a.shortest_word()
			['y']

			sage: a = dag.Empty(['a', 'b'])
			sage: a.shortest_word()
			

		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = dag.AnyWord("ab")
			sage: a.shortest_word(i=-2)
			Traceback (most recent call last):
			...
			ValueError: -2 is not a state (i.e. between 0 and 0)

			sage: a = dag.AnyWord("ab")
			sage: a.shortest_word(f=-2)
			Traceback (most recent call last):
			...
			ValueError: -2 is not a state (i.e. between 0 and 0)

		"""
		cdef Dict w
		if i is None:
			i = self.a.i
		else:
			if i < -1 or i >= self.a.n:
				raise ValueError("%s is not a state (i.e. between 0 and %s)" % (i, self.a.n-1))
		if f is None:
			f = -1
		else:
			if f < -1 or f >= self.a.n:
				raise ValueError("%s is not a state (i.e. between 0 and %s)" % (f, self.a.n-1))
		sig_on()
		res = shortestWord(self.a[0], &w, i, f, verb)
		sig_off()
		if not res:
			return None
		r = []
		for i in range(w.n):
			r.append(self.A[w.e[i]])
		sig_on()
		FreeDict(&w)
		sig_off()
		return r

	def shortest_words(self, i=None, bint verb=False):
		"""
		Compute the list of shortest words from the initial state to every state.

		INPUT:

		- ``i`` -- (default: None) - the initial state

		- ``verb`` -- Bool (default: False) - the verbose parameter

		OUTPUT:

		return a list of words, as a list of list of letters.

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("abaa")
			sage: a.shortest_words()
			[[], ['a'], ['a', 'b'], ['a', 'b', 'a'], ['a', 'b', 'a', 'a']]

			sage: a = dag.AnyWord("ab")
			sage: a.shortest_words()
			[[]]

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.shortest_words()
			[[], ['a'], [], []]

		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = dag.AnyWord("ab")
			sage: a.shortest_words(i=-2)
			Traceback (most recent call last):
			...
			ValueError: -2 is not a state (i.e. between 0 and 0)
		"""
		cdef Dict* w
		sig_on()
		w = <Dict*>malloc(sizeof(Dict) * self.a.n)
		sig_off()
		if w is NULL:
			raise MemoryError("Failed to allocate memory for w in "
							  "shortest_words")
		if i is None:
			i = self.a.i
		else:
			if i < -1 or i >= self.a.n:
				raise ValueError("%s is not a state (i.e. between 0 and %s)" % (i, self.a.n-1))
		sig_on()
		res = shortestWords(self.a[0], w, i, verb)
		sig_off()
		if not res:
			return None
		rt = []
		for j in range(self.a.n):
			r = []
			for i in range(w[j].n):
				r.append(self.A[w[j].e[i]])
			rt.append(r)
			sig_on()
			FreeDict(&w[j])
			sig_off()
		sig_on()
		free(w)
		sig_off()
		return rt

	def rec_word(self, w):
		"""
		Determine if the word ``w`` is recognized or nor not by the automaton

		INPUT:

		- ``w`` -- a list of letters

		OUTPUT:

		return ``True`` if the word is recognized, otherwise ``False``

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("abaa")
			sage: a.rec_word("aba")
			False
			sage: a.rec_word("abaa")
			True

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.rec_word(['a', 'b', 'b'])
			False
			sage: a.rec_word("a")
			True

		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("ab")
			sage: a.rec_word(['a', 0])
			Traceback (most recent call last):
			...
			KeyError: 0

		"""
		cdef int i, e
		cdef dict d	 
		e = self.a.i
		if e == -1:
			return False
		d = {}
		for i, a in enumerate(self.A):
			d[a] = i
		for a in w:
			e = self.a.e[e].f[d[a]]
			if e == -1:
				return False
		return c_bool(self.a.e[e].final)

	def add_state(self, bint final, label=""):
		"""
		Add a state in the automaton

		INPUT:

		- ``final`` -- Boolean indicate if the added state is final
		
		- ``label`` (default: "") -- label of the new state

		OUTPUT:

		return the new state (which is an integer)

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("abaa")
			sage: s = a.add_state(true)
			sage: a.add_transition(2,'b',s)
			sage: a.rec_word("abb")
			True

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.add_state(True)
			4
			sage: a.add_state(False)
			5

			sage: a = DetAutomaton([(10,10,'x'),(10,20,'y'),(20,20,'z'),\
				(20,10,'y'),(20,30,'x'),(30,30,'y'),(30,10,'z'),(30,20,'x'),\
				(10,30,'z')], i=10)
			sage: a
			DetAutomaton with 3 states and an alphabet of 3 letters
			sage: a.add_state(True)
			3
			sage: a
			DetAutomaton with 4 states and an alphabet of 3 letters

		"""
		sig_on()
		AddState(self.a, final)
		sig_off()
		if self.S is not None:
			self.S.append(label)
		return self.a.n-1

	def add_transition(self, int i, l, int j):
		"""
		Add an edge in the automaton

		INPUT:

		- ``i`` - int - the first state

		- ``l`` -- the label of edge

		- ``j`` - int - the second state

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("abaa")
			sage: a.add_transition(2,'a',4)
			sage: a.rec_word("aba")
			True

			sage: a = dag.Word("abaa")
			sage: s = a.add_state(true)
			sage: a.add_transition(2,'b',s)
			sage: a.rec_word("abb")
			True

		TESTS::

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.add_transition(2,'a',1)
			sage: a == DetAutomaton([(0, 1, 'a'), (2, 3, 'b'), (2, 1, 'a')], i=0)
			True
			sage: a.add_transition(2,'v',1)
			Traceback (most recent call last):
			...
			ValueError: The letter v doesn't exist.
			sage: a.add_transition(2,'v',6)
			Traceback (most recent call last):
			...
			ValueError: The state 6 doesn't exist.
			sage: a.add_transition(5,'v',6)
			Traceback (most recent call last):
			...
			ValueError: The state 5 doesn't exist.

		"""
		if i < 0 or i >= self.a.n:
			raise ValueError("The state %s doesn't exist." % i)
		if j< 0 or j >= self.a.n:
			raise ValueError("The state %s doesn't exist." % j)
		try:
			k = self.A.index(l)
		except Exception:
			raise ValueError("The letter %s doesn't exist." % l)
		sig_on()
		self.a.e[i].f[k] = j
		sig_off()

	@property
	def n_states(self):
		"""
		return the numbers of states

		OUTPUT:

		int

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("abaa")
			sage: a.n_states
			5

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.n_states
			4

		"""
		return self.a.n

	@property
	def n_letters(self):
		"""
		return the numbers of letters of the alphabet.

		OUTPUT:

		int

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("abaaca")
			sage: a.n_letters
			3

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.n_letters
			2

		"""
		return self.a.na

	def bigger_alphabet(self, A):
		"""
		Gives a copy of the :class:`DetAutomaton`, but with the
		bigger alphabet ``A``.

		INPUT:

		- ``A`` --  alphabet of the new automaton. We assume that it
		  contains the current alphabet of the automaton.

		OUTPUT:

		return a :class:`DetAutomaton` with a bigger alphabet ``nA``

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.AnyLetter(['a', 'b'])
			sage: a.bigger_alphabet(['a','b','c'])
			DetAutomaton with 2 states and an alphabet of 3 letters

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')])
			sage: a.bigger_alphabet(['a','b','c'])
			DetAutomaton with 4 states and an alphabet of 3 letters

		TESTS::

			sage: from badic.cautomata_generators import *
			sage: a = dag.AnyWord(['a', 'b'])
			sage: a.bigger_alphabet(['a','1','c'])
			Traceback (most recent call last):
			...
			ValueError: Letter 'b' not found in the new alphabet

		"""
		cdef Dict d
		cdef int i
		cdef DetAutomaton r
		r = DetAutomaton(None)
		sig_on()
		d = NewDict(self.a.na)
		sig_off()
		for i in range(self.a.na):
			try:
				d.e[i] = A.index(self.A[i])
			except Exception:
				raise ValueError("Letter %r not found in the new alphabet"%self.A[i])
			sig_check()
		sig_on()
		r.a[0] = BiggerAlphabet(self.a[0], d, len(A))
		sig_off()
		r.A = A
		return r

	def complementary_op(self):
		"""
		Change the language of the automaton to the complementary ON PLACE.
		If the language of self was L and the alphabet is A,
		then the new language of self is A^*\L
		(i.e. words over the alphabet A that are not in L)

		OUTPUT:

		return None
		(the operation is on place)

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("abaa")
			sage: a.complementary_op()
			sage: a
			DetAutomaton with 6 states and an alphabet of 2 letters

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.complementary_op()
			sage: a
			DetAutomaton with 5 states and an alphabet of 2 letters
		"""
		cdef i
		self.complete_op()
		for i in range(self.a.n):
			self.a.e[i].final = not self.a.e[i].final

	def complementary(self):
		"""
		Gives an automaton whose language is the complementary.

		OUTPUT:

		:class:`DetAutomaton`
		return  a new automaton whose language is the complementary
		of the language of ``self``

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("abaa")
			sage: a.complementary()
			DetAutomaton with 6 states and an alphabet of 2 letters

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.complementary()
			DetAutomaton with 5 states and an alphabet of 2 letters
		"""
		a = self.copy()
		a.complementary_op()
		return a

	def included(self, DetAutomaton a, bint pruned=False, bint verb=False):
		r"""
		Test if the language of self is included in the language of ``a``

		INPUT:

		- ``a`` --  a :class:`DetAutomaton`

		- ``pruned`` -- (default: False) - set to True if the automaton self is
		  already pruned, in order to avoid unuseful computation.

		- ``verb`` -- (default: False) - verbose parameter

		OUTPUT:

		Boolean
		return ``True`` if the language of ``self`` is included in the
		language of ``a``, otherwise return ``False`` 

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.AnyWord("ab")
			sage: b = dag.Word("aba")
			sage: b.included(a)
			True
			sage: a.included(b)
			False

			sage: a = dag.AnyWord("abc")
			sage: b = dag.AnyWord("ab")
			sage: a.included(b)
			False
			sage: b.included(a)
			True

			sage: from badic.cautomata import *
			sage: a = DetAutomaton([(0, 1, 'a'), (2, 3, 'b')], i=0)
			sage: a.included(a)
			True
			sage: b = DetAutomaton([(0, 1, 'c')], i=0)
			sage: a.included(b)
			False
			sage: b = DetAutomaton([(0, 1, 'a')], i=0)
			sage: b.included(a)
			True
			sage: b = DetAutomaton([(0, 1, 'c')])
			sage: b.included(a)
			True
		"""
		cdef DetAutomaton b, a2
		cdef list A
		if self.A != a.A:
			A = list(set(a.A+self.A))
			b = self.bigger_alphabet(A)
			a2 = a.bigger_alphabet(A)
		else:
			b = self
			a2 = a
		sig_on()
		res = Included(b.a[0], a2.a[0], pruned, verb)
		answ = c_bool(res)
		sig_off()
		return answ

#		d = {}
#		for l in self.A:
#			if l in a.A:
#				d[(l,l)] = l
#		if verb:
#			print("d=%s"%d)
#		a.complete_op()
#		cdef DetAutomaton p = self.product(a, d, verb=verb)
#
#		#set final states
#		cdef int i,j
#		cdef n1 = self.a.n
#		for i in range(n1):
#			for j in range(a.a.n):
#				p.a.e[i+n1*j].final = self.a.e[i].final and not a.a.e[j].final
#
#		if step == 1:
#			return p;
#
#		return p.has_empty_language()

	def find_state_with_language (self, DetAutomaton a, get_scc=False, minimize=True, verb=False):
		r"""
		Return the index of state whose language is the language of a.
		Assume that self is minimized.

		INPUT:

			- ``a`` -- DetAutomaton

			- ``get_scc`` - bool (default: ``False``) if True, and if a is strongly connected,
									return also the component where the state was found.

			- ``minimize`` -- bool (default: ``True``) - if False, assume a to be minimized.

			- ``verb`` -- int (default: ``False``) - if >0, print informations.

		OUTPUT:
			A int.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0), (0,1,1), (1,0,0)], avoidDiGraph=True)

		"""
		if minimize:
			if verb > 1:
				print("minimize...")
			a = a.minimize()
			#self = self.minimize()
		i0 = self.a.i
		if a.is_strongly_connected():
			S = []
			cc = self.strongly_connected_components(only_terminal=True)
			for c in cc:
				if len(c) == a.n_states:
					S.append(c)
			if verb > 0:
				print("a is strongly connected, S=%s" % S)
		else:
			S = [range(self.a.n)]
		for c in S:
			for i in c:
				self.a.i = i
				if self.has_same_language_as(a):
					self.a.i = i0
					if get_scc:
						return i,c
					return i
		else:
			raise RuntimeError("State recognizing the language of a not found !")

	def random_word(self, int nmin=-1, int nmax=100):
		r"""
		Return a random word recognized by the automaton, by following a
		random path in the automaton from the initial state. If we don't fall
		into the set of final states before reaching the maximal length
		``nmax``, then return ``word not found!``.

		INPUT:

		- ``nmin`` -- (default: -1) - minimal length of the word

		- ``nmax`` -- (default: 100) - maximal length of the word

		OUTPUT:

		Return a random word of length between ``nmin`` and ``nmax`` if found.
		Otherwise return ``word not found!``.

		EXAMPLES::

			sage: from badic.cautomata_generators import *
			sage: a = dag.Word("abaa")
			sage: a.random_word()
			['a', 'b', 'a', 'a']

			sage: a = dag.AnyWord("ab")
			sage: a.random_word()
			[]

			sage: a = dag.Word("aba").union(dag.Word("bab"))
			sage: a.random_word()   # random
			['a', 'b', 'a']

			sage: a = dag.Empty(['a', 'b'])
			sage: a.random_word()
			'word not found!'

		"""
		cdef int i, j, l, na
		cdef list w, li
		i = self.a.i
		if i == -1:
			return "word not found!"
		w = []
		na = len(self.A)
		if nmin < 0:
			nmin = 0
		from sage.misc.prandom import random
		for j in range(nmin):
			li = [l for l in range(na) if self.succ(i, l) != -1]
			l = li[(int)(random() * len(li))]
			w.append(self.A[l])
			i = self.succ(i, l)
		# continue the word to get into a final state
		for j in range(nmax-nmin):
			if self.a.e[i].final:
				break
			li = [l for l in range(na) if self.succ(i, l) != -1]
			if li == []:
				return "word not found!"
			l = li[(int)(random() * len(li))]
			w.append(self.A[l])
			i = self.succ(i, l)
		if i < 0 or not self.a.e[i].final:
			return "word not found!"
		return w
	
	def matrices_algo (self, v, int i, h=None, which = False):
		"""
		Apply one step of the continued fraction algorithm defined by matrices from vector v and state i.
		Modify v ON PLACE, and return the new state.
		If h is specified, act by transposed on h (ON PLACE).

		INPUT:

			- ``v`` - vector

			- ``i`` - int -- initial state

			- ``h`` - vector (default: ``None``) -- if specifyed, act by transposed ON PLACE on h

			- ``which`` - boolean (default: ``False``) - if True, return which transition is followed (given by the index of its label in the alphabet) rather than new state

		OUTPUT:
			int (new state, or edge followed if which is True)
		
		EXAMPLES::
			sage: from badic import *
			sage: lm = [matrix([[1,1],[0,1]]), matrix([[1,0],[1,1]])]
			sage: for m in lm:
			....:	 m.set_immutable()
			sage: a = DetAutomaton([(0,m,0) for m in lm], avoidDiGraph=True)
			sage: v = vector((1,2))
			sage: a.matrices_algo(v, 0)
			0
			sage: v
			(1, 1)
			
		"""
		if i < 0 or i > self.a.n:
			raise ValueError("i=%s must be between 0 and %s" % (i,self.a.n))
		cdef int j,k
		lj = self.succs(i)
		if not hasattr(self, 'A_inv') or self.A_inv is None:
			self.A_inv = []
			for m in self.A:
				self.A_inv.append(m.inverse())
		for j in lj:
			v2 = self.A_inv[j]*v
			if is_positive(v2):
				for k in range(len(v)):
					v[k] = v2[k]
				if h is not None:
					h2 = h*self.A[j]
					for k in range(len(h)):
						h[k] = h2[k]
				if which:
					return j
				else:
					return self.a.e[i].f[j]
		return -1
	
	def win_lose_algo (self, v, int i, h=None, which = False):
		"""
		Apply one step of the win-lose continued fraction algorithm from vector v and state i.
		Modify v ON PLACE, and return the new state.
		If h is specified, act by transposed on h (ON PLACE).

		INPUT:

			- ``v`` - vector

			- ``i`` - int -- state of self

			- ``h`` - vector (default: ``None``) -- if specifyed, act by transposed ON PLACE on h

			- ``which`` - boolean (default: ``False``) - if True, return which transition is followed (given by the index of its label in the alphabet) rather than the new state

		OUTPUT:
			int (new state)

		EXAMPLES::
			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0)], i=0)
			sage: v = vector((1,2))
			sage: a.win_lose_algo(v, 0)
			0
			sage: v
			(1, 1)

			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,1,1),(2,0,0)], i=0) # Cassaigne continued fraction algorithm
			sage: i = 0 # initial state
			sage: v = vector((.1234567, .2345678, .3456789))
			sage: for k in range(5):
			....:	 i = a.win_lose_algo(v, i)
			....:	 print(v, i)
			(0.123456700000000, 0.234567800000000, 0.111111100000000) 1
			(0.0123456000000000, 0.234567800000000, 0.111111100000000) 2
			(0.0123456000000000, 0.222222200000000, 0.111111100000000) 0
			(0.0123456000000000, 0.111111100000000, 0.111111100000000) 2
			(0.0123456000000000, 0.0987655000000000, 0.111111100000000) 0

		"""
		cdef j,jm
		if i < 0 or i > self.a.n:
			raise ValueError("i must be a state of self !")
		jm = -1
		for j in range(self.a.na):
			if self.a.e[i].f[j] != -1:
				if jm == -1 or v[j] < v[jm]:
					jm = j
		for j in range(self.a.na):
			if j != jm and self.a.e[i].f[j] != -1:
				v[j] -= v[jm]
				if h is not None:
					h[jm] += h[j]
		if which:
			return jm
		return self.a.e[i].f[jm]

	def algo (self, v, int i, h = None, which = False):
		"""
		Apply one step of the continued fraction algorithm from vector v and state i.
		Modify v ON PLACE, and return the new state.
		If h is specified, act by transposed on h (ON PLACE).

		INPUT:

			- ``v`` - vector

			- ``i`` - int -- state of self

			- ``h`` - vector (default: ``None``) -- if specifyed, act by transposed ON PLACE on h

			- ``which`` - boolean (default: ``False``) - if True, return which transition is followed (given by the index of its label in the alphabet) rather than the new state

		OUTPUT:
			int (new state)

		EXAMPLES::
			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0)], i=0)
			sage: v = vector((1,2))
			sage: a.algo(v, 0)
			0
			sage: v
			(1, 1)
			
			sage: lm = [matrix([[1,1],[0,1]]), matrix([[1,0],[1,1]])]
			sage: for m in lm:
			....:	 m.set_immutable()
			sage: a = DetAutomaton([(0,m,0) for m in lm], avoidDiGraph=True)
			sage: v = vector((1,2))
			sage: a.algo(v, 0)
			0
			sage: v
			(1, 1)
			
			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,1,1),(2,0,0)], i=0) # Cassaigne continued fraction algorithm
			sage: i = 0 # initial state
			sage: v = vector((.1234567, .2345678, .3456789))
			sage: for k in range(5):
			....:	 i = a.algo(v, i)
			....:	 print(v, i)
			(0.123456700000000, 0.234567800000000, 0.111111100000000) 1
			(0.0123456000000000, 0.234567800000000, 0.111111100000000) 2
			(0.0123456000000000, 0.222222200000000, 0.111111100000000) 0
			(0.0123456000000000, 0.111111100000000, 0.111111100000000) 2
			(0.0123456000000000, 0.0987655000000000, 0.111111100000000) 0
			
		"""
		from sage.structure.element import is_Matrix
		if is_Matrix(self.A[0]):
			return self.matrices_algo(v,i,h,which)
		else:
			return self.win_lose_algo(v,i,h,which)
	
	def theta (self, niter=1000000):
		r"""
		Compute a estimation of theta1 and theta2 of the win-lose graph.

		INPUT:

		- ``niter`` int (default: 1000000) - number of iterations of the continued fraction algorithm

		OUTPUT:

		Return a couple of numbers theta1, theta2.

		EXAMPLE::

		sage: from badic import *
		sage: a = DetAutomaton([('a',2,'b'),('a',3,'c'),('b',1,'a'),('b',3,'c'),('c',1,'a'),('c',2,'b')], avoidDiGraph=True)
		sage: a.theta()
		(0.18..., -0.07...)
		"""
		cdef double t1, t2
		sig_on()
		ComputeTheta2 (self.a, self.a.i, &t1, &t2, niter)
		sig_off()
		return t1,t2

	def Lpriviledge_subgraph (self, L):
		r"""
		Compute the L-priviledge subgraph.

		INPUT:

		- ``L`` list - subset of the alphabet

		OUTPUT:

		DetAutomaton

		EXAMPLE::

		sage: from badic import *
		sage: a = DetAutomaton([('a',2,'b'),('a',3,'c'),('b',1,'a'),('b',3,'c'),('c',1,'a'),('c',2,'b')], avoidDiGraph=True)
		sage: a.Lpriviledge_subgraph([1,2])
		DetAutomaton with 3 states and an alphabet of 3 letters
		"""
		cdef DetAutomaton r
		r = DetAutomaton(None)
		cdef int *l
		cdef int i
		sig_on()
		l = <int *>malloc(sizeof(int) * len(L))
		sig_off()
		for i,t in enumerate(L):
			l[i] = self.A.index(t)
		sig_on()
		r.a[0] = Lpriv_subgraph(self.a, l, len(L))
		sig_off()
		free(l)
		r.A = self.A
		r.S = self.S
		return r

	def satisfies_Fougeron_criterion (self, verb=False):
		r"""
		Determine if the graph satisfies the Fougeron's criterion.

		INPUT:

		OUTPUT:
		boolean

		EXAMPLE::

		sage: from badic import *
		sage: a = DetAutomaton([('a',2,'b'),('a',3,'c'),('b',1,'a'),('b',3,'c'),('c',1,'a'),('c',2,'b')], avoidDiGraph=True)
		sage: a.satisfies_Fougeron_criterion()
		True

		sage: a = DetAutomaton([(0,0,0),(0,1,0)], avoidDiGraph=True)
		sage: a.satisfies_Fougeron_criterion()
		True
		
		sage: a = DetAutomaton([(0,0,0),(0,1,0),(0,2,0)], avoidDiGraph=True)
		sage: a.satisfies_Fougeron_criterion()
		False

		sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,2,0),(1,1,1)], avoidDiGraph=True)
		sage: a.satisfies_Fougeron_criterion()
		False

		sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0),(1,1,3),(3,0,3),(3,2,0)], avoidDiGraph=True)
		sage: a.satisfies_Fougeron_criterion()
		True
		
		sage: a = DetAutomaton([(1,3,2),(1,0,4),(2,1,1),(2,0,3),(3,2,2),(3,1,4),(4,2,1),(4,3,3),(0,0,0),(0,1,-1),(-1,3,1),(-1,2,-2),(-2,1,1),(-2,0,0)], avoidDiGraph=True)
		sage: a.satisfies_Fougeron_criterion()
		True
		"""
		cdef bint b
		sig_on()
		b = FougeronCriterion(self.a, verb)
		sig_off()
		return c_bool(b)

	def winlose_edge_matrix (self, i, j):
		"""
		Return the matrix corresponding to the outgoing edge j from state i.

		OUTPUT:
		A matrix.
		"""
		m = identity_matrix(self.a.na)
		for l in self.succs(i):
			if l != j:
				m[l,j] = 1
		m.set_immutable()
		return m

	def winlose_to_matrices(self, verb=False):
		"""
		Convert the winlose graph to a graph labeled by matrices.
		
		OUTPUT:
		
		A DetAutomaton labeled by matrices.
		
		EXAMPLE::
		
		sage: from badic import *
		sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,0),(2,1,1)], i=1)
		sage: a.winlose_to_matrices()
		DetAutomaton with 3 states and an alphabet of 6 letters
		"""
		cdef int i,j,k,l
		from copy import copy
		I = identity_matrix(self.a.na)
		if verb:
			print(I)
		le = []
		for i in range(self.a.n):
			ls = self.succs(i)
			if verb:
				print("{}: {}".format(i, ls))
			for k in ls:
				j = self.a.e[i].f[k]
				m = copy(I)
				for l in ls:
					if l != k:
						m[l,k] = 1
				m.set_immutable()
				le.append((i,m,j))
		return DetAutomaton(le, i=self.a.i, final_states=self.final_states, avoidDiGraph=True)
	
	def winlose_to_comparisons (self, init=None):
		"""
		Convert a win-lose graph to a graph labeled by comparaisons
		New labels are i,S, where i is the loosing letter and S is the set of winning letters
		"""
		cdef int i,j,k
		r = []
		for i in range(self.a.n):
			s = set()
			for k in range(self.a.na):
				j = self.a.e[i].f[k]
				if j != -1:
					s.add(k)
			for k in s:
				j = self.a.e[i].f[k]
				s2 = frozenset(s.difference([k]))
				r.append((i,(k,s2),j))
		if init is None:
			init = self.a.i
		return DetAutomaton(r, i=init, avoidDiGraph=True)
	
	def invariant_density_approx (self, v, n=10, i=None):
		"""
		Give an approximation of the invariant density for the win-lose graph at point v.
		
		INPUT:
		
		- ``v`` - vector -- the projective point where we want to approximate the density
		
		- ``n`` - int (default: ``10``) - the number of iterations to do. It is exponential with n, so don't take a too large value.
		
		- ``i`` - int (default: ``None``) - the initial state. If the DetAutomaton has no initial state, this must be provided.
		
		OUTPUT:
		
		A real number, approximating the density.
		
		EXAMPLE::
		
		sage: from badic import *
		sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,0),(2,1,1)], i=1)
		sage: a.invariant_density_approx((1,1,0), n=20)/a.invariant_density_approx((0,1,1), n=20)
		0.99999999...
		sage: a.invariant_density_approx((1,0,1), n=20)/a.invariant_density_approx((0,1,1), n=20)
		1.99...
		"""
		cdef CAutomaton ac
		cdef DetAutomaton acd
		cdef int l,j,k
		cdef int **A
		cdef int *lA
		cdef double *r
		# compute the mirror comparisons graph
		#print("mirror comp...")
		acd = self.winlose_to_comparisons(init=i)
		# set initial state
		i = acd.a.i
		if i == -1:
			raise ValueError("You must precise the initial state !")
		#print("i={}".format(i))
		ac = acd.mirror()
		# convert it to C data
		#print("allocate {}...".format(ac.a.na))
		sig_on()
		A = <int **>malloc(sizeof(int*) * ac.a.na)
		lA = <int*>malloc(sizeof(int) * ac.a.na)
		sig_off()
		for l in range(ac.a.na):
			lA[l] = 1+len(ac.A[l][1])
			#print(sizeof(int)*lA[l])
			sig_on()
			A[l] = <int*>malloc(sizeof(int)*lA[l])  # crash for an unexplenable reason !!!!!
			sig_off()
			A[l][0] = ac.A[l][0]
			for j,k in enumerate(ac.A[l][1]):
				A[l][j+1] = k
		#print("allocate {}".format(self.a.na))
		sig_on()
		r = <double*>malloc(sizeof(double)*self.a.na)
		sig_off()
		# convert v to C
		for l in range(self.a.na):
			r[l] = v[l]
		#return
		sig_on()
		s = InvariantDensityApprox(ac.a, i, A, lA, r, self.a.na, n)
		sig_off()
		#return
		sig_on()
		free(r)
		for i in range(self.a.na):
			free(A[i])
		free(A)
		free(lA)
		sig_off()
		return s

	def invariant_densities_equations (self, lvar=None, lvar_latex=None, A=None, hom=True, hom_func=False):
		"""
		Return the equations satisfied by an invariant density with respect to Lebesgue measure
		for the winlose graph or the graph labeled by matrices.

		INPUT:

			- ``lvar`` - list (default: ``None``) -- list of variables

			- ``lvar_latex`` - list (default: ``None``) -- list of variables in LaTeX

			- ``A`` - list (default: ``None``) - list of names of variables (if lvar and lvar_latex not defined)

			- ``hom`` - boolean (default: ``True``) -- with homogeneous coordinates or not

			- ``hom_func`` - boolean (default: ``False``) -- are the functions homogeneous

		EXAMPLE::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,0),(2,1,1)], i=1)
			sage: a.invariant_densities_equations()
			f0(x0, x1, x2) = f1(x0, x1, x0 + x2) + f2(x0, x0 + x1, x2)
			f1(x0, x1, x2) = f0(x0, x1, x1 + x2) + f2(x0 + x1, x1, x2)
			f2(x0, x1, x2) = f0(x0, x1 + x2, x2) + f1(x0 + x2, x1, x2)
			sage: show(a.invariant_densities_equations())	# not tested
		"""
		cdef i,j,k,l
		# test if we are labeled by matrices and convert if needed
		try:
			self.A[0].nrows()
		except:
			if A is None:
				A = self.A
			self = self.winlose_to_matrices()
		d = self.A[0].nrows()
		if A is None:
			A = range(d)
		if hom:
			F = lambda x:x
			G = lambda x:x
		else:
			F = lambda x:proj_to_delta(x)
			G = lambda x:vector([1-sum(x[1:])]+list(x[1:]))
		cdef CAutomaton a
		a = self.mirror()
		v = []
		vlat = []
		from sage.calculus.var import var
		for i in range(d):
			vlat.append(var("x_{}".format(A[i])))
			v.append(var("x{}".format(A[i])))
		if lvar is not None:
			v = lvar
		if lvar_latex is not None:
			vlat = lvar_latex
		from sage.modules.free_module_element import vector
		v = vector(v)
		vlat = vector(vlat)
		from sage.misc.latex import latex
		txt = ""
		lat = "\\begin{eqnarray*}\n"
		for i in range(a.a.n):
			lat += "	f_{{{}}}{} &=& ".format(i, F(G(vlat)))
			txt += "f{}{} = ".format(i, F(G(v)))
			for k in range(a.a.e[i].n):
				l = a.a.e[i].a[k].l
				j = a.a.e[i].a[k].e
				if k > 0:
					txt += " + "
					lat += " + "
				if abs(self.A[l].det()) != 1:
					lat += "{}".format(abs(self.A[l].det()))
				lat += "f_{{{}}}{}".format(j, F(self.A[l]*G(vlat)))
				if hom_func:
					lat += "{}".format(latex((sum(G(vlat))/sum(self.A[l]*G(vlat)))**d))
				if abs(self.A[l].det()) != 1:
					txt += "{}".format(abs(self.A[l].det()))
				txt += "f{}{}".format(j, F(self.A[l]*G(v)))
				if hom_func:
					txt += "*{}".format((sum(G(v))/sum(self.A[l]*G(v)))**d)
			txt += "\n"
			lat += "\\\\\n"
		lat += "\\end{eqnarray*}\n"
		return Eqns(txt, lat)

	def apply_transfer_operator (self, lf, lvar=None, hom=None, verb=False):
		"""
		Apply the transfer operator to the list of functions lf.
		The DetAutomaton is seen as a win-lose graph or graph labeled by matrices.
		lf[i] is the function for the state i, with variables xi/(x0+...+xd), i=1...d

		INPUT:

			- ``lf`` - list of expression - list of functions, for each state of self

			- ``lvar`` - list of variables used

			- ``hom`` - boolean (default: ``None``) -- homogeneous coordinates ?

			- ``verb`` - bool (default: ``False``)

		OUTPUT:

			A list of expressions.

		EXAMPLE::

			sage: from badic import *
			sage: a = DetAutomaton([[0,1,0],[0,2,0]], i=0, avoidDiGraph=True)
			sage: var('y')
			y
			sage: a.apply_transfer_operator([1], [y])
			[1/(y + 1)^2 + 1/(y - 2)^2]

			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,0),(2,1,1)], i=1)
			sage: var('y,z')
			(y, z)
			sage: lf = [1/((1-y)*(1-z)), 1/((y+z)*(1-z)), 1/((1-y)*(y+z))]
			sage: a.apply_transfer_operator(lf)					 # not tested

		TESTS::

			sage: a = DetAutomaton([(0,0,1),(0,1,1),(1,0,1),(1,1,1)], avoidDiGraph=True)
			sage: a.apply_transfer_operator([0, 1/(x*y)])
			[0, 1/((x + y)*x) + 1/((x + y)*y)]
		"""
		cdef int i,j,k,d
		if lvar is None:
			try:
				lvar = sum(lf).variables()
			except:
				lvar = sum(lf).parent().gens()
		from sage.modules.free_module_element import vector
		# test if we are labeled by matrices and convert if needed
		try:
			self.A[0].nrows()
		except:
			self = self.winlose_to_matrices()
		if hom is None:
			hom = (self.A[0].nrows() == len(lvar))
		if verb:
			print("hom : %s" % hom)
		v = vector(lvar) # list of variables
		if not hom:
			vproj = delta_to_proj(v) # projective variables
		if verb:
			if hom:
				print("v = {}".format(v))
			else:
				print("v = {}, vproj = {}".format(v, vproj))
		r = [lf[0].parent()(0) for _ in range(self.n_states)]
		d = self.A[0].nrows()
		for i in range(self.n_states):
			for k in range(self.n_letters):
				j = self.a.e[i].f[k]
				if j != -1:
					if hom:
						v2 = self.A[k]*v
						di = {x:y for x,y in zip(v,self.A[k]*v)}
					else:
						v2 = self.A[k]*vproj
						di = {x:y for x,y in zip(v,proj_to_delta(v2))}
					if verb:
						print("{} --{}--> {} : {}".format(i, k, j, di))
					if hom:
						r[j] += lf[i].subs(di)*abs(self.A[k].determinant())
					else:
						r[j] += lf[i].subs(di)*abs(self.A[k].determinant())/sum(v2)**d
		return r
	
	def is_invariant_density (self, lf, lvar=None, hom=None):
		"""
		Check if the list of functions is invariant by the transfer operator
		(i.e. if it is a density for each state which is invariant by the continued fraction algorithm).
		
		INPUT:
		
		- ``lf`` - list of expression - list of functions, for each state of self
		
		- ``lvar`` - (default: ``None``) -- list of variables used
		
		- ``hom`` - bool (default: ``None``) -- set to True if functions are given with homogeneous coordinates
				
		OUTPUT:
		
		A boolean.
		
		EXAMPLE::
		
		sage: from badic import *
		sage: a = DetAutomaton([[0,1,0],[0,2,0]], i=0, avoidDiGraph=True)
		sage: var('y')
		y
		sage: a.is_invariant_density([1], [y])
		False
		sage: a.is_invariant_density([1/(y*(1-y))], [y])
		True
		sage: a.is_invariant_density([1/(x*y)])
		True
		
		sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,0),(2,1,1)])
		sage: var('y,z')
		(y, z)
		sage: lf = [1/((1-y)*(1-z)), 1/((y+z)*(1-z)), 1/((1-y)*(y+z))]
		sage: a.is_invariant_density(lf)
		True
		"""
		lf2 = self.apply_transfer_operator(lf, lvar, hom=hom)
		for f,f2 in zip(lf, lf2):
			if not (f-f2).is_zero():
				return False
		return True
	
	def winlose2_to_full_comparison(self):
		cdef DetAutomaton r
		r = DetAutomaton(None)
		sig_on()
		r.a[0] = RelabelFougeron2(self.a)
		sig_off()
		r.A = [(x,y) for x in self.A for y in self.A]
		return r

	def full_comparison_to_winlose2(self):
		cdef DetAutomaton r
		r = DetAutomaton(None)
		sig_on()
		r.a[0] = Fougeron2FromRelabel(self.a)
		sig_off()
		from math import sqrt, floor
		r.A = list(range(floor(sqrt(self.a.na))))
		return r

	def minimize_winlose2 (self):
		r"""
		Minimize the win-lose graph.
		The graph is assumed to have leaving arity of 2.
		"""
		cdef DetAutomaton r
		cdef Automaton a,a2
		r = DetAutomaton(None)
		sig_on()
		a = RelabelFougeron2(self.a)
		a2 = Minimise(a, False) 
		FreeAutomaton(&a)
		r.a[0] = Fougeron2FromRelabel(&a2)
		FreeAutomaton(&a2)
		sig_off()
		r.A = self.A
		return r
	
	def minimize_winlose (self):
		r"""
		Minimize the win-lose graph.
		Return a win-lose graph describing the same continued fraction algorithm but with the minimal number of vertices.
		
		OUTPUT:
			A DetAutomaton.
			
		EXAMPLES::
			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,1,2),(2,1,2),(2,0,1),(1,0,0)], avoidDiGraph=True)
			sage: a.minimize_winlose()
			DetAutomaton with 1 state and an alphabet of 2 letters
			
		"""
		cdef DetAutomaton ac = self.winlose_to_comparisons().minimize()
		# check there is no problem and construct the new automaton
		Ac = ac.alphabet
		L = []
		for i in ac.states:
			lj = ac.succs(i)
			for j in lj:
				k = ac.succ(i,j)
				L.append((i,Ac[j][0],k))
			lj = [Ac[j] for j in lj]
			if len(set([S.union([k]) for k,S in lj])) != 1:
				raise RuntimeError("Error during the winlose minimization.")
		return DetAutomaton(L, i=ac.a.i, final_states=ac.final_states, avoidDiGraph=True)

	def minimize_algo(self):
		"""
		Minimize the continued fraction algorithm (matrices graph or win-lose graph).
		
		EXAMPLES::
			sage: from badic import *
			sage: a = dag.Cassaigne()
			sage: A = a.alphabet
			sage: a = DetAutomaton([(0,A[0],0),(0,A[1],1),(1,A[1],2),(2,A[0],2),(2,A[1],2)], i=0, avoidDiGraph=True)
			sage: am = a.mirror().determinize().minimize()
			sage: am.minimize_algo()
			DetAutomaton with 1 state and an alphabet of 2 letters
			
		"""
		cdef DetAutomaton a
		type = self.algo_type()
		if type == "general":
			raise ValueError("Only win-lose or matrices graphs can be minimized, sorry!")
		elif type == "matrices":
			a = self.copy()
			a.set_final_states(list(range(a.a.n)))
			return a.minimize()
		else:
			return self.minimize_winlose()

	# compute loops gens of pi1 of the (non-oriented) graph
	def pi1_gens (self, i=None, verb=False):
		r"""
		Return a list of loops generating the pi1 of the non-oriented graph, from a state i0.
		
		INPUT:
		
		- ``i0`` - int (default: ``None``) - initial state of loops.
		
		OUTPUT:
		
		List of loops, given as list of edges (i,j,k), where k is the label of an edge i --> j or j --> i.
		
		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(1,2,2),(1,3,3),(2,1,1),(2,3,3),(3,1,1),(3,2,2)], i=1)
			sage: a.pi1_gens()
			[[(0, 1, 2), (2, 1, 2), (2, 0, 1)],
			 [(0, 2, 3), (2, 0, 1)],
			 [(2, 0, 1), (2, 1, 2), (1, 0, 1)],
			 [(2, 0, 1), (2, 1, 2), (1, 2, 3), (2, 0, 1)]]

			sage: for l in a.pi1_gens():
			....:	print(l)
			....:	print(a.path_matrix(l))
			[(0, 1, 2), (2, 1, 2), (2, 0, 1)]
			[ 0 -1  0]
			[ 1  1  0]
			[ 1  1  1]
			[(0, 2, 3), (2, 0, 1)]
			[1 0 0]
			[1 1 1]
			[0 0 1]
			[(2, 0, 1), (2, 1, 2), (1, 0, 1)]
			[ 1  1  0]
			[-1  0  0]
			[ 1  0  1]
			[(2, 0, 1), (2, 1, 2), (1, 2, 3), (2, 0, 1)]
			[ 2  1  1]
			[-1  0 -1]
			[ 0  0  1]
		"""
		g = self.get_Graph()
		if i is None:
			if self.a.i == -1:
				i = 0
			else:
				i = self.a.i
		i0 = i
		if verb:
			print("initial state: {}".format(i))
		# compute spanning tree
		ct = list(g.depth_first_search(start=i, edges=True))
		r = []
		A = self.A
		for (i,j) in ct:
			for k in range(self.a.na):
				if self.succ(i,k) == j:
					r.append((i,j,A[k]))
					break
				elif self.succ(j,k) == i:
					r.append((j,i,A[k]))
					break
		# compute dictionnary of parents in the tree
		d = dict()
		for (i,j),e in zip(ct, r):
			d[j] = (i,e)
		# browse edges not in the spanning tree
		r = set(r)
		lpaths = []
		for i in range(self.a.n):
			for l in range(self.a.na):
				j = self.a.e[i].f[l]
				l = A[l]
				if j == -1:
					continue
				if (i, j, l) not in r:
					# compute the path from i0 to i
					path1 = []
					k = i
					while k != i0:
						k,e = d[k]
						path1.append(e)
					path1.reverse()
					if verb:
						print("path1 = {}".format(path1))
					# compute the path from j to i0
					path2 = []
					k = j
					while k != i0:
						k,e = d[k]
						path2.append(e)
					if verb:
						print("path2 = {}".format(path2))
					path = path1+[(i,j,l)]+path2
					lpaths.append(path)
		return lpaths
	
	# give the matrix of an edge i --l-->
	def edge_matrix(self, i, l, invert=False):
		r"""
		Give the matrix of the edge from state i labeled by letter of index l in the win-lose graph.
		
		INPUT:
		
		- ``i`` - int -- index of a state
		
		- ``l`` - int -- index of a label of an edge from i
		
		- ``invert`` - bool (default: ``False``) -- if True, return the inverse of the matrix
		
		OUTPUT:
		
		A square matrix of size the length of the alphabet.
		
		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(1,2,2),(1,3,3),(2,1,1),(2,3,3),(3,1,1),(3,2,2)], i=1)
			sage: a.edge_matrix(a.initial_state, a.alphabet.index(2))
			[1 0 0]
			[0 1 0]
			[0 1 1]
		"""
		ls = self.succs(i)
		if l not in ls:
			raise RuntimeError("Not an edge !")
		from sage.matrix.special import identity_matrix
		m = identity_matrix(self.a.na)
		if invert:
			for k in ls:
				if k != l:
					m[k,l] = -1
		else:
			for k in ls:
				m[k,l] = 1
		return m
	
	# give the matrix from a list of edges
	def path_matrix(self, le, i=None, verb=False):
		r"""
		Give the matrix of the path in the win-lose graph.
		
		INPUT:
		
		- ``le`` - list -- list of edges given in the form (i,j,k) where k is the label of the edge i --> j,
							or given by a list of labels (if so the initial state must be provided).
		
		- ``i`` - int (default: ``None``) -- initial state
		
		- ``verb`` - bool (default: ``False``) -- print the followed path if le is a list of labels
		
		OUTPUT:
		
		A square matrix of size the lenght of the alphabet.
		
		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(1,2,2),(1,3,3),(2,1,1),(2,3,3),(3,1,1),(3,2,2)], i=1)
			sage: a.path_matrix([2,3,1])
			[1 0 1]
			[1 1 0]
			[1 1 1]
			sage: a.relabel({1:'1',2:'2',3:'3'})
			sage: a.path_matrix("231")
			[1 0 1]
			[1 1 0]
			[1 1 1]
		"""
		cdef list A
		A = self.A
		#if not hasattr(self, 'dA'):
		dA = {}
		for k,a in enumerate(A):
			dA[a] = k
		if i is None:
			i = self.a.i
			if i == -1:
				raise ValueError("You must give the initial state !")
		rj = i
		if hasattr(le, '__iter__') and (not hasattr(le[0], '__iter__') or len(le[0]) == 1):
			le2 = []
			for e in le:
				j = self.a.e[i].f[dA[e]]
				if verb:
					print("{} --{}--> {}".format(i,e,j))
				if j == -1:
					raise ValueError("Not a path !")
				le2.append((i,j,e))
				i = j
			le = le2
		from sage.matrix.special import identity_matrix
		m = identity_matrix(self.a.na)
		for e in le:
			i,j,k = e
			k = dA[k]
			if not self.a.e[i].f[k] == j:
				raise ValueError("{} --{}--> {} is not an edge !".format(i,A[k],j))
			if i == rj:
				m1 = self.edge_matrix(i,k)
				rj = j
			elif j == rj:
				m1 = self.edge_matrix(i,k, invert=True)
				rj = i
			else:
				raise ValueError("Not a path...")
			m *= m1
		return m
	
	def invariant_subspace (self):
		r"""
		Compute an invariant subspace.
		
		OUTPUT:
		
		A vector space.
		
		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(1,2,2),(1,3,3),(2,1,1),(2,3,3),(3,1,1),(3,2,2)], i=1)
			sage: a.invariant_subspace()
			Vector space of degree 9 and dimension 0 over Symbolic Ring
			Basis matrix:
			[]
			
			sage: a = DetAutomaton([(0,'B',1),(0,'C',0),(1,'A',0),(1,'C',2),(2,'B',1),(2,'A',2)], i=1, avoidDiGraph=True)
			sage: a.invariant_subspace()
			Vector space of degree 9 and dimension 1 over Symbolic Ring
			Basis matrix:
			...
		"""
		i = self.a.i
		if i == -1:
			i = 0
		matrix_generators = [self.path_matrix(p, i=i) for p in self.pi1_gens(i=i)]
		R = []
		n =  matrix_generators[0].ncols()
		from sage.calculus.var import var
		from sage.matrix.constructor import matrix
		M = matrix(n, n, lambda i,j: var("a_" + str(i) + "_" + str(j)))
		variables = M.coefficients()
		for A in matrix_generators:
			E = A.transpose()*M*A-M
			for eq in E.coefficients():
				R.append([eq.coefficient(v) for v in variables])
		return matrix(R).right_kernel()

	def flip_flop (self, AS, F = None):
		r"""
		Return the flip-flop product of self for the given list of list of edges AS.
		It is a DetAutomaton with states SxAS, where S is the states of self,
		and with transitions (s,e) -l-> (t, e) for each transition s-l->t of self which is not in the union of AS,
		and transition (s,e) -l-> (t,f) for each transition s-l->t of self  which is on the set of edges f.
		
		INPUT:
			
			- AS - list -- list of list of edges
			
			- F - list (default: ``None``) -- list of final states
			
		OUTPUT:
		
		A DetAutomaton.
		
		EXAMPLES::
			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0),(1,1,1)])
			sage: a.flip_flop ([[(2,0,0),(2,1,1)],[(0,1,1),(1,0,0)]])
			DetAutomaton with 4 states and an alphabet of 2 letters
		"""
		cdef int n,i,k
		
		d = {}
		for i,l in enumerate(AS):
			for e in l:
				d[e] = i
		R = []
		for n in range(len(AS)):
			for (i,k,j) in self.edges():
				if (i,j,k) in d:
					R.append(((i,n), j, (k,d[(i,j,k)])))
				else:
					R.append(((i,n), j, (k,n)))
		if self.a.i == -1:
			return DetAutomaton(R, final_states=F, avoidDiGraph=True)
		else:
			return DetAutomaton(R, i=(self.a.i, 0), final_states=F, avoidDiGraph=True)
		
	def words(self, n=None, get_state=False):
		"""
		Return an iterator over all words of the language of self.
		If a number n is specified, iterate only over words of length n
		
		INPUT:
			
			- n -- integer (defaut: ``None``) - length of words

			- get_state -- bool (defaut: ``False``) - if True, return also the arrival state

		OUTPUT:
			
			A generator object.
		
		EXAMPLES::
			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0)], i=0)
			sage: g = a.words()
			sage: g						# random
			<_cython_3_0_6.generator object at 0x7f7293a1c040>
			sage: g = a.words()
			sage: next(g)
			[]
			sage: next(g)
			[0]
		"""
		cdef i,j,k
		l = [(self.a.i, [])]
		if self.a.e[self.a.i].final and (n is None or n == 0):
			yield []
		N = 1
		while True:
			l2 = []
			for i,w in l:
				for j in range(self.a.na):
					k = self.a.e[i].f[j]
					if k == -1:
						continue
					w2 = w+[j]
					if self.a.e[k].final and (n is None or n == N):
						if get_state:
							yield (w2,k)
						else:
							yield w2
					l2.append((k, w2))
			l = l2
			if len(l) == 0:
				break
			if n == N:
				break
			N += 1

	def simple_words(self):
		"""
		Return all words of the language of self not passing two times through the same state.
			
		OUTPUT:
			
			A list of list.
		
		EXAMPLES::
			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0)], i=0)
			sage: a.simple_words()
			[[], [0], [1], [1, 0]]
		"""
		l = [(self.a.i,[],[self.a.i])]
		r = []
		N = 1
		if self.a.e[self.a.i].final:
			r.append([])
		while True:
			l2 = []
			for i,w,ls in l:
				lj = self.succs(i)
				for j in lj:
					k = self.a.e[i].f[j]
					w2 = w+[j]
					if self.a.e[k].final:
						r.append(w2)
					if k not in ls:
						ls2 = ls+[k]
						l2.append((k, w2, ls2))
			l = l2
			if len(l) == 0:
				break
			if n == N:
				break
			N += 1
		return r

	def plot_domains (self, state=None, colors="hsv", alpha=None, float norm=.005, int N=200, size=None, int N2=30, exact=None, verb=False, m0=None, thickness=.05):
		"""
		Plot domains on the dual space, for the win-lose continued fraction algorithm on 3 letters.
		
		INPUT:

			- ``state`` - int (default: ``None``) -- if not None, plot only this state, and colors the decomposition into subtiles

			- ``N`` - int (default: ``200``) -- max number of iterations
			
			- ``norm`` - float (default: ``.05``) -- maximum norm of vector before plotting points
			
			- ``size`` - int (default: ``None``) -- size^(d-1) is the number of initial points, with d letters.
			
			- ``N2`` - int (default: ``30``) --number of points plotted for each initial point
			
			- ``colors`` - list or string (default: ``"hsv"``) -- color for each state of self

			- ``alpha`` - list (default: None) -- transparency

			- ``exact`` - boolean (default: ``None``) -- plot exact or approximative domains

			- ``m0`` - matrix (default: ``None``) - apply m0 before drawing

			- ``thickness`` - float (default: ``.05``) - thickness of 1 dimensional domains

		OUTPUT:
			A Graphics object.
		
		EXAMPLES::
			sage: from badic import *
			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,1,1),(2,0,0)], i=0)
			sage: a.plot_domains()
			Graphics object consisting of 10 graphics primitives
			sage: a.plot_domains(exact=False)						# long time
			Graphics object consisting of 7 graphics primitives
		"""
		cdef int i,x,y
		
		from sage.structure.element import is_Matrix
		if is_Matrix(self.A[0]):
			algo = self.matrices_algo
			d = self.A[0].ncols()
			if self.A[0].nrows() != self.A[0].ncols() or d not in [2,3]:
				raise NotImplementedError("This plot function is implemented only for win-lose graphs on 2 or 3 letters or with square matrices of size 2 or 3.")	
		else:
			algo = self.win_lose_algo
			d = self.a.na
			if d not in [2,3]:
				raise NotImplementedError("This plot function is implemented only for win-lose graphs on 2 or 3 letters or with square matrices of size 2 or 3.")
		if verb:
			print("algo = %s" % algo)
			print("d = %s" % d)
		
		from sage.plot.point import point2d
		from sage.plot.line import line2d
		from sage.matrix.special import identity_matrix
		from sage.modules.free_module_element import vector
		from sage.matrix.constructor import matrix
		from sage.plot.graphics import Graphics
		from sage.misc.prandom import random

		if m0 is None:
			m0 = identity_matrix(d)

		if state is not None:
			# find ingoing edges
			if self.algo_type() == "matrices":
				inedges = [(a,self.alphabet.index(j)) for (a,b,j) in self.edges() if b == state]
				inmatrices = [(a, self.alphabet[j].transpose()) for (a,j) in inedges]
			elif self.algo_type() == "win-lose":
				d_mat = dict()
				inedges = [(a,j) for (a,b,j) in self.edges() if b == state]
				inmatrices = [(a, self.winlose_edge_matrix(a,j).transpose()) for (a,j) in inedges]
			else:
				raise ValueError("Cannot handle general algo")
			if verb:
				print("inedges : {}".format(inedges))
				print("inmatrices : {}".format(inmatrices))
		
		# Manage colors
		if isinstance(colors, list):
			cols = colors
		elif isinstance(colors, str):
			from matplotlib import cm

			if colors not in cm.datad:
				raise RuntimeError("Color map %s not known (type sorted(colors) for valid names)" % colors)

			colors = cm.__dict__[colors]
			cols = []
			if state is None:
				for i in range(self.a.n):
					cols.append(colors(float(i)/float(self.a.n))[:3])
			else:
				for i in range(len(inmatrices)):
					cols.append(colors(float(i)/float(len(inmatrices)))[:3])
		else:
			raise TypeError("Type of option colors (=%s) must be list or str" % colors)
		if verb:
			print("colors : {}".format(cols))
		
		if alpha is None:
			alpha = [1 for _ in range(self.a.n)]
		
		if exact is None:
			try:
				ls = self.domains_polyhedra()
				if verb:
					print("ls = ")
					print(ls)
				for f in ls:
					if not f.is_finite():
						raise RuntimeError("Infinite !")
				exact = True
			except:
				exact = False
			if verb:
				print("exact? %s" % exact)
		elif exact:
			ls = self.domains_polyhedra()
		
		if exact:
			from badic.density import formulae_to_matrices
			ls = [[m for m,_ in formulae_to_matrices(f)] for f in ls]
			g = plot_tri(identity_matrix(d))
			if state is None:
				for l,color,alph in zip(ls, cols, alpha):
					for m in l:
						g += plot_tri(m0*m, fill=True, color=color, alpha=alph, text=False)
				for l in ls:
					for m in l:
						g += plot_tri(m0*m, text=False)
			else:
				for (a,M),color,alph in zip(inmatrices, cols,alpha):
					for m in ls[a]:
						g += plot_tri(m0*M*m, fill=True, color=color, alpha=alph, text=False)
			g.axes(False)
			return g
		else:
			if size is None:
				if d == 2:
					size = 500
				else:
					size = 50
			if state is None:
				lp = [[] for _ in range(self.a.n)]
			else:
				lp = [[] for _ in range(len(inmatrices))]
			for x in range(size**(d-1)):
				#v = pt_to_delta([random() for _ in range(d-1)])
				v = vector([random() for _ in range(d)])
				h = vector([1 for _ in range(d)])
				i = self.a.i
				if i == -1:
					i = 0
				# iterate the algo
				for _ in range(N):
					if sum(v) < norm:
						break
					i = algo(v, i, h)
					if i == -1:
						break
				if i == -1:
					break
				for _ in range(N2):
					ri = i
					j = algo(v, i, h, True)
					if j == -1:
						i = -1
					else:
						i = self.a.e[i].f[j]
					if i == -1:
						break
					if state is None:
						lp[i].append(proj_to_pt(m0*h))
					else:
						if i == state:
							#if verb:
							#	print("index of {}".format((ri,j)))
							lpi = inedges.index((ri,j))
							lp[lpi].append(proj_to_pt(m0*h))
			if d == 2:
				if verb:
					print("d = 2")
					for p in lp:
						print(p[:20])
				g = Graphics()
				for l, color,alph in zip(lp, cols, alpha):
					for p in l:
						g += line2d([(p[0],0),(p[0],thickness)], rgbcolor=color, alpha=alph)
					#g += point2d([(p[0],0) for p in l], size=1, color=color, alpha=alph)
				g.set_aspect_ratio(1)
			else:
				if verb > 0:
					print("d = 3")
				g = plot_tri(identity_matrix(3))
				for l, color, alph in zip(lp, cols, alpha):
					g += point2d(l, size=1, color=color, alpha=alph)
				g.set_aspect_ratio(1)
			g.axes(False)
			return g

	def matrices_to_general (self):
		"""
		Convert a matrices graph to a general algo with transitions i --m,m--> j.

		OUTPUT:
			A DetAutomaton.

		EXAMPLES::
			sage: from badic import *
			sage: a = dag.Cassaigne()
			sage: b = a.matrices_to_general()
			sage: b.alphabet
			[(
			[0 1 0]  [0 1 0]
			[1 0 0]  [1 0 0]
			[0 1 1], [0 1 1]
			),
			 (
			[1 1 0]  [1 1 0]
			[0 0 1]  [0 0 1]
			[0 1 0], [0 1 0]
			)]

		"""
		cdef DetAutomaton r
		r = self.copy()
		r.A = [(m,m) for m in self.A]
		return r

	def transition_matrices_list (self):
		r"""
		Give the list of matrices of the continued fraction algorithm described by self.
		The ith element of the result is a list of couples ((m,D),k) where m is the matrix of the transition from i to k, on cone D.
		
		OUTPUT:
			A list.
		
		EXAMPLES::
			sage: from badic import *
			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,0),(2,1,1)])
			sage: a.transition_matrices_list()
			[[((
			[1 0 0]  [1 0 0]
			[0 1 0]  [0 1 0]
			[0 1 1], [0 1 1]
			), 1),
			  ((
			[1 0 0]  [1 0 0]
			[0 1 1]  [0 1 1]
			[0 0 1], [0 0 1]
			), 2)],
			 [((
			[1 0 0]  [1 0 0]
			[0 1 0]  [0 1 0]
			[1 0 1], [1 0 1]
			), 0),
			  ((
			[1 0 1]  [1 0 1]
			[0 1 0]  [0 1 0]
			[0 0 1], [0 0 1]
			), 2)],
			 [((
			[1 0 0]  [1 0 0]
			[1 1 0]  [1 1 0]
			[0 0 1], [0 0 1]
			), 0),
			  ((
			[1 1 0]  [1 1 0]
			[0 1 0]  [0 1 0]
			[0 0 1], [0 0 1]
			), 1)]]

			sage: a = dag.Brun()
			sage: a.transition_matrices_list()
			[[((
			[1 1 0]  [1 1 1]
			[0 1 0]  [0 1 1]
			[0 0 1], [0 0 1]
			), 0),
			  ((
			[1 0 0]  [1 0 0]
			[0 1 0]  [1 1 0]
			[0 1 1], [1 1 1]
			), 0),
			  ((
			[1 0 0]  [1 0 0]
			[0 1 1]  [1 1 1]
			[0 0 1], [1 0 1]
			), 0),
			  ((
			[1 0 0]  [1 0 1]
			[1 1 0]  [1 1 1]
			[0 0 1], [0 0 1]
			), 0),
			  ((
			[1 0 0]  [1 1 0]
			[0 1 0]  [0 1 0]
			[1 0 1], [1 1 1]
			), 0),
			  ((
			[1 0 1]  [1 1 1]
			[0 1 0]  [0 1 0]
			[0 0 1], [0 1 1]
			), 0)]]
		"""
		cdef int i,j,k
		type = self.algo_type()
		if type == "win-lose":
			return self.winlose_to_matrices().transition_matrices_list()
		if type == "matrices":
			return self.matrices_to_general().transition_matrices_list()
		lm = []
		for i in range(self.a.n):
			lm.append([])
			for j in range(self.a.na):
				k = self.a.e[i].f[j]
				if k != -1:
					lm[i].append((self.A[j], k))
		return lm
	
	def plot_algo (self, i=None, depht=3, only_last=False, verb=False, fill=True, fill_color = "grey"):
		r"""
		Plot the partition corresponding to the continued fraction algorithm defined by self, from initial state i.

		INPUT:

			- ``i`` - int (default: ``None``) -- initial state used for the continued fraction algorithm

			- ``depht`` - int (default: ``3``) -- depht

			- ``only_last`` - bool (default: ``False``) -- if True, display only the last step

			- ``fill`` - bool (default: ``True``) - fill or not the last step with fill color fill_color

			- ``fill_color`` - color (default: ``"grey"``) - fill color used if fill is True

		OUTPUT:
			A Graphics object.

		EXAMPLES::
			sage: from badic import *
			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,0),(2,1,1)], i=0)
			sage: a.plot_algo()		# not tested

		"""
		from badic.density import polyhedra_intersection
		type = self.algo_type()
		d = self.algo_size()
		if d != 3:
			raise NotImplementedError("Sorry, this function is implemented only in projective dimension 2.")
		if i is None:
			i = self.a.i
		from sage.matrix.special import identity_matrix
		lm = self.transition_matrices_list()
		g = plot_tri(identity_matrix(3))
		if type == "general":
			# browse graph from i
			S = [(identity_matrix(3), identity_matrix(3), i)]
			for n in range(depht):
				S2 = []
				for (m,D,i) in S:
					for ((m2,D2),j) in lm[i]:
						#print("inter")
						#print(m*D2)
						#print()
						#print(D)
						#print()
						D3 = polyhedra_intersection(m*D2, D)
						#print(D3)
						if D3.ncols() >= d:
							m3 = m*m2
							if not only_last or n == depht-1:
								g += plot_tri(D3, alpha=1 - n/depht, text=False)
							if n == depht-1:
								g += plot_tri(D3, alpha=1 - n/depht, text=False, color=fill_color, fill=fill)
							S2.append((m3,D3,j))
				S = S2
				if verb:
					print("len(S) = %s" % len(S))
		else:
			# browse graph from i
			S = [(identity_matrix(3), i)]
			for n in range(depht):
				S2 = []
				for (m,i) in S:
					for ((m2,D2),j) in lm[i]:
						m3 = m*m2
						if not only_last or n == depht-1:
							g += plot_tri(m*D2, alpha=1 - n/depht, text=False)
						if n == depht-1:
							g += plot_tri(m*D2, alpha=1 - n/depht, text=False, color=fill_color, fill=fill)
						S2.append((m3,j))
				S = S2
		g.axes(False)
		g.set_aspect_ratio(1)
		return g
		
	def mirror_languages (self):
		r"""
		Return a list of DetAutomaton for each state of a.
		The ith DetAutomaton describe the set of words from any state to state i, following transitions in reverse order.

		OUTPUT:
			A list of DetAutomaton.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0),(1,1,1)])
			sage: a.mirror_languages()
			[DetAutomaton with 2 states and an alphabet of 2 letters,
			  DetAutomaton with 2 states and an alphabet of 2 letters]

		"""
		la = []
		for i in range(self.a.n):
			a2 = self.mirror()
			a2.set_final_states(range(a2.n_states))
			a2.set_initial_states([i])
			a2 = a2.determinize().minimize()
			la.append(a2)
		return la
	
	def domains_languages (self):
		r"""
		Return a list of DetAutomaton for each state of a.
		The ith DetAutomaton is a description of the domain corresponding to the state i in the dual space, given as a rationnal language over an alphabet of matrices.

		OUTPUT:
			A list of DetAutomaton.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0),(1,1,1)])
			sage: a.domains_languages()
			[DetAutomaton with 2 states and an alphabet of 2 letters,
			 DetAutomaton with 2 states and an alphabet of 2 letters]

		"""
		from sage.structure.element import is_Matrix
		if is_Matrix(self.A[0]):
			am = self
		else:
			am = self.winlose_to_matrices()
		la = am.mirror_languages()
		# transpose the matrices
		for a2 in la:
			lm = [m.transpose() for m in a2.alphabet]
			for m in lm:
				m.set_immutable()
			a2.set_alphabet(lm)
		return la

	def algo_size (self):
		"""
		Give the number of coordinates on which acts the continued fraction algorithm.

		OUTPUT:
			An integer.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,0),(2,1,1)])
			sage: a.algo_size()
			3

		"""
		type = self.algo_type()
		if type == "matrices":
			return self.A[0].ncols()
		if type == "win-lose":
			return self.a.na
		return self.A[0][0].nrows()

	def algo_type (self):
		r"""
		Test whether self is an algo defined as a matrices graph, a win-lose graph, or a general algorithm.

		OUTPUT:
			A string. Either "win-lose", "general" or "matrices".

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.ArnouxRauzy()
			sage: a.algo_type()
			'matrices'

			sage: a = dag.ArnouxRauzyPoincare()
			sage: a.algo_type()
			'general'

			sage: a = dag.FullySubstractive()
			sage: a.algo_type()
			'win-lose'

		"""
		from sage.structure.element import is_Matrix
		try:
			if is_Matrix(self.A[0][0]) and is_Matrix(self.A[0][1]):
				return "general"
		except:
			pass
		try:
			if is_Matrix(self.A[0]):
				return "matrices"
		except:
			pass
		return "win-lose"

	def are_invariant_polyhedra (self, lm, ring=None, verb=False):
		r"""
		Check if the list of polyhedra given by the list of matrices lm is invariant by the dual continued fraction algorithm.

		INPUT:

			- ``lm`` - list of matrices

			- ``ring`` - a ring (default: ``None``) - do the computation in ring
			
			- ``verb`` - boolean (default: ``False``) -- if True, print informations.
		
		OUTPUT:
			A boolean.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,0)], avoidDiGraph=True)
			sage: a.are_invariant_polyhedra([identity_matrix(2)])
			True
		"""
		if ring is None:
			from sage.rings.qqbar import AlgebraicField
			ring = AlgebraicField()
		#from sage.geometry.polyhedron.all import Polyhedron
		#lp = [Polyhedron(m.columns()) for m in lm]
		#d = len(lp)-1
		## test disjointness
		#for i in range(d+1):
		#	for j in range(i):
		#		if lp[i].intersection(lp[j]).dimension() >= d:
		#			if verb:
		#				print("The polyhedra are not disjoint.")
		#			return False # it is not a partition
		##
		from sage.structure.element import is_Matrix
		from badic.density import polyhedron_density
		if is_Matrix(self.A[0]):
			am = self
		else:
			am = self.winlose_to_matrices()
		if verb:
			print("Compute list of transitions for each state...")
		A = am.alphabet
		lsm = [[] for _ in range(self.a.n)] # list of matrices for each state
		for i in range(self.a.n):
			lj = am.succs(i)
			for j in lj:
				k = am.succ(i,j)
				lsm[k].append(A[j].transpose()*lm[i])
		for i,(m,l) in enumerate(zip(lm, lsm)):
			d = polyhedron_density(m, ring=ring)
			d2 = sum([polyhedron_density(m, ring=ring) for m in l])
			if not (d-d2).is_zero():
				if verb:
					print("State %s is not partitionned." % i)
				return False
		return True

	def find_invariant_polyhedra(self, N=30, alg=False, check=True, verb=False):
		"""
		Try to find invariant polyhedra for the dual continued fraction algorithm.
		Used by function domains_polyhedra().

		INPUT:

			- ``N`` - int (default: ``100``) - number of iteration of the stabilization step

			- ``alg`` - boolean (default: ``False``) - if False, try only rational points

			- ``check`` - boolean (default: ``True``) - check if polyhedra are indeed invariant

			- ``verb`` - boolean (default: ``False``) - if True, print informations

		OUTPUT:
			A list of matrices.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,0),(2,1,1)])
			sage: a.find_invariant_polyhedra()
			[
			[1 1 1]  [1 1 0]  [1 1 0]
			[1 0 1]  [1 1 1]  [0 1 1]
			[0 1 1], [0 1 1], [1 1 1]
			]

			sage: a = dag.Poincare()
			sage: a.find_invariant_polyhedra()
			[
			[1 0 0]
			[0 1 0]
			[0 0 1]
			]

		"""
		if len(self.strongly_connected_components()) != 1:
			raise ValueError("This function works only for strongly connected graphs.")
		# try the identity
		#if self.a.n == 1:
		#	m = identity_matrix(self.algo_size())
		#	if self.are_invariant_polyhedra([m]):
		#		return [m]
		#
		from sage.misc.misc_c import prod
		from sage.geometry.polyhedron.all import Polyhedron
		# find loops on each state
		from sage.structure.element import is_Matrix
		if is_Matrix(self.A[0]):
			am = self
		else:
			am = self.winlose_to_matrices()
		if alg:
			from sage.rings.qqbar import AlgebraicRealField
			K = AlgebraicRealField()
		else:
			from sage.rings.rational_field import QQ
			K = QQ
		#A = [matrix(m, ring=K) for m in am.alphabet]
		#A = [matrix(m, ring=ZZ) for m in am.alphabet]
		A = am.alphabet
		for m in A:
			m.set_immutable()
		if verb:
			print("find loops...")
		lv = []
		ri = am.initial_state
		rF = am.final_states
		for i0 in range(am.n_states):
			am.set_initial_state(i0)
			am.set_final_states([i0])
			lw = am.simple_words()
			l = set([])
			for w in lw:
				if len(w) == 0:
					continue
				m = prod([A[j] for j in w])
				#if m.is_primitive():
				#	continue
				lvs = loop_vectors(m, ring=K, left=True)
				lvs = [vector([K(t) for t in v]) for v in lvs]
				lvs = [v/sum(v) for v in lvs]
				for v in lvs:
					v.set_immutable()
				#print(w, v)
				l = l.union(lvs)
			lv.append(l)
		am.set_initial_state(ri)
		am.set_final_states(rF)
		lp = [Polyhedron(l) for l in lv]
		lv = [[vector(v) for v in p.vertices()] for p in lp]
		if verb:
			print(lv)
			print("stabilize...")
		# stabilize the set of vertices
		from copy import copy
		for kk in range(N):
			if verb:
				print(kk)
			lv2 = [copy(l) for l in lv]
			for i in range(am.n_states):
				lj = am.succs(i)
				for j in lj:
					k = am.succ(i,j)
					#print("%s -%s-> %s" % (i,j,k))
					for v in lv[i]:
						#print("compute %s*%s" % (v,A[j]))
						v2 = v*A[j]
						lv2[k].append(v2/sum(v2))
			lp2 = [Polyhedron(l) for l in lv2]
			for p,p2 in zip(lp,lp2):
				if p != p2:
					break
			else:
				break
			lp = lp2
			lv = [[vector(v) for v in p.vertices()] for p in lp]
			if verb:
				print(lp)
		lv = [sorted([vector(v) for v in p.vertices()], reverse=True) for p in lp]
		if verb:
			print("after sort:")
			print(lv)
			print("simplify...")
		from badic.density import simplify_polyhedron
		r = [simplify_polyhedron(matrix(l).transpose()) for l in lv]
		if check:
			if verb:
				print("check with K=%s ..." % K)
			if not self.are_invariant_polyhedra(r, ring=K, verb=verb):
				raise RuntimeError("Sorry, I was unable to find invariant polyhedra.")
		return r

	def domain_density_two_letters (self, lvar=None, ring=None):
		r"""
		Compute density of the set describe by self.
		Used by function invariant_densities_two_letters().

		INPUT:

			- ``lvar`` - list (default: ``None``) -- list of variables used

			- ``ring`` - a ring (default: ``None``) -- if a ring is given, convert the result to a FractionField of polynomials over this ring.

		OUTPUT:
			An expression or a rational fraction.

		EXAMPLES::
			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0),(1,1,1)])
			sage: la = a.mirror_languages()
			sage: la[0].domain_density_two_letters()
			1/((x0 + x1)*x0)

		"""
		lv = self.union_of_intervals()
		d = 0
		for i in range(len(lv)//2):
			d += simplex_density(matrix(lv[2*i:2*i+2]).transpose(), lvar)
		# convert the density to a FractionField
		if lvar is None:
			lvar = d.variables()
		if ring is not None:
			from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
			from sage.rings.fraction_field import FractionField
			PR = PolynomialRing(ring, list(lvar))
			K = FractionField(PR)
			return K(d)
		else:
			return d

	def min_lex (self, key=None):
		"""
		Return the minimal infinite word in lexicographical order in the language of a.
		Return a couple u,v such that the minimum is for the infinite word u(v)^oo.
		Return None if there is no word.

		INPUT:

			- ``key`` - function (default: ``None``) - associate to each index of letter its order

		OUTPUT:
			A couple of list of indices of letters.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0)], i=0)
			sage: a.min_lex()
			([], [0])

		"""
		cdef int i,j
		i = self.a.i
		if i == -1:
			return None
		seen = set()
		r = []
		l = []
		while i not in seen:
			seen.add(i)
			l.append(i)
			lj = self.succs(i)
			j = min(lj, key=key)
			r.append(j)
			i = self.a.e[i].f[j]
			if i == -1:
				return r,[]
		j = l.index(i)
		return r[:j], r[j:]

	def max_lex (self, key=None):
		"""
		Return the maximal infinite word in lexicographical order in the language of a.
		Return a couple u,v such that the maximum is for the infinite word u(v)^oo.
		Return None if there is no word.

		INPUT:

			- ``key`` - function (default: ``None``) - associate to each index of letter its order

		OUTPUT:
			A couple of list of indices of letters.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0)], i=0)
			sage: a.max_lex()
			([], [1, 0])

		"""
		cdef int i,j
		i = self.a.i
		if i == -1:
			return None
		seen = set()
		r = []
		l = []
		while i not in seen:
			seen.add(i)
			l.append(i)
			lj = self.succs(i)
			j = max(lj, key=key)
			r.append(j)
			i = self.a.e[i].f[j]
			if i == -1:
				return r,[]
		j = l.index(i)
		return r[:j], r[j:]

	def boundary_two_letters (self):
		"""
		Compute the boundary of the set described by a.
		Only for a DetAutomaton over an alphabet of two letters.

		OUTPUT:
			A DetAutomaton.

		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0, 0, 0), (0, 1, 1), (1, 0, 0), (1, 2, 1), (2, 1, 0), (2, 2, 1)])
			sage: la = a.domains_languages()
			sage: la[0].boundary_two_letters()
			DetAutomaton with 5 states and an alphabet of 2 letters

		"""
		if self.a.na != 2:
			raise ValueError("This function is only for DetAutomaton with an alphabet over two letters.")
		ac = self.complementary().prune()
		ac.set_final_states(ac.states)
		arel = DetAutomaton([(0,(0,0),0),(0,(1,1),0),(0,(1,0),1),(0,(0,1),2),(1,(0,1),1),(2,(1,0),2)], i=0, final_states=[0,1,2], avoidDiGraph=True)
		#arel.plot()
		ar = self.product(ac).intersection(arel).proji(0)
		# add extremities of the set
		for p,b in [self.min_lex(), self.max_lex()]:
			ab = DetAutomaton([(i,j,i+1) for i,j in enumerate(b[:-1])]+[(len(b)-1, b[-1], 0)], i=0, avoidDiGraph=True)
			if len(p) > 0:
				from badic.cautomata_generators import dag
				am = dag.Word(p).concat(ab)
			else:
				am = ab
			ar = ar.union(am)
		return ar.prune_inf()

	def union_of_intervals (self, verb=False):
		"""
		Find a finite union of intervals corresponding to the set described by self.
		It is given as a list of 2-dimensionnal vectors.
		The ith interval is between points of index 2*i and 2*i+1.
		Raise an error if the union is infinite.

		OUTPUT:
			A list of 2-dimensional vectors.

		EXAMPLES:
			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,1,1),(1,0,0)])
			sage: la = a.mirror_languages()
			sage: la[0].union_of_intervals()
			[(1, 1), (1, 0)]

			sage: a = DetAutomaton([(0, 0, 0), (0, 1, 1), (1, 0, 0), (1, 2, 1), (2, 1, 0), (2, 2, 1)])
			sage: la = a.mirror_languages()
			sage: la[0].union_of_intervals()
			[(6.8541019..., 4.236067...), (1, 0)]

		"""
		
		def E(i,j):
			"""
			Matrix of the transvection.
			"""
			m = identity_matrix(2)
			m[i,j] = 1
			return m
		
		from sage.misc.misc_c import prod
		ab = self.boundary_two_letters()
		cc = ab.strongly_connected_components()
		lv = set()
		F = []
		for c in cc:
			# determine if the strongly connected component is trivial and if it is terminal
			trivial = True
			terminal = True
			for i in c:
				for j in ab.succs(i):
					if ab.succ(i,j) in c:
						trivial = False
					else:
						terminal = False
			if trivial:
				continue
			if not terminal:
				if verb:
					print("Infinite union of intervals")
				break
			i0 = c[0] # choose a state in c
			# check if the strongly connected component is a cycle and compute the associated eigenvector
			i = i0
			cycle = True
			w = []
			for _ in range(len(c)):
				lj = ab.succs(i)
				if len(lj) != 1:
					print("Infinite union of intervals !")
					cycle = False
					break
				w.append(lj[0])
				i = ab.succ(i,lj[0])
				if i == i0:
					break
			if not cycle:
				break
			# matrix of the cycle
			m = prod([E(j,1-j) for j in w])
			v = max(m.right_eigenvectors())[1][0]
			# disconnect the state i0
			for j in ab.succs(i0):
				ab.set_succ(i0,j,-1)
			F.append((i0, v))
		#print(F)
		if not trivial and (not terminal or not cycle):
			return
		for i0, v in F:
			ab.set_final_states([i0])
			# now ab describe the finite set of pre-period for the cycle starting at state i0
			for w in ab.words():
				#print(w, v)
				v2 = prod([E(j,1-j) for j in w])*v
				v2.set_immutable()
				lv.add(v2)
		lv = list(lv)
		lv.sort(key=lambda x: x[0]/sum(x))
		if len(lv)%2 != 0:
			raise RuntimeError("Something gone wrong during computation of intervals. lv = %s" % lv)
		return lv

	def domains_polyhedra (self, check=True, verb=False):
		"""
		Compute a decomposition of the domain of each state as a finite union of polyhedra.
		Raise an error if not found.

		INPUT:

			- ``check`` - boolean (default: ``True``) - check if the result is indeed invariant

			- ``verb`` - boolean (default: ``False``) - if True, print informations.

		OUTPUT:
			A list of list of matrices.

		EXAMPLES::
			# Cassaigne continued fraction algorithm, as a win-lose graph
			sage: from badic import *
			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,0),(2,1,1)])
			sage: a.domains_polyhedra()
			[[1 1 1]
			 [0 1 1]
			 [1 0 1] positive cone on 3 coordinates,
			 [0 1 1]
			 [1 1 1]
			 [1 0 1] positive cone on 3 coordinates,
			 [0 1 1]
			 [1 0 1]
			 [1 1 1] positive cone on 3 coordinates]

			# Brun continued fraction algorithm
			sage: a = dag.Brun()
			sage: am = a.general_algo_to_matrices()
			sage: am.domains_polyhedra()
			[{0},
			 [1 1 2]
			 [1 1 1]
			 [0 1 1] positive cone on 3 coordinates,
			 [1 1 2]
			 [1 1 1]
			 [0 1 1] positive cone on 3 coordinates,
			 [0 1 1]
			 [1 1 2]
			 [1 1 1] positive cone on 3 coordinates,
			 [1 1 2]
			 [1 1 1]
			 [0 1 1] positive cone on 3 coordinates,
			 [0 1 1]
			 [1 1 2]
			 [1 1 1] positive cone on 3 coordinates,
			 [1 1 2]
			 [0 1 1]
			 [1 1 1] positive cone on 3 coordinates]

			# Example where the golden number appears
			sage: a = dag.winlose_from_set_of_quadratic_numbers([(sqrt(5)+1)/2])
			sage: a.domains_polyhedra()
			[[				 0 2.618033988749895?]
			 [				 1 4.236067977499789?] positive cone on 2 coordinates,
			 [6.854101966249684?				  1]
			 [4.236067977499789?				  0] positive cone on 2 coordinates,
			 [				 1 2.618033988749895?]
			 [1.618033988749895? 1.618033988749895?] positive cone on 2 coordinates]

			# Example with complicated domain
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,1,2),(2,1,1),(2,0,1),(1,0,0)], i=0, avoidDiGraph=True)
			sage: a.domains_polyhedra()
			[Union of m R_+^d for m recognized by a DetAutomaton with 7 states and an alphabet of 3 letters,
			 Union of m R_+^d for m recognized by a DetAutomaton with 6 states and an alphabet of 3 letters,
			 Union of m R_+^d for m recognized by a DetAutomaton with 6 states and an alphabet of 3 letters]

			# Example with domain with infinite union
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0),(1,0,1)], i=0)
			sage: dp = a.domains_polyhedra()
			sage: dp
			[Union of [1 0]
			 [2 1]^j [1 1]
			 [0 1] positive cone on 2 coordinates for j in N,
			 Union of [1 0]
			 [2 1]^j [1 1]
			 [1 2] positive cone on 2 coordinates for j in N]
			sage: latex(dp)
			\left[\biguplus_{j \in \mathbb{N}} \left(\begin{array}{rr}
			1 & 0 \\
			2 & 1
			\end{array}\right)^j \left(\begin{array}{rr}
			1 & 1 \\
			0 & 1
			\end{array}\right) \mathbb{R}_+^2, \biguplus_{j \in \mathbb{N}} \left(\begin{array}{rr}
			1 & 0 \\
			2 & 1
			\end{array}\right)^j \left(\begin{array}{rr}
			1 & 1 \\
			1 & 2
			\end{array}\right) \mathbb{R}_+^2\right]

			# More complicated example with domains with infinite union
			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,3),(2,1,1),(3,1,4),(3,2,2),(4,0,0),(4,2,5),(5,0,0),(5,1,1)], i=0, avoidDiGraph=True)
			sage: a.domains_polyhedra()
			[Union of [2 2 3]
			 [1 1 2]
			 [2 1 2]^j ( [1 1 2]
			 [1 1 1]
			 [1 0 1] positive cone on 3 coordinates union [2 4 5]
			 [1 2 3]
			 [2 3 4] positive cone on 3 coordinates union [3 5 7]
			 [2 3 4]
			 [2 4 5] positive cone on 3 coordinates ) for j in N,
			 Union of [2 2 1]
			 [3 2 2]
			 [2 1 1]^j ( [0 1 1]
			 [1 1 2]
			 [1 1 1] positive cone on 3 coordinates union [1 1 2]
			 [2 1 2]
			 [1 0 1] positive cone on 3 coordinates union [2 4 5]
			 [3 5 7]
			 [2 3 4] positive cone on 3 coordinates ) for j in N,
			 Union of [1 2 1]
			 [1 2 2]
			 [2 3 2]^j ( [1 1 1]
			 [0 1 1]
			 [1 1 2] positive cone on 3 coordinates union [0 1 1]
			 [1 1 2]
			 [1 2 2] positive cone on 3 coordinates union [1 1 2]
			 [2 1 2]
			 [2 1 3] positive cone on 3 coordinates ) for j in N,
			 Union of [2 2 3]
			 [1 1 2]
			 [2 1 2]^j ( [1 2 2]
			 [0 1 1]
			 [1 1 2] positive cone on 3 coordinates union [1 2 3]
			 [1 1 2]
			 [1 2 2] positive cone on 3 coordinates union [3 2 4]
			 [2 1 2]
			 [2 1 3] positive cone on 3 coordinates ) for j in N,
			 Union of [2 2 1]
			 [3 2 2]
			 [2 1 1]^j ( [1 2 2]
			 [1 2 3]
			 [1 1 2] positive cone on 3 coordinates union [1 2 3]
			 [2 3 4]
			 [1 2 2] positive cone on 3 coordinates union [3 2 4]
			 [4 2 5]
			 [2 1 3] positive cone on 3 coordinates ) for j in N,
			 Union of [1 2 1]
			 [1 2 2]
			 [2 3 2]^j ( [1 2 2]
			 [1 2 3]
			 [2 3 4] positive cone on 3 coordinates union [1 2 3]
			 [2 3 4]
			 [2 4 5] positive cone on 3 coordinates union [3 2 4]
			 [4 2 5]
			 [5 3 7] positive cone on 3 coordinates ) for j in N]			

			# Example of domains with double infinite union
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,1,1),(1,0,2),(2,1,3),(3,1,3),(3,0,3)],i=0,avoidDiGraph=True)
			sage: a = a.mirror().determinize()
			sage: a.domains_polyhedra()
			[[1 0]
			 [1 1] Union of [1 0]
			 [1 1]^j [2 1]
			 [1 1] positive cone on 2 coordinates for j in N,
			 [2 1]
			 [1 1] Union of [1 0]
			 [1 1]^j [2 1]
			 [1 1] positive cone on 2 coordinates for j in N,
			 [1 2]
			 [0 1] Union of [1 1]
			 [0 1]^i [1 0]
			 [1 1] Union of [1 0]
			 [1 1]^j [2 1]
			 [1 1] positive cone on 2 coordinates for j in N for i in N,
			 [1 0]
			 [1 1] Union of [1 0]
			 [1 1]^j [1 2]
			 [0 1] positive cone on 2 coordinates for j in N,
			 [2 1]
			 [1 1] Union of [1 0]
			 [1 1]^j [1 2]
			 [0 1] positive cone on 2 coordinates for j in N,
			 [1 2]
			 [0 1] Union of [1 1]
			 [0 1]^i [1 0]
			 [1 1] Union of [1 0]
			 [1 1]^j [1 2]
			 [0 1] positive cone on 2 coordinates for j in N for i in N]

			# Example with finite unions
			sage: a = dag.CassaigneWinLose()
			sage: AS = [[(2,0,0),(0,1,1)],[(2,1,1)]]
			sage: a2 = a.flip_flop(AS)
			sage: a2.domains_polyhedra()
			[[1 2 2]
			 [1 1 2]
			 [1 1 1] positive cone on 3 coordinates,
			 [0 1 1]
			 [1 1 2]
			 [1 2 2] positive cone on 3 coordinates union [1 2 2]
			 [1 1 2]
			 [2 2 3] positive cone on 3 coordinates,
			 [2 1 2]
			 [1 1 2]
			 [1 0 1] positive cone on 3 coordinates union [1 1 2]
			 [1 0 1]
			 [1 1 1] positive cone on 3 coordinates,
			 [0 1 1]
			 [1 1 2]
			 [1 1 1] positive cone on 3 coordinates,
			 [1 1 1]
			 [1 1 2]
			 [2 1 2] positive cone on 3 coordinates union [1 1 2]
			 [1 0 1]
			 [2 1 2] positive cone on 3 coordinates union [2 1 2]
			 [1 1 2]
			 [2 1 3] positive cone on 3 coordinates,
			 [1 1 1]
			 [1 1 2]
			 [1 0 1] positive cone on 3 coordinates]

		TESTS::
			sage: a = dag.Brun()
			sage: a.domains_polyhedra()
			Traceback (most recent call last):
			...
			ValueError: This function is only for matrices graphs or win-lose graphs.

		"""
		cdef DetAutomaton a2, am
		cdef int i
		from sage.misc.misc_c import prod
		from sage.structure.element import is_Matrix
		type = self.algo_type()
		if type == "matrices":
			am = self
		elif type == "win-lose":
			am = self.winlose_to_matrices()
		else:
			raise ValueError("This function is only for matrices graphs or win-lose graphs.")
		# decompose the graph into strongly connected components
		cc = am.strongly_connected_components()
		if len(cc) > 1:
			if verb:
				print(cc)
			r = [None for _ in range(am.a.n)] # result
			n = 0
			S = set()
			for c in cc:
				if verb:
					print("c = %s" % c)
				# determine if the strongly connected component is trivial and if it is terminal
				trivial = True
				terminal = True
				for i in c:
					for j in am.succs(i):
						if am.a.e[i].f[j] in c:
							trivial = False
						else:
							terminal = False
				if trivial:
					continue
				if not terminal:
					#raise RuntimeError("Sorry, this continued fraction algorithm is too complicated for me : it has a non-terminal strongly connected component.")
					continue
				for i in c:
					S.add(i)
				# compute polyhedra for this strongly connected component
				a2 = self.sub_automaton(c)
				ld = a2.domains_polyhedra(verb=verb)
				Sm = am.states
				S2 = a2.states
				for i in range(a2.n_states):
					r[Sm.index(S2[i])] = ld[i]
				n += a2.n_states
				# empty polyhedra for remaining sets
				for i in range(am.a.n):
					if r[i] is None:
						from badic.density import ZeroVect
						r[i] = ZeroVect(self.algo_size())
			return r

		# in the following, the graph is assumed to be strongly connected
		if verb:
			print("Find polyhedra for the strongly connected component...")
		if self.algo_type() == "win-lose" and self.n_letters == 2:
			try:
				# two letters algorithm
				la = self.mirror_languages()
				lm = []
				for a in la:
					lI = a.union_of_intervals()
					if verb:
						print(lI)
					l = []
					for i in range(int(len(lI)/2)):
						l.append(matrix(lI[2*i:2*i+2]).transpose())
					lm.append(l)
				from badic.density import Union
				from badic.density import PosCone
				return [Union([(m, PosCone(2)) for m in l]).simplify() for l in lm]
			except:
				if verb:
					print("Could not find finite union of intervals.")
				pass
		am = self.minimize_algo()
		try:
			ldm = am.find_invariant_polyhedra(check=check)
		except:
			raise ValueError("Sorry, I was not able to find convex polyhedra as domains of the minimized algorithm.")
		lam = am.domains_languages()
		la = self.domains_languages()
		from badic.density import decompose
		from badic.density import split_polyhedron
		ld = []
		if verb:
			print("ldm = %s" % ldm)
		from badic.density import Domain
		from badic.cautomata_generators import dag
		for a in la:
			if verb:
				print("decompose %s with %s" % (a, lam))
			lW = decompose(a, lam, minimize=False, verb=verb)
			#print("lW = %s" % lW)
			l = [w.concat(dag.AnyLetter(split_polyhedron(d))) for w,d in zip(lW,ldm)]
			#print("l = %s" % l)
			ld.append(Domain(dag.Union(l)))
		# try to decompose as sums
		from badic.density import decompose_domain
		dd = []
		for d in ld:
			try:
				dd.append(decompose_domain(d))
			except:
				dd.append(d)
		return [d.simplify() for d in dd]

	def is_total_mass_finite (self):
		"""
		Determine if the total mass of the invariant density is finite, for each state of the win-lose or matrices graph.
		
		EXAMPLES::

			sage: from badic import *
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,2,0),(2,2,0),(2,2,1)], i=0)
			sage: a = a.mirror().determinize()
			sage: a.is_total_mass_finite()
			[False, True, False, False]
		"""
		cdef int i
		cdef DetAutomaton a
		if self.algo_type() == "win-lose" and self.algo_size() == 2:
			la = self.mirror_languages()
			from badic.density import is_total_mass_finite_two_letters
			l = []
			for a in la:
				l.append(is_total_mass_finite_two_letters(a))
			return l
		else:
			dp = self.domains_polyhedra()
			from badic.density import is_total_mass_finite
			l = []
			for d in dp:
				l.append(is_total_mass_finite(d))
			return l

	def invariant_density (self, ring=None, lvar=None, check=True, verb=False):
		"""
		Try to find invariant densities of the continued fraction algorithm.

		INPUT:

			- ``ring`` - a ring (default: ``None``) - if a ring is specified, express the result as a rational fraction with coefficients in this ring.

			- ``lvar`` - a list of variables (default: ``None``) - list of variables used to express the result.

			- ``verb`` - boolean (default: ``False``) - if True, print informations.

		OUTPUT:
			A list of expressions or rational fractions.

		EXAMPLES::

			# Cassaigne continued fraction algorithm as a win-lose graph
			sage: from badic import * 
			sage: a = DetAutomaton([(0,1,1),(0,2,2),(1,0,0),(1,2,2),(2,0,0),(2,1,1)])
			sage: ld = a.invariant_density(); ld
			[1/((x0 + x1 + x2)*(x0 + x1)*(x0 + x2)),
			 1/((x0 + x1 + x2)*(x0 + x1)*(x1 + x2)),
			 1/((x0 + x1 + x2)*(x0 + x2)*(x1 + x2))]
			sage: a.is_invariant_density(ld)
			True

			# Cassaigne continued fraction algorithm as full-shift with matrices
			sage: a = dag.Cassaigne()
			sage: ld = a.invariant_density(); ld
			[1/((x0 + x1 + x2)*(x0 + x1)*(x1 + x2))]
			sage: a.is_invariant_density(ld)
			True

			# two letters continued fraction algorithm, with a density where the golden number appears
			sage: a = DetAutomaton([(0, 0, 0), (0, 1, 1), (1, 0, 0), (1, 2, 1), (2, 1, 0), (2, 2, 1)])
			sage: ld = a.invariant_density(); ld
			[4.236067977499789?/((6.854101966249684?*x0 + 4.236067977499789?*x1)*x0),
			 2.618033988749895?/((2.618033988749895?*x0 + 1.618033988749895?*x1)*(x0 + 1.618033988749895?*x1)),
			 2.618033988749895?/((2.618033988749895?*x0 + 4.236067977499789?*x1)*x1)]
			sage: a.is_invariant_density(ld)				# not tested (bug for an unexplained reason) 
			True

			# Brun algorithm
			sage: a = dag.Brun()
			sage: d = a.invariant_density(verb=True)
			general algorithm : the density is a piecewise function
			state 0 : sum of
			1/((x0 + x1 + x2)*(x0 + x2)*x0) on
			[1 1 0]
			[1 0 0]
			[0 0 1]
			1/((x0 + x1 + x2)*(x1 + x2)*x1) on
			[1 0 0]
			[1 1 0]
			[0 0 1]
			1/((x0 + x1 + x2)*(x0 + x1)*x1) on
			[1 0 0]
			[0 1 1]
			[0 1 0]
			1/((x0 + x1 + x2)*(x0 + x1)*x0) on
			[1 1 0]
			[0 0 1]
			[1 0 0]
			1/((x0 + x1 + x2)*(x0 + x2)*x2) on
			[1 0 0]
			[0 1 0]
			[0 1 1]
			1/((x0 + x1 + x2)*(x1 + x2)*x2) on
			[1 0 0]
			[0 1 0]
			[1 0 1]

			sage: am = a.general_algo_to_matrices()
			sage: ld = am.invariant_density(); ld
			[0,
			 1/((2*x0 + x1 + x2)*(x0 + x1 + x2)*(x0 + x1)),
			 1/((2*x0 + x1 + x2)*(x0 + x1 + x2)*(x0 + x1)),
			 1/((x0 + 2*x1 + x2)*(x0 + x1 + x2)*(x1 + x2)),
			 1/((2*x0 + x1 + x2)*(x0 + x1 + x2)*(x0 + x1)),
			 1/((x0 + 2*x1 + x2)*(x0 + x1 + x2)*(x1 + x2)),
			 1/((2*x0 + x1 + x2)*(x0 + x1 + x2)*(x0 + x2))]
			sage: am.is_invariant_density(ld)
			True

			# Selmer
			sage: a = dag.Selmer()
			sage: d = a.invariant_density(verb=True)
			general algorithm : the density is a piecewise function
			state 0 : sum of
			1/(x0*x1*x2) on
			[1 1 0]
			[1 0 1]
			[1 1 1]
			1/(x0*x1*x2) on
			[1 1 0]
			[1 1 1]
			[1 0 1]
			1/(x0*x1*x2) on
			[1 1 1]
			[1 1 0]
			[1 0 1]

			# Reverse
			sage: a = dag.Reverse()
			sage: a.invariant_density()
			[2/((x0 + x1)*(x0 + x2)*(x1 + x2))]

			# Poincaré
			sage: a = dag.Poincare()
			sage: a.invariant_density()
			[1/(x0*x1*x2)]

			# Example with complicated density
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,1,2),(2,1,1),(2,0,1),(1,0,0)], i=0, avoidDiGraph=True)
			sage: a.invariant_density()
			[Sum of |det(m)|/prod(m x) for m recognized by a DetAutomaton with 7 states and an alphabet of 3 letters,
			 Sum of |det(m)|/prod(m x) for m recognized by a DetAutomaton with 6 states and an alphabet of 3 letters,
			 Sum of |det(m)|/prod(m x) for m recognized by a DetAutomaton with 6 states and an alphabet of 3 letters]

			# Example with density with a sum
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,0,0),(1,0,1)], i=0)
			sage: dp = a.invariant_density()
			sage: dp
			[Sum of 1/(((2*j + 1)*x1 + x0)*(2*j*x1 + x0)) for j in N,
			 Sum of 1/(((2*j + 1)*x1 + x0)*(2*(j + 1)*x1 + x0)) for j in N]
			sage: latex(dp)
			\left[\sum_{j \in \mathbb{N}} \frac{1}{{\left({\left(2 \, j + 1\right)} x_{1} + x_{0}\right)} {\left(2 \, j x_{1} + x_{0}\right)}}, \sum_{j \in \mathbb{N}} \frac{1}{{\left({\left(2 \, j + 1\right)} x_{1} + x_{0}\right)} {\left(2 \, {\left(j + 1\right)} x_{1} + x_{0}\right)}}\right]

			# Example with density with double sums
			sage: a = DetAutomaton([(0,0,0),(0,1,1),(1,1,1),(1,0,2),(2,1,3),(3,1,3),(3,0,3)],i=0,avoidDiGraph=True)
			sage: a = a.mirror().determinize()
			sage: a.invariant_density()
			[Sum of 1/(((2*j + 3)*x1 + 2*x0)*((j + 2)*x1 + x0)) for j in N,
			 Sum of 1/(((2*j + 5)*x0 + (2*j + 3)*x1)*((j + 3)*x0 + (j + 2)*x1)) for j in N,
			 Sum of 1/(((i*(2*j + 3) + 4*j + 8)*x0 + (2*j + 3)*x1)*((i*(j + 2) + 2*j + 5)*x0 + (j + 2)*x1)) for j, i in N,
			 Sum of 1/(((2*j + 3)*x1 + 2*x0)*((j + 1)*x1 + x0)) for j in N,
			 Sum of 1/(((2*j + 5)*x0 + (2*j + 3)*x1)*((j + 2)*x0 + (j + 1)*x1)) for j in N,
			 Sum of 1/(((i*(2*j + 3) + 4*j + 8)*x0 + (2*j + 3)*x1)*((i*(j + 1) + 2*j + 3)*x0 + (j + 1)*x1)) for j, i in N]

			# Example with density with sum with powers of 2
			sage: a = dag.Reverse()
			sage: A = a.alphabet
			sage: a = DetAutomaton([(0,A[2],0),(0,A[0],1)]+[(1,m,1) for m in A], i=0, avoidDiGraph=True)
			sage: am = a.mirror().determinize().minimize()
			sage: am.invariant_density()
			[Sum of 54*2^j/(((4*2^j - (-1)^j)*x0 + 2*(2*2^j + (-1)^j)*x1 + (4*2^j - (-1)^j)*x2)*(2*(2*2^j + (-1)^j)*x0 + (4*2^j - (-1)^j)*x1 + (4*2^j - (-1)^j)*x2)*((2*2^j + (-1)^j)*x0 + (2*2^j + (-1)^j)*x1 + 2*(2^j - (-1)^j)*x2)) for j in N,
			 ( Sum of 54*2^j/(((4*2^j - (-1)^j)*x0 + (4*2^j - (-1)^j)*x1 + 2*(2*2^j + (-1)^j)*x2)*((4*2^j - (-1)^j)*x0 + 2*(2*2^j + (-1)^j)*x1 + (4*2^j - (-1)^j)*x2)*(2*(2^j - (-1)^j)*x0 + (2*2^j + (-1)^j)*x1 + (2*2^j + (-1)^j)*x2)) for j in N ) + ( Sum of 54*2^j/(((4*2^j - (-1)^j)*x0 + (4*2^j - (-1)^j)*x1 + 2*(2*2^j + (-1)^j)*x2)*(2*(2*2^j + (-1)^j)*x0 + (4*2^j - (-1)^j)*x1 + (4*2^j - (-1)^j)*x2)*((2*2^j + (-1)^j)*x0 + 2*(2^j - (-1)^j)*x1 + (2*2^j + (-1)^j)*x2)) for j in N )]

		"""
		from sage.structure.element import is_Matrix
		from badic.density import polyhedron_density, zero_density
		type = self.algo_type()
		if type == "general":
			# general algorithm
			if verb:
				print("general algorithm : the density is a piecewise function")
			am = self.general_algo_to_matrices()
			ld = am.domains_polyhedra(check=check)
			from badic.density import formulae_to_matrices
			ld = [[m for m,_ in formulae_to_matrices(f)] for f in ld]
			from sage.misc.misc_c import prod
			f = lambda x: prod([int(t > 0) for t in x])
			r = []
			for i in range(self.a.n):
				if verb:
					print("state %s : sum of" % i)
				l = []
				for k,(i2,E) in enumerate(am.states):
					if i == i2:
						for m in ld[k]:
							if m.is_zero():
								continue
							d = polyhedron_density(E.transpose().inverse()*m, ring=ring)/abs(E.det())
							if verb:
								print("%s on" % d)
								print(E)
							l.append((E,d))
				r.append(l) #lambda *x: sum([f(E.inverse()*vector(x))*y(*x) for E,y in l]))
			return r
		# try with the algorithm for matrices or win-lose graphs
		if verb:
			print("compute the density of win-lose or matrices graph")
		dp = self.domains_polyhedra(check=check)
		from badic.density import domain_to_density
		return [domain_to_density(d, ring=ring) for d in dp]

	def invariant_density_at_point (self, v, int i, ring=None, lvar=None, verb=False):
		r"""
		Try to find invariant density of the general continued fraction algorithm at point v and state i.

		INPUT:

			- ``ring`` - a ring (default: ``None``) - if a ring is specified, express the result as a rational fraction with coefficients in this ring.

			- ``lvar`` - a list of variables (default: ``None``) - list of variables used to express the result.

			- ``verb`` - boolean (default: ``False``) - if True, print informations.

		OUTPUT:
			A list of expressions or rational fractions.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Brun(3)
			sage: factor(a.invariant_density_at_point((.1, .2, .3), 0))
			1/((x0 + x2)*x1*x2)

			sage: a = dag.Brun(4)
			sage: factor(a.invariant_density_at_point((.1, .2, .3, .4), 0))		# long time
			(x0 + x1 + 2*x3)/((x0 + x1 + x3)*(x0 + x3)*(x1 + x3)*x2*x3)

			sage: a = dag.Selmer()
			sage: factor(a.invariant_density_at_point((.3, .4, .5), 0))
			1/(x0*x1*x2)
			sage: a.invariant_density_at_point((.1, .2, .5), 0)
			0

		"""
		from badic.density import is_in
		ld = self.invariant_density(ring=ring, lvar=lvar, verb=verb)
		s = 0
		for m,d in ld[i]:
			if is_in(v, m):
				if verb:
					print(d)
				s += d
		return s

	def general_algo_to_matrices (self, verb=False):
		r"""
		Convert a general continued fraction algorithm to an algorithm defined as a matrices graph.
		The general algorithm is defined by a graph with edges i --(m,D)--> j,
		where the matrix D corresponds the domain where we apply m^(-1).
		The states of the result are of the form (i,D), where i is a state of the general algorithm,
		and D is a positive matrix.
		Applying one step of the new algorithm from a vector v and state (i,D)
		gives a vector w = m^(-1)v such that (i,D) --m--> (j,D'), with v in m*R^d.
		It corresponds to applying one step of the general algorithm
		D'w = M^(-1)*D v such that i --(M,E)--> j, with Dv in E.

		INPUT:

			- ``verb`` - boolean (default: ``False``) - if True, print informations

		OUTPUT:
			A DetAutomaton

		EXAMPLES::

			# Arnoux-Rauzy-Poincaré algorithm
			sage: from badic import *
			sage: a = dag.ArnouxRauzyPoincare(); a
			DetAutomaton with 1 state and an alphabet of 9 letters
			sage: a.general_algo_to_matrices()
			DetAutomaton with 7 states and an alphabet of 32 letters

			# Brun algorithm
			sage: a = dag.Brun(); a
			DetAutomaton with 1 state and an alphabet of 6 letters
			sage: a.general_algo_to_matrices()
			DetAutomaton with 7 states and an alphabet of 18 letters

		"""
		from sage.geometry.polyhedron.all import Polyhedron
		from badic.density import split_polyhedron
		from badic.density import simplify_polyhedron

		def inter(E,F):
			p = Polyhedron([c/sum(c) for c in E.columns() if sum(c) > 0])
			q = Polyhedron([c/sum(c) for c in F.columns() if sum(c) > 0])
			return matrix(sorted(p.intersection(q).vertices(), reverse=True)).transpose()

		try:
			d = self.A[0][0].ncols()
		except:
			raise ValueError("The DetAutomaton has to be labeled by couples of matrices")
		if verb:
			print("d=%s" % d)
		Id = identity_matrix(d)
		Id.set_immutable()
		to_see = [(i,Id) for i in range(self.a.n)]
		if self.a.i != -1:
			i0 = (self.a.i, Id) # initial state of the new algorithm
		else:
			i0 = -1
		seen = set(to_see)
		lt = []
		while len(to_see) != 0:
			(i,E) = to_see.pop()
			if verb:
				print((i,E))
				print("*****")
			lj = self.succs(i)
			for j in lj:
				k = self.a.e[i].f[j]
				m,F = self.A[j]
				if verb:
					print("%s ---> %s" % (i,k))
					print(m)
					print("E")
					print(E)
					print("F")
					print(F)
				G = inter(E,F)
				if verb:
					print("G")
					print(G)
					print("m")
					print(m)
				if G.ncols() < d:
					continue
				G = m.inverse()*G
				G = matrix([c/sum(c) for c in G.columns()]).transpose()
				lG = split_polyhedron(G)	
				if verb:
					print("lG")
					print(lG)
				for G in lG:
					G = matrix(sorted(G.columns(), reverse=True)).transpose()
					G = simplify_polyhedron(G)
					G.set_immutable()
					if verb:
						print("m^(-1)(E n F)")
						print(G)
					M = E.inverse()*m*G
					M.set_immutable()
					lt.append(((i,E),M,(k,G)))
					if (k,G) not in seen:
						seen.add((k,G))
						if verb:
							print((k,G))
						to_see.append((k,G))
		if verb:
			print(lt)
		return DetAutomaton(lt, i=i0, avoidDiGraph=True)

	def dual_algo (self, verb=False):
		r"""
		Compute the dual continued fraction algorithm of self, given as a win-lose graph or matrices graph.
		Return a continued fraction algorithm given as a graph i --m,D--> j, where we apply m^(-1) on domain D.

		INPUT:

			- ``verb`` - boolean (default: ``False``) - if True, print informations

		OUTPUT:
			A DetAutomaton

		EXAMPLES::

			# Cassaigne algorithm
			sage: from badic import *
			sage: a = dag.Cassaigne(); a
			DetAutomaton with 1 state and an alphabet of 2 letters
			sage: a.dual_algo()
			DetAutomaton with 1 state and an alphabet of 2 letters
		"""
		from sage.structure.element import is_Matrix
		try:
			if not is_Matrix(self.A[0]):
				if verb:
					print("win-lose graph : convert it to matrices graph")
				self = self.winlose_to_matrices()
		except:
			pass
		if verb:
			print("Compute invariant polyhedra...")
		try:
			ld = self.domains_polyhedra(verb=verb)
			from badic.density import formulae_to_matrices
			ld = [[m for m,_ in formulae_to_matrices(f)] for f in ld]
		except:
			raise RuntimeError("Sorry, I was unable to find invariant polyhedra. The dual algo may be defined on a more complicated set than polyhedra.")
		r = []
		for i in range(self.a.n):
			if len(ld[i]) == 0:
				continue
			lj = self.succs(i)
			for j in lj:
				k = self.a.e[i].f[j]
				if len(ld[k]) == 0:
					continue
				m = self.A[j]
				for D in ld[i]:
					mt = m.transpose()
					mt.set_immutable()
					D2 = mt*D
					D2.set_immutable()
					r.append((k,(mt,D2),i))
		return DetAutomaton(r, i=self.a.i, avoidDiGraph=True)

	def is_plain_algo (self, verb=False):
		r"""
		If self is a matrices graph, check if the continued fraction algorithm is defined on the full positive cone. 
		If self is a win-lose graph, return True.
		If self is a general algorithm with transitions i --m,D--> j, raise an exception.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Brun()
			sage: am = a.general_algo_to_matrices()
			sage: am.is_plain_algo()
			True

			sage: a = dag.Reverse()
			sage: a.is_plain_algo()
			True

			sage: a = dag.ArnouxRauzy()
			sage: a.is_plain_algo()
			False

		"""
		cdef int i,j
		from sage.rings.rational_field import QQ
		from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
		from sage.rings.fraction_field import FractionField
		type = self.algo_type()
		if type == "win-lose":
			return True
		if type == "matrices" or type == "general":
			from sage.misc.misc_c import prod
			for i in range(self.a.n):
				s = 0
				for j in range(self.a.na):
					k = self.a.e[i].f[j]
					if k != -1:
						if type == "general":
							sd = simplex_density(self.A[j][1])
							K = FractionField(PolynomialRing(QQ, sd.variables()))
							s += K(sd)
						else:
							sd = simplex_density(self.A[j])
							K = FractionField(PolynomialRing(QQ, sd.variables()))
							s += K(sd)
				if s - 1/prod(s.parent().gens()) != 0:
					if verb:
						print("state %s is not partitioned : density %s" % (i,s))
					return False
			return True

	def matrices_to_winlose_with_permutations (self, verb=False):
		r"""
		Convert the matrices graph to a winlose graph with permutations.

		OUTPUT:
			A DetAutomaton.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Cassaigne()
			sage: a.matrices_to_winlose_with_permutations()
			DetAutomaton with 3 states and an alphabet of 4 letters

		"""
		r = []
		for i in range(self.n_states):
			if verb:
				print("state %s" % i)
			lj = self.succs(i)
			lm = [self.alphabet[j] for j in lj]
			if verb:
				print(lm)
			l = decompose_matrices(lm)
			if len(l) == 0 and len(lm) > 0:
				raise RuntimeError("Sorry, I was not able to decompose matrices for state %s" % i)
			if verb:
				print(l)
			lf = [self.succ(i,j) for j in lj]
			C = [0]
			r += edges_from_list(i, l, lf, C)
		if verb:
			print(r)
		return DetAutomaton(r, avoidDiGraph=True)

	def depermut_winlose (self, verb=False):
		r"""
		Convert the winlose graph with permutations to a winlose graph.

		OUTPUT:
			A DetAutomaton.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Cassaigne()
			sage: b = a.matrices_to_winlose_with_permutations()
			sage: b.depermut_winlose()
			DetAutomaton with 6 states and an alphabet of 3 letters
		"""
		from sage.groups.perm_gps.permgroup import PermutationGroup
		d = dict()
		lp = []
		for i in range(self.a.n):
			lj = self.succs(i)
			if len(lj) == 1:
				k = self.a.e[i].f[lj[0]]
				lp.append(self.A[lj[0]])
				d[i] = (k, self.A[lj[0]])
		if verb:
			print(lp)
			print(d)
		G = PermutationGroup(lp)
		ident = G()
		G = list(G)
		r = []
		for a,g in enumerate(G):
			if verb:
				print("a=%s, g=%s" % (a,g))
			for i in range(self.a.n):
				if i in d:
					continue
				for j in range(self.a.na):
					k = self.a.e[i].f[j]
					if k != -1:
						if verb:
							print("%s --%s--> %s" % (i,self.A[j],k))
						if k in d:
							j2 = g.inverse()(self.A[j])
							k2 = G.index(g*d[k][1])
							r.append(((i,a), j2, (d[k][0],k2)))
						else:
							j2 = g.inverse()(self.A[j])
							r.append(((i,a), j2, (k,a)))
		if self.a.i == -1:
			return DetAutomaton(r, avoidDiGraph=True)
		else:
			return DetAutomaton(r, i=(self.a.i, G.index(ident)), avoidDiGraph=True)

	def matrices_to_winlose (self):
		r"""
		Convert the matrices graph to a winlose graph.

		OUTPUT:
			A DetAutomaton.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.Cassaigne()
			sage: a.matrices_to_winlose()
			DetAutomaton with 6 states and an alphabet of 3 letters

			sage: a = dag.Brun()
			sage: am = a.general_algo_to_matrices()
			sage: amm = am.sub_automaton(max(am.strongly_connected_components(), key=lambda x:len(x)))
			sage: amm.matrices_to_winlose().minimize_winlose()
			DetAutomaton with 9 states and an alphabet of 3 letters

			sage: a = dag.JacobiPerron()
			sage: am = a.general_algo_to_matrices().minimize()
			sage: amm = am.sub_automaton(max(am.strongly_connected_components(), key=lambda x:len(x)))
			sage: amm.matrices_to_winlose().minimize_winlose()
			DetAutomaton with 18 states and an alphabet of 3 letters
		"""
		return self.matrices_to_winlose_with_permutations().depermut_winlose()

	def matrices_graph (self, verb=False):
		"""
		Convert the continued fraction algorithm to a matrices graph.

		INPUT:

			- ``verb`` - boolean (default: ``False``) - if True, print informations

		OUTPUT:
			A DetAutomaton

		EXAMPLES::

			# Arnoux-Rauzy-Poincaré algorithm
			sage: from badic import *
			sage: a = dag.ArnouxRauzyPoincare(); a
			DetAutomaton with 1 state and an alphabet of 9 letters
			sage: a.matrices_graph()
			DetAutomaton with 7 states and an alphabet of 32 letters

			# Brun algorithm
			sage: a = dag.Brun(); a
			DetAutomaton with 1 state and an alphabet of 6 letters
			sage: a.matrices_graph()
			DetAutomaton with 7 states and an alphabet of 18 letters

			# Cassaigne
			sage: a = dag.Cassaigne(); a
			DetAutomaton with 1 state and an alphabet of 2 letters
			sage: a.matrices_graph()
			DetAutomaton with 1 state and an alphabet of 2 letters

		"""
		type = self.algo_type()
		if type == "general":
			if verb > 0:
				print("Convert the general algo to a matrices graph.")
			return self.general_algo_to_matrices()
		elif type == "win-lose":
			if verb > 0:
				print("Convert the win-lose graph to a matrices graph.")
			return self.winlose_to_matrices()
		else:
			if verb > 0:
				print("Already a matrices graph.")
			return self

	def winlose (self, verb=False):
		"""
		Convert the continued fraction algorithm to a winlose graph.

		INPUT:

			- ``verb`` - boolean (default: ``False``) - if True, print informations

		OUTPUT:
			A DetAutomaton

		EXAMPLES::

			# Brun algorithm
			sage: from badic import *
			sage: a = dag.Brun(); a
			DetAutomaton with 1 state and an alphabet of 6 letters
			sage: a.winlose()
			DetAutomaton with 102 states and an alphabet of 3 letters

			# Cassaigne
			sage: a = dag.Cassaigne(); a
			DetAutomaton with 1 state and an alphabet of 2 letters
			sage: a.winlose().minimize_winlose()
			DetAutomaton with 3 states and an alphabet of 3 letters

		TESTS::

			# Arnoux-Rauzy-Poincaré algorithm
			sage: from badic import *
			sage: a = dag.ArnouxRauzyPoincare(); a
			DetAutomaton with 1 state and an alphabet of 9 letters
			sage: a = a.matrices_graph(); a
			DetAutomaton with 7 states and an alphabet of 32 letters
			sage: a.winlose()
			Traceback (most recent call last):
			...
			RuntimeError: Sorry, I was not able to decompose matrices for state 0

		"""
		type = self.algo_type()
		if type == "general":
			if verb > 0:
				print("Convert the general algo to a winlose graph")
			return self.general_algo_to_matrices().matrices_to_winlose()
		elif type == "win-lose":
			if verb > 0:
				print("Already a winlose graph")
			return self
		else:
			if verb > 0:
				print("Convert the matrices graph to a win-lose graph")
			return self.matrices_to_winlose()

	def general (self, verb=False):
		"""
		Convert the continued fraction algorithm to a general algo,
		i.e. a graph labeled by matrices m,D, where
		m is the inverse of the operation applied on domain D.

		INPUT:

			- ``verb`` - boolean (default: ``False``) - if True, print informations

		OUTPUT:
			A DetAutomaton

		EXAMPLES::

			# Arnoux-Rauzy-Poincaré algorithm
			sage: from badic import *
			sage: a = dag.ArnouxRauzyPoincare(); a
			DetAutomaton with 1 state and an alphabet of 9 letters
			sage: a.general()
			DetAutomaton with 1 state and an alphabet of 9 letters

			# Brun algorithm
			sage: a = dag.Brun(); a
			DetAutomaton with 1 state and an alphabet of 6 letters
			sage: a.general()
			DetAutomaton with 1 state and an alphabet of 6 letters

			# Cassaigne
			sage: a = dag.Cassaigne(); a
			DetAutomaton with 1 state and an alphabet of 2 letters
			sage: a.general()
			DetAutomaton with 1 state and an alphabet of 2 letters

		"""
		type = self.algo_type()
		if type == "general":
			if verb > 0:
				print("Already a general algo.")
			return self
		elif type == "win-lose":
			if verb > 0:
				print("Convert the winlose graph to a general algo.")
			return self.winlose_to_matrices().matrices_to_general()
		else:
			if verb > 0:
				print("Convert the matrices graph to a general algo.")
			return self.matrices_to_general()

	def apply_matrices (self, lm):
		r"""
		Apply a matrix to each state of the matrices graph,
		i.e. changes the matrices of edges in order
		to have an equivalent continued fraction algorithm.
		Replace edges i --m--> j by i --M--> j, where
		M = lm[i]*m*lm[j]^(-1).

		INPUT:

			- ``lm`` - list of matrices

		OUTPUT:
			A DetAutomaton.

		EXAMPLES::

			sage: from badic import *
			sage: a = dag.CassaigneWinLose().winlose_to_matrices()
			sage: p = matrix([[0,1,0],[0,0,1],[1,0,0]])
			sage: a.apply_matrices([p^0, p^1, p^2]).minimize()
			DetAutomaton with 1 state and an alphabet of 2 letters

		"""
		r = []
		for i,j,m in self.edges():
			m2 = lm[i]*m*lm[j].inverse()
			m2.set_immutable()
			r.append((i, m2, j))
		return DetAutomaton(r, i=self.a.i, final_states=self.final_states, avoidDiGraph=True)
