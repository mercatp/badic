
struct Complexe
{
	double x,y;
};
typedef struct Complexe Complexe;

Complexe prod (Complexe a, Complexe b);
Complexe mul_i (Complexe a, int i);
Complexe zero (void);
Complexe un (void);
Complexe powC (Complexe a, int n);
Complexe sub (Complexe a, Complexe b);
Complexe add (Complexe a, Complexe b);
void addOP (Complexe *a, Complexe b);
double carre (double x);
double cnorm (Complexe c);
Complexe inv (Complexe c);
